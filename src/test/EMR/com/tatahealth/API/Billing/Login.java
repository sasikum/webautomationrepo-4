package com.tatahealth.API.Billing;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.map.HashedMap;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.apache.poi.util.SystemOutLogger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

import com.tatahealth.API.Core.CommonFunctions;
import com.tatahealth.API.Core.Encryption;
import com.tatahealth.DB.Scripts.OTP;
import com.tatahealth.EMR.Scripts.Billing.BillingTest;
import com.tatahealth.ReusableModules.Web_Testbase;
import com.tatahealth.API.libraries.ExcelIntegration;
import com.tatahealth.API.libraries.SheetsAPI;

public class Login {

	public static String url = null;
	public static String baseurl = null;
	public static String environment = null;
	public static String mobileNumber = null;
	public static String dataRange = null;
	public static Integer rangeIndex = 0;
	public static String message = null;
	public static String otp = null;

	// consumer details
	public static String consumerUserId = null;
	public static String consumerName = null;
	public static String consumerToken = null;
	public static String userDOB = null;
	public static JSONObject userProfile = null;
	public static JSONArray managedAccount = null;
	public static Integer switchUserIndex = 0;

	// purchase package use
	public static String packageAmount = null;
	public static String packageId = null;

	// cc avenue order id
	public static String orderId = null;
	public static String trackingId = null;
	public static String bankRefernceNo = null;

	public static String orgId = null;
	public static String doctorId = null;
	public Double consumerLatitude = 12.9121181;
	public Double consumerLongitude = 77.6445548;
	public static String slugName = null;

	public static String appointmentType = null;
	public static Integer appointmentAmount = 0;
	// public static String slot = null;
	public static String previousSlot = null;
	public static Integer rewardBalance = 0;

	// slot, appointment and bill details details
	public static Integer slotIndex = 0;
	public static JSONObject bookedSlot = null;
	public static Integer dateSlotId = 0;
	public static String appointmentDate = null;
	public static Long appointmentBookedDate = null;
	public static Integer appointmentId = 0;
	public static Integer slotId = 0;
	public static Integer billId = 0;
	// public static String doctorToken = null;
	public static String patientId = null;
	public static JSONObject locationObj = null;
	public JSONObject fetchDefaultServiceObj = null;
	public static FileWriter logFile;

	// emr reports
	public static JSONObject billingMetrics = null;

	/*
	 * public static Integer additionServiceAmount = 0; public static Integer
	 * serviceDiscount = 0; public static Integer additionalServiceDiscount = 0;
	 * public static String paid = null;
	 */
	public static Map<String, PaymentDetails> paymentObjectMapping = new HashedMap<String, PaymentDetails>();
	public static Map<String, PaymentDetails> readExcel = new HashedMap<String, PaymentDetails>();
	public static Map<String, PaymentDetails> paymentWithBillId = new HashedMap<String, PaymentDetails>();

	// package info
	public static Integer carePackageId = 0;
	public static Integer purchasePackageId = 0;
	public static String packageName = null;

	// coupon infor
	public static String couponName = null;
	public static JSONObject couponpackageObj = null;
	public static String redemptionId = null;

	// doctor details
	public static String doctorSearchString = null;
	public static String doctorLoginName = null;
	public static String doctorPassword = null;
	public static String doctorToken = null;
	public static String doctorSSOId = null;
	public static String clinicUserId = null;
	public static String orgName = null;
	public static String userName = null;
	public static String visitId = null;
	public static String billNumber = null;

	public static String cbrflag = null;
	public static String emrreportsflag = null;
	public static String vbrflag = null;

	// non-tdh file name
	public static String nonTDHFileName = null;
	public static Integer draft = 8;

	// clinic billing file name
	public static String clinicBillingFileName = null;

	public static String fileName = null;

	// clinic billing
	public static List<String> appointmentNameList = null;
	public static List<String> serviceNameList = null;

	// emr reports
	public static Double EMRGrossAmount = 0.0;
	public static Double EMRDiscountAmount = 0.0;
	public static Double EMRNetBilledAmount = 0.0;

	public static Double othersGrossAmount = 0.0;
	public static Double othersDiscountAmount = 0.0;
	public static Double othersNetBilledAmount = 0.0;

	// mobile number from array
	// public static String[] mobile = {"1199661199","1166991166","1199551199"};

	@BeforeSuite
	public void readData() throws Exception {
		environment = SheetsAPI.getDataFromSheet("Data!B1");
		if (environment.matches("QA")) {
			baseurl = "https://apiqa.tatahealth.com";
		} else if (environment.matches("STG")) {
			baseurl = "https://apigtw.tatahealth.com";
		}
		doctorLoginName = BillingTest.doctorName;
		doctorPassword = SheetsAPI.getDataConfig(Web_Testbase.input + "." +BillingTest.doctorName);
		doctorSearchString =BillingTest.doctorName;
	}

	/*
	 * @BeforeSuite public void openLogFile() throws Exception{ logFile = new
	 * FileWriter("request_response_log.txt",true); }
	 * 
	 * @AfterSuite public void closeLogFile() throws Exception{ logFile.close(); }
	 */
	public String readLoginMobileNumber(Integer rangeIn) throws Exception {

		// hide code to test manually

		if (rangeIn == 0) {
			rangeIndex = 3;
		}

		dataRange = "Data!A" + rangeIndex.toString();
		// System.out.println("data range : "+rangeIndex);

		mobileNumber = SheetsAPI.getDataFromSheet(dataRange);

		// mobileNumber = mobile[rangeIndex];
		// System.out.println("Mobile Number : "+mobileNumber);

		return mobileNumber;
	}

	@BeforeTest
	public void consumerLogin() throws Exception {
		// https://apigtw.tatahealth.com/consumerOnboarding/api/v2/verifyMobileNo/1199661199
		url = baseurl + "/consumerOnboarding/api/v2/verifyMobileNo/" + readLoginMobileNumber(rangeIndex);
		// System.out.println("url : "+url);
		rangeIndex++;
		if(rangeIndex>10)
		{
			rangeIndex=3;
		}

		CommonFunctions func = new CommonFunctions();
		OTP getotp = new OTP();

		// add headers
		List<NameValuePair> headers = new ArrayList<NameValuePair>();
		headers.add(new BasicNameValuePair("platformId", "3"));

		JSONObject responseObj = func.getRequestWithHeaders(url, headers);

		System.out.println("Logged in with: " + mobileNumber);

		message = responseObj.getJSONObject("status").getString("message");
		// System.out.println("message : "+message);

		message = responseObj.getJSONObject("responseData").getString("message");
		// System.out.println("message : "+message);

		if (baseurl.equals("https://apiqa.tatahealth.com")) {
			otp = getotp.getQAOTP(mobileNumber);
		} else if (baseurl.equals("https://apigtw.tatahealth.com")) {
			otp = getotp.getStagingOTP(mobileNumber);
		}
		url = baseurl + "/consumerOnboarding/api/v4/userLogin";

		headers.add(new BasicNameValuePair("visitorId", "d82494ce-d421-47b7-a864-3291c5a515ab"));

		JSONObject param = new JSONObject();
		param.put("userLoginName", mobileNumber);
		param.put("otpCode", otp);
		param.put("deviceId", "");
		param.put("deviceToken", "");
		param.put("macAddress", "");
		param.put("devicePlatform", "3");

		responseObj = func.PostHeadersWithJSONRequest(url, param, headers);
		System.out.println(responseObj);
		message = responseObj.getJSONObject("status").getString("message");
		userProfile = responseObj.getJSONObject("responseData").getJSONObject("userProfile");
		Integer ssoId = userProfile.getInt("consumerUserId");
		consumerUserId = ssoId.toString();
		consumerName = userProfile.getString("firstName");
		consumerToken = userProfile.getString("authToken");
		Long dob = userProfile.getLong("userDob");
		userDOB = convertMilliSecond(dob);
	}

	@AfterTest
	public void consumerLogout() throws Exception {

		// System.out.println("logout");

		url = baseurl + "/consumerOnboarding/api/v1/logout";

		CommonFunctions func = new CommonFunctions();
		List<NameValuePair> headers = new ArrayList<NameValuePair>();
		headers.add(new BasicNameValuePair("userId", consumerUserId));
		headers.add(new BasicNameValuePair("tata_auth_token", consumerToken));

		JSONObject param = new JSONObject();
		param.put("consumerUserId", consumerUserId);

		JSONObject responseObj = func.PostHeadersWithJSONRequest(url, param, headers);

		message = responseObj.getJSONObject("status").getString("message");
	}

	// @Before
	public void doctorLogin() throws Exception {

		url = baseurl + "/HPPClinicsCARestApp/rest/clinicAdministration/3.1/appLogin";

		CommonFunctions func = new CommonFunctions();
		List<NameValuePair> headers = new ArrayList<NameValuePair>();
		headers.add(new BasicNameValuePair("platformId", "3"));

		JSONObject param = new JSONObject();
		param.put("macAddress", "");
		param.put("deviceToken", "");
		param.put("devicePlatform", 3);
		param.put("userLoginName", doctorLoginName);

		Encryption enc = new Encryption();
		String encKey = null;
		if (doctorLoginName.length() > 8) {
			encKey = "TATADHP-" + doctorLoginName.substring(0, 8);
		} else if (doctorLoginName.length() == 8) {
			encKey = "TATADHP-" + doctorLoginName;
		} else if (doctorLoginName.length() < 8) {
			int len = 8 - doctorLoginName.length();
			String Key = "TATADHP-";
			while (len-- != 0) {
				Key = Key.concat("0");
			}
			encKey = Key + doctorLoginName;
			// System.out.println(encKey);
		}

		// Encrypt enc = new Encrypt();
		String encPassword = enc.encrypt(doctorPassword, encKey);

		param.put("password", encPassword);

		JSONObject responseObj = func.PostHeadersWithJSONRequest(url, param, headers);

		try {
			doctorToken = responseObj.getString("token");
			clinicUserId = responseObj.getString("clinicUserId");
			doctorSSOId = responseObj.getString("ssoId");
			// System.out.println("logged in");

		} catch (Exception e) {
			// System.out.println("inside catch");
			message = responseObj.getJSONObject("status").getString("message");
			doctorToken = responseObj.getJSONObject("responseData").getString("token");
			clinicUserId = responseObj.getJSONObject("responseData").getString("clinicUserId");
			doctorLogout();
			doctorLogin();
		}

		// System.out.println("message : "+message);

	}

	// @AfterClass
	public void doctorLogout() throws Exception {

		// System.out.println("doctor logout method");

		url = baseurl + "/HPPClinicsCARestApp/rest/clinicAdministration/3.0/logout";

		CommonFunctions func = new CommonFunctions();
		List<NameValuePair> headers = new ArrayList<NameValuePair>();
		headers.add(new BasicNameValuePair("tata_auth_token", doctorToken));

		JSONObject param = new JSONObject();
		param.put("token", doctorToken);
		param.put("clinicUserId", clinicUserId);
		param.put("userLoginName", doctorLoginName);

		JSONObject responseObj = func.PostHeadersWithJSONRequest(url, param, headers);

		System.out.println("doctor logout reponse : " + responseObj);

		message = responseObj.getJSONObject("status").getString("message");

		// System.out.println("logout message : "+ message);

	}

	public String convertMilliSecond(Long milliSec) throws Exception {

		DateFormat format = new SimpleDateFormat("ddMMyyyy");

		Date misc = new Date(milliSec);

		userDOB = format.format(misc);

		return userDOB;
	}

	public String millSecondMyFormat(Long milliSec) throws Exception {
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");

		Date misc = new Date(milliSec);

		appointmentDate = format.format(misc);

		return appointmentDate;
	}

	public String getTodaysDate() throws Exception {
		DateFormat format = new SimpleDateFormat("dd-MM-yyyy");

		Date appt = new Date();

		String apptDate = format.format(appt);

		return apptDate;
	}

	public void readExcelData() throws Exception {
		ExcelIntegration excelIn = new ExcelIntegration();
		excelIn.readExcel();
		// System.out.println(readExcel);

	}

	public Long convertDateToMilliSec(int plus) throws Exception {
		Long milliDate = (long) 0;
		DateFormat format = new SimpleDateFormat("dd-MM-yyyy");
		String formattedDate = null;
		if (plus == 0) {
			Date d = new Date();
			formattedDate = format.format(d);

		} else {

			Calendar c = Calendar.getInstance();
			// DateFormat format = new SimpleDateFormat("dd-MM-yyyy");
			c.add(Calendar.DAY_OF_YEAR, 1);
			Date d = c.getTime();
			formattedDate = format.format(d);

		}

		Date milli = format.parse(formattedDate);
		milliDate = milli.getTime();

		// System.out.println("milli sec date :"+milliDate);
		return milliDate;

	}

}
