package com.tatahealth.EMR.Scripts.Billing;

import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.sound.midi.SysexMessage;

import org.apache.tools.ant.filters.TokenFilter.Trim;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.EMR.Scripts.Consultation.ConsultationTest;
import com.tatahealth.EMR.Scripts.Consultation.GlobalPrintTest;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.pages.Billing.Billing;
import com.tatahealth.EMR.pages.Login.LoginPage;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class BillingTest {
	
	Billing bill = new Billing();
	LoginPage loginPage = new LoginPage();
	MasterAdministrationTest masterAdmin=new MasterAdministrationTest();
	ConsultationTest appt=new ConsultationTest();
	final static String EXPECTED_GENERATE_BILL_SUCCESS_MESSAGE = "Bill Created Successfully";
	static String service;
	public static String patientName;
	public static String doctorName;
	static String paymentModeCash;
	static String paymentModeWallet;
	static String paymentModeCard;
	static int billNumber;
	public static ExtentTest logger;
	static int rowCount;
	static double expectedNetTotalAmount;
	static double additionalAmountDiscount;
	static double payableAmount;
	static String pdf;
	static double grandTotal;

	  @BeforeClass(alwaysRun = true) public static void beforeClass() throws Exception { 
		  Reports.reports();
		  Login_Doctor.executionName="CretaeViewBillValidation";
		  Login_Doctor.LoginTestwithDiffrentUser("Kasu");
		  }
	  
	  @AfterClass(alwaysRun = true) public static void afterClass() throws Exception {
		  Login_Doctor.LogoutTest();
		  Reports.extent.flush(); 
		  }
       public  synchronized void billingPageLaunch() throws Exception {
		logger = Reports.extent.createTest("EMR_Billing Page Launch");
		bill.getEmrTopMenu(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);
		bill.getEmrTopMenuElement(Login_Doctor.driver, logger, "Billing").click();
		bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isSelected();
		bill.getEmrBillTab(Login_Doctor.driver, logger, "View Bill").isDisplayed();

	}

       /*Test case:EMR_Bill_001,EMR_Bill_002,EMR_Bill_004,EMR_Bill_005*/
	@Test(priority = 1)
	public synchronized void billingCreateBillSelectPatientSearchInvalidData() throws Exception {
		logger = Reports.extent.createTest("EMR_Billing Createbill Patient Search InValid Data");
		bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);
		billingPageLaunch();
		generateBillButtonClick();
		String errorMessage = bill.getEmrBillErrorMessage(Login_Doctor.driver, logger).getText();
		Assert.assertTrue("Please select a patient".equals(errorMessage.trim()), "Actual message not expected");
		bill.getEmrCreateBillPatientTextBox(Login_Doctor.driver, logger).clear();
		bill.getEmrCreateBillPatientTextBox(Login_Doctor.driver, logger).sendKeys("Invalid data");
		String noResultFound = bill.getEmrCreateBillPatientSearchNoResulFoundText(Login_Doctor.driver, logger)
				.getText();
		Assert.assertTrue("No result found".equals(noResultFound.trim()),
				"Actual No Result Found not matched with expected");
	}
	 /*Test case:EMR_Bill_003 */
	@Test(priority = 2)
	public synchronized void billingCreateBillSelectPatientSearchValidData() throws Exception {
		logger = Reports.extent.createTest("EMR_Billing Createbill Patient Search Valid Data");
		bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);
		billingPageLaunch();
		String[] s = {"*8117", "1234567897", "#7777", "Ballu"};
		for(String op:s)
		{
			bill.getEmrCreateBillPatientTextBox(Login_Doctor.driver, logger).clear();
			Web_GeneralFunctions.wait(1);
			bill.getEmrCreateBillPatientTextBox(Login_Doctor.driver, logger).sendKeys(op);
			List<WebElement> elePatientSearchResult = bill.getEmrCreateBillPatientSearchResultSelect(Login_Doctor.driver,
					logger);
			elePatientSearchResult.get(0).click();
		}
	}
	 /*Test case:EMR_Bill_006 -EMR_Bill_052,Biiling_C_P_46*/
	@Test(priority = 3)
	public synchronized void billingCreateBillValidation() throws Exception {
		logger = Reports.extent.createTest("EMR_Billing Createbill Service Validation");
		bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);
		billingPageLaunch();
		rowCount = 0;
		expectedNetTotalAmount=0;
		Web_GeneralFunctions.wait(4);
		bill.getEmrCreateBillPatientTextBox(Login_Doctor.driver, logger).sendKeys("Ballu");
		List<WebElement> elePatientSearchResult = bill.getEmrCreateBillPatientSearchResultSelect(Login_Doctor.driver,
				logger);
		elePatientSearchResult.get(0).click();
		patientName=bill.getEmrCreateBillPatientTextBox(Login_Doctor.driver, logger).getAttribute("value").trim();
		billingCreateBillSelectDoctorSearchValidData();
		billingCreateBillServiceSectionColumnDisplay();
		generateBillButtonClick();
		String errorMessage = bill.getEmrBillErrorMessage(Login_Doctor.driver, logger).getText();
		Assert.assertTrue("Please select a service".equals(errorMessage.trim()), "Actual message not expected");
		bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "service", rowCount).clear();
		bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "service", rowCount)
				.sendKeys("invalid data");
		String noServiceFound = bill.getEmrCreateBillServiceSearchMatchingServiceFoundText(Login_Doctor.driver, logger)
				.getText();
		Assert.assertTrue("No matching service found".equals(noServiceFound.trim()),
				"Actual No Service Found not matched with expected");
		bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "service", rowCount).clear();
		bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "service", rowCount).sendKeys("service");
		List<WebElement> eleServiceSearchResult = bill.getEmrCreateBillServiceList(Login_Doctor.driver, logger);
		eleServiceSearchResult.get(0).click();
		service = bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "service", rowCount)
				.getAttribute("value");
		tariffValidation();
		quantityValidation();
		discountPercentageValidation();
		discountAmountValidation();
		addNetTotalAmount();
		addService();
		Web_GeneralFunctions.wait(4);
		bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "service", rowCount)
				.sendKeys("Test Service");
		bill.getEmrCreateBillService(Login_Doctor.driver, logger, "Test Service",rowCount).click();
		String errorMessage1 = bill.getEmrBillErrorMessage(Login_Doctor.driver, logger).getText();
		Assert.assertTrue(("Duplicate service").equals(errorMessage1.trim()), "Actual message not expected");
		bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "service", rowCount).clear();
		bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "service", rowCount)
				.sendKeys("Lab service");
		bill.getEmrCreateBillService(Login_Doctor.driver, logger, "Lab service",rowCount).click();
		tariffValidation();
		quantityValidation();
		discountPercentageValidation();
		discountAmountValidation();
		addNetTotalAmount();
		subNetTotalAmount();
		deleteService();
		serviceTotalValidation();
		additionalDiscountAndGrandTotalValidation();
		grandTotal=getGrandTotal();
		billingCreateBillPaymentValidation();
		paymentCashWallet();
		billingCreateBillPopUpValidation();
		billingCreateBillPdfValidation();
	}	
	 /*Test case:EMR_Bill_053-EMR_Bill_055*/
	 @Test(priority=4) 
	  public synchronized void billingViewBillPatientSearchValidation() throws Exception
	 {
	  logger = Reports.extent.createTest("EMR_Billing View Bill Patient Search Validation");
	  bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);
	  billingPageLaunch();
	  Web_GeneralFunctions.wait(5);
	  bill.getEmrBillTab(Login_Doctor.driver, logger,"View Bill").click();
	  viewBillSearchAllfieldDisplayValidation();
	  viewBillPatientNameValidation();
	 }
	 /*Test case:EMR_Bill_056-EMR_Bill_058*/
	 @Test(priority=5) 
	  public synchronized void billingViewBillAgeSearchValidation() throws Exception
	 {
	  logger = Reports.extent.createTest("EMR_Billing View Bill Age Search Validation");
	  bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);
	  billingPageLaunch();
	  Web_GeneralFunctions.wait(5);
	  bill.getEmrBillTab(Login_Doctor.driver, logger,"View Bill").click();
	  viewBillSearchAllfieldDisplayValidation();
	  viewBillAgeValidation();
	 }
	 /*Test case:EMR_Bill_059*/
	 @Test(priority=6) 
	  public synchronized void billingViewBillGenderSearchValidation() throws Exception
	 {
	  logger = Reports.extent.createTest("EMR_Billing View Bill Gender Search Validation");
	  bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);
	  billingPageLaunch();
	  Web_GeneralFunctions.wait(5);
	  bill.getEmrBillTab(Login_Doctor.driver, logger,"View Bill").click();
	  viewBillSearchAllfieldDisplayValidation();
	  viewBillGenderValidation();
	 }
	 /*Test case:EMR_Bill_060-EMR_Bill_061*/
	 @Test(priority=7) 
	  public synchronized void billingViewBillUHIDValidation() throws Exception
	 {
	  logger = Reports.extent.createTest("EMR_Billing View Bill UHID Search Validation");
	  bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);
	  billingPageLaunch();
	  Web_GeneralFunctions.wait(5);
	  bill.getEmrBillTab(Login_Doctor.driver, logger,"View Bill").click();
	  viewBillSearchAllfieldDisplayValidation();
	  viewBillUHIDValidation();
	 }
	 /*Test case:EMR_Bill_062-EMR_Bill_063*/
	 @Test(priority=8) 
	  public synchronized void billingViewBillClinicIdValidation() throws Exception
	 {
	  logger = Reports.extent.createTest("EMR_Billing View Bill ClinicId Search Validation");
	  bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);
	  billingPageLaunch();
	  Web_GeneralFunctions.wait(5);
	  bill.getEmrBillTab(Login_Doctor.driver, logger,"View Bill").click();
	  viewBillSearchAllfieldDisplayValidation();
	  viewBillClinicIdValidation();
	 }
	 /*Test case:EMR_Bill_064-EMR_Bill_066*/
	 @Test(priority=9) 
	  public synchronized void billingViewBillFromDateValidation() throws Exception
	 {
	  logger = Reports.extent.createTest("EMR_Billing View Bill FromDate Search Validation");
	  bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);
	  billingPageLaunch();
	  Web_GeneralFunctions.wait(5);
	  bill.getEmrBillTab(Login_Doctor.driver, logger,"View Bill").click();
	  viewBillSearchAllfieldDisplayValidation();
	  viewBillFromDateValidation();
	 }
	 /*Test case:EMR_Bill_067-EMR_Bill_069*/
	 @Test(priority=10) 
	  public synchronized void billingViewBillToDateValidation() throws Exception
	 {
	  logger = Reports.extent.createTest("EMR_Billing View Bill ToDate Search Validation");
	  bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);
	  billingPageLaunch();
	  Web_GeneralFunctions.wait(5);
	  bill.getEmrBillTab(Login_Doctor.driver, logger,"View Bill").click();
	  viewBillSearchAllfieldDisplayValidation();
	  viewBillToDateValidation();
	 }
	 /*Test case:EMR_Bill_070*/
	 @Test(priority=11) 
	  public synchronized void billingViewBillMobileNumberValidation() throws Exception
	 {
	  logger = Reports.extent.createTest("EMR_Billing View Bill MobileNumber Search Validation");
	  bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);
	  billingPageLaunch();
	  Web_GeneralFunctions.wait(5);
	  bill.getEmrBillTab(Login_Doctor.driver, logger,"View Bill").click();
	  viewBillSearchAllfieldDisplayValidation();
	  viewBillMobileNumberValidation();
	 }
	 /*Test case:EMR_Bill_071*/
	@Test(priority=12) 
	  public synchronized void billingViewBillGovtIdValidation() throws Exception
	 {
	  logger = Reports.extent.createTest("EMR_Billing View Bill GovtId Search Validation");
	  bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);
	  billingPageLaunch();
	  Web_GeneralFunctions.wait(5);
	  bill.getEmrBillTab(Login_Doctor.driver, logger,"View Bill").click();
	  viewBillSearchAllfieldDisplayValidation();
	  viewBillGovtIdValidation();
	 }
	/*Test case:EMR_Bill_072-EMR_Bill_074*/
	 @Test(priority=13) 
	  public synchronized void billingViewBillTypeValidation() throws Exception
	 {
	  logger = Reports.extent.createTest("EMR_Billing View Bill Type Search Validation");
	  bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);
	  billingPageLaunch();
	  Web_GeneralFunctions.wait(5);
	  bill.getEmrBillTab(Login_Doctor.driver, logger,"View Bill").click();
	  viewBillSearchAllfieldDisplayValidation();
	  viewBillTypeValidation();
	 }
	 /*Test case:EMR_Bill_075-EMR_Bill_078*/
	 @Test(priority=14) 
	  public synchronized void billingViewBillDoctorValidation() throws Exception
	 {
	  logger = Reports.extent.createTest("EMR_Billing View Bill Type Search Validation");
	  bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);
	  billingPageLaunch();
	  Web_GeneralFunctions.wait(5);
	  bill.getEmrBillTab(Login_Doctor.driver, logger,"View Bill").click();
	  viewBillSearchAllfieldDisplayValidation();
	  viewBillSelectDoctorValidation();
	 }
	 /*Test case:EMR_Bill_079*/
	  @Test(priority=15) 
	  public synchronized void billingViewBillPrintValidation() throws Exception
	 {
	  logger = Reports.extent.createTest("EMR_Billing View Bill Result Print Button Validation");
	  bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);
	  billingPageLaunch();
	  Web_GeneralFunctions.wait(5);
	  bill.getEmrBillTab(Login_Doctor.driver, logger,"View Bill").click();
	  viewBillSearchAllfieldDisplayValidation();
	  viewBillResultPrintValidation();
	 }
	  /*Test case:EMR_Bill_081*/
	    @Test(priority=16) 
		  public synchronized void billingViewBillCancelledButtonValidation() throws Exception
		 {
		  logger = Reports.extent.createTest("EMR_Billing View Bill Result Cancelled Button Validation");
		  bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
		  billingPageLaunch();
		  Web_GeneralFunctions.wait(5);
		  bill.getEmrBillTab(Login_Doctor.driver, logger,"View Bill").click();
		  viewBillSearchAllfieldDisplayValidation();
		  viewBillResultCancelledValidation();
		 }
	    /*Test case:EMR_Bill_080*/
		 @Test(priority=17) 
		  public synchronized void billingViewBillCancelButtonValidation() throws Exception
		 {
		  logger = Reports.extent.createTest("EMR_Billing View Bill Result Cancel Button Validation");
		  bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
		  billingPageLaunch();
		  Web_GeneralFunctions.wait(5);
		  bill.getEmrBillTab(Login_Doctor.driver, logger,"View Bill").click();
		  viewBillSearchAllfieldDisplayValidation();
		  viewBillResultCancelValidation();
		 }
		public synchronized void billingCreateBillPaymentValidation() throws Exception {
			logger = Reports.extent.createTest("EMR_Billing Createbill Payment Validation");
			bill.getEmrCreateBillPaymentModeText(Login_Doctor.driver, logger).isDisplayed();
			bill.getEmrCreateBillPayementOption(Login_Doctor.driver, logger, "Cash").isDisplayed();
			bill.getEmrCreateBillPayementOption(Login_Doctor.driver, logger, "Card").isDisplayed();
			bill.getEmrCreateBillPayementOption(Login_Doctor.driver, logger, "Wallets").isDisplayed();
			paymentValidation();
		}

		public synchronized void billingCreateBillPopUpValidation() throws Exception {
			logger = Reports.extent.createTest("EMR_Billing Createbill Generate Bill Pop Up Validation");
			generateBillButtonClick();
			billPopUpValiadion();
		}

		public synchronized void billingCreateBillPdfValidation() throws Exception {
			logger = Reports.extent.createTest("EMR_Billing Createbill Generate Bill Pdf Validation");
			generateBillPrintButton();
			String pdfContent = pdfValidation().trim();
			pdf=pdfContent;
			Assert.assertTrue(pdfContent.contains("Name : "+patientName+""),"Patient Name not matching in Pdf");
			Assert.assertTrue(pdfContent.contains("Doctor : Dr. "+doctorName+""),"Doctor Name not matching in Pdf");
			Assert.assertTrue(pdfContent.contains(service),"Service not matching in Pdf");
			Assert.assertTrue(pdfContent.contains("Bill Number : "+billNumber +""), "Bill Number Not Matching in pdf");
			Assert.assertTrue(pdfContent.contains(paymentModeCash),"Cash payment not matching in Pdf");
			Assert.assertTrue(pdfContent.contains(paymentModeWallet), "Wallet Payment Not Matching in pdf");
			Assert.assertTrue(pdfContent.contains("SAC Code"), "SAC Code Not Present in pdf");
	        Assert.assertTrue(pdfContent.contains(getCurrentDate().trim()), "Current date is not matching in pdf");
	        double expectedGrandTotal=Double.parseDouble(pdfContent.substring(pdfContent.lastIndexOf("Rs.")+3,pdfContent.lastIndexOf('(')).trim());
	        double expectedAmountPayable=Double.parseDouble(pdfContent.substring(pdfContent.lastIndexOf("- Rs.")+5,pdfContent.lastIndexOf("Grand")).trim());
	        double expectedAdditionalDiscountAmount=Double.parseDouble(pdfContent.substring(pdfContent.lastIndexOf("amount")+11,pdfContent.lastIndexOf("amount")+15).trim());
	       // if(!(expectedGrandTotal==grandTotal))
	      //  {
	        //	Assert.assertTrue(false, "Grand total not matching in Pdf");
	     //   }
	         //if(!(expectedAmountPayable==payableAmount))
		       //{
		        	//Assert.assertTrue(false, "Paid at Clinic not matching in Pdf");
		      // }
	         if(!(expectedAdditionalDiscountAmount==additionalAmountDiscount))
		       {
		        	Assert.assertTrue(false, "Additional Discount Amount not matching in Pdf");
		       }
	        String[] s=patientName.trim().split("\\s+",2);	
	        patientName=s[1].trim();
	        
	}
		public synchronized void billingCashWalletPdfValidation() throws Exception {
			logger = Reports.extent.createTest("EMR_Billing Createbill Generate Bill Pdf Validation");
			generateBillPrintButton();
			String pdfContent = pdfValidation().trim();
			pdf=pdfContent;
			Assert.assertTrue(pdfContent.contains("Name : "+patientName+""),"Patient Name not matching in Pdf");
			Assert.assertTrue(pdfContent.contains("Doctor : Dr. "+doctorName+""),"Doctor Name not matching in Pdf");
			Assert.assertTrue(pdfContent.contains(service),"Service not matching in Pdf");
			Assert.assertTrue(pdfContent.contains("Bill Number : "+billNumber +""), "Bill Number Not Matching in pdf");
			Assert.assertTrue(pdfContent.contains(paymentModeCash),"Cash payment not matching in Pdf");
			Assert.assertTrue(pdfContent.contains(paymentModeWallet), "Wallet Payment Not Matching in pdf");
			Assert.assertTrue(pdfContent.contains("SAC Code"), "SAC Code Not Present in pdf");
	        Assert.assertTrue(pdfContent.contains(getCurrentDate().trim()), "Current date is not matching in pdf");
	        double expectedGrandTotal=Double.parseDouble(pdfContent.substring(pdfContent.lastIndexOf("Rs.")+3,pdfContent.lastIndexOf('(')).trim());
	        double expectedAmountPayable=Double.parseDouble(pdfContent.substring(pdfContent.lastIndexOf("- Rs.")+5,pdfContent.lastIndexOf("Grand")).trim());
	        double expectedAdditionalDiscountAmount=Double.parseDouble(pdfContent.substring(pdfContent.lastIndexOf("amount")+11,pdfContent.lastIndexOf("amount")+15).trim());
	       // if(!(expectedGrandTotal==grandTotal))
	      //  {
	        //	Assert.assertTrue(false, "Grand total not matching in Pdf");
	     //   }
	         //if(!(expectedAmountPayable==payableAmount))
		       //{
		        	//Assert.assertTrue(false, "Paid at Clinic not matching in Pdf");
		      // }
	         if(!(expectedAdditionalDiscountAmount==additionalAmountDiscount))
		       {
		        	Assert.assertTrue(false, "Additional Discount Amount not matching in Pdf");
		       }
	        String[] s=patientName.trim().split("\\s+",2);	
	        patientName=s[1].trim();
	        
	}
		public synchronized void billingCashCardPdfValidation() throws Exception {
			logger = Reports.extent.createTest("EMR_Billing Createbill Generate Bill Pdf Validation");
			generateBillPrintButton();
			String pdfContent = pdfValidation().trim();
			pdf=pdfContent;
			Assert.assertTrue(pdfContent.contains("Name : "+patientName+""),"Patient Name not matching in Pdf");
			Assert.assertTrue(pdfContent.contains("Doctor : Dr. "+doctorName+""),"Doctor Name not matching in Pdf");
			Assert.assertTrue(pdfContent.contains(service),"Service not matching in Pdf");
			Assert.assertTrue(pdfContent.contains("Bill Number : "+billNumber +""), "Bill Number Not Matching in pdf");
			Assert.assertTrue(pdfContent.contains(paymentModeCash),"Cash payment not matching in Pdf");
			Assert.assertTrue(pdfContent.contains(paymentModeCard), "Card Payment Not Matching in pdf");
			Assert.assertTrue(pdfContent.contains("SAC Code"), "SAC Code Not Present in pdf");
	        Assert.assertTrue(pdfContent.contains(getCurrentDate().trim()), "Current date is not matching in pdf");
	        double expectedGrandTotal=Double.parseDouble(pdfContent.substring(pdfContent.lastIndexOf("Rs.")+3,pdfContent.lastIndexOf('(')).trim());
	        double expectedAmountPayable=Double.parseDouble(pdfContent.substring(pdfContent.lastIndexOf("- Rs.")+5,pdfContent.lastIndexOf("Grand")).trim());
	        double expectedAdditionalDiscountAmount=Double.parseDouble(pdfContent.substring(pdfContent.lastIndexOf("amount")+11,pdfContent.lastIndexOf("amount")+15).trim());
	       // if(!(expectedGrandTotal==grandTotal))
	      //  {
	        //	Assert.assertTrue(false, "Grand total not matching in Pdf");
	     //   }
	         //if(!(expectedAmountPayable==payableAmount))
		       //{
		        	//Assert.assertTrue(false, "Paid at Clinic not matching in Pdf");
		      // }
	         if(!(expectedAdditionalDiscountAmount==additionalAmountDiscount))
		       {
		        	Assert.assertTrue(false, "Additional Discount Amount not matching in Pdf");
		       }
	        String[] s=patientName.trim().split("\\s+",2);	
	        patientName=s[1].trim();
	        
	}
		public synchronized void billingCashPdfValidation() throws Exception {
			logger = Reports.extent.createTest("EMR_Billing Createbill Generate Bill Pdf Validation");
			generateBillPrintButton();
			String pdfContent = pdfValidation().trim();
			pdf=pdfContent;
			Assert.assertTrue(pdfContent.contains("Name : "+patientName+""),"Patient Name not matching in Pdf");
			Assert.assertTrue(pdfContent.contains("Doctor : Dr. "+doctorName+""),"Doctor Name not matching in Pdf");
			Assert.assertTrue(pdfContent.contains(service),"Service not matching in Pdf");
			Assert.assertTrue(pdfContent.contains("Bill Number : "+billNumber +""), "Bill Number Not Matching in pdf");
			Assert.assertTrue(pdfContent.contains(paymentModeCash),"Cash payment not matching in Pdf");
			Assert.assertTrue(pdfContent.contains("SAC Code"), "SAC Code Not Present in pdf");
	        Assert.assertTrue(pdfContent.contains(getCurrentDate().trim()), "Current date is not matching in pdf");
	        double expectedGrandTotal=Double.parseDouble(pdfContent.substring(pdfContent.lastIndexOf("Rs.")+3,pdfContent.lastIndexOf('(')).trim());
	        double expectedAmountPayable=Double.parseDouble(pdfContent.substring(pdfContent.lastIndexOf("- Rs.")+5,pdfContent.lastIndexOf("Grand")).trim());
	        double expectedAdditionalDiscountAmount=Double.parseDouble(pdfContent.substring(pdfContent.lastIndexOf("amount")+11,pdfContent.lastIndexOf("amount")+15).trim());
	       // if(!(expectedGrandTotal==grandTotal))
	      //  {
	        //	Assert.assertTrue(false, "Grand total not matching in Pdf");
	     //   }
	         //if(!(expectedAmountPayable==payableAmount))
		       //{
		        	//Assert.assertTrue(false, "Paid at Clinic not matching in Pdf");
		      // }
	         if(!(expectedAdditionalDiscountAmount==additionalAmountDiscount))
		       {
		        	Assert.assertTrue(false, "Additional Discount Amount not matching in Pdf");
		       }
	        String[] s=patientName.trim().split("\\s+",2);	
	        patientName=s[1].trim();
	        
	}
		public synchronized void billingCardPdfValidation() throws Exception {
			logger = Reports.extent.createTest("EMR_Billing Createbill Generate Bill Pdf Validation");
			generateBillPrintButton();
			String pdfContent = pdfValidation().trim();
			pdf=pdfContent;
			Assert.assertTrue(pdfContent.contains("Name : "+patientName+""),"Patient Name not matching in Pdf");
			Assert.assertTrue(pdfContent.contains("Doctor : Dr. "+doctorName+""),"Doctor Name not matching in Pdf");
			Assert.assertTrue(pdfContent.contains(service),"Service not matching in Pdf");
			Assert.assertTrue(pdfContent.contains("Bill Number : "+billNumber +""), "Bill Number Not Matching in pdf");
			Assert.assertTrue(pdfContent.contains(paymentModeCard),"Card payment not matching in Pdf");
			Assert.assertTrue(pdfContent.contains("SAC Code"), "SAC Code Not Present in pdf");
	        Assert.assertTrue(pdfContent.contains(getCurrentDate().trim()), "Current date is not matching in pdf");
	        double expectedGrandTotal=Double.parseDouble(pdfContent.substring(pdfContent.lastIndexOf("Rs.")+3,pdfContent.lastIndexOf('(')).trim());
	        double expectedAmountPayable=Double.parseDouble(pdfContent.substring(pdfContent.lastIndexOf("- Rs.")+5,pdfContent.lastIndexOf("Grand")).trim());
	        double expectedAdditionalDiscountAmount=Double.parseDouble(pdfContent.substring(pdfContent.lastIndexOf("amount")+11,pdfContent.lastIndexOf("amount")+15).trim());
	       // if(!(expectedGrandTotal==grandTotal))
	      //  {
	        //	Assert.assertTrue(false, "Grand total not matching in Pdf");
	     //   }
	         //if(!(expectedAmountPayable==payableAmount))
		       //{
		        	//Assert.assertTrue(false, "Paid at Clinic not matching in Pdf");
		      // }
	         if(!(expectedAdditionalDiscountAmount==additionalAmountDiscount))
		       {
		        	Assert.assertTrue(false, "Additional Discount Amount not matching in Pdf");
		       }
	        String[] s=patientName.trim().split("\\s+",2);	
	        patientName=s[1].trim();
	        
	}
		public synchronized void billingWalletPdfValidation() throws Exception {
			logger = Reports.extent.createTest("EMR_Billing Createbill Generate Bill Pdf Validation");
			generateBillPrintButton();
			String pdfContent = pdfValidation().trim();
			pdf=pdfContent;
			Assert.assertTrue(pdfContent.contains("Name : "+patientName+""),"Patient Name not matching in Pdf");
			Assert.assertTrue(pdfContent.contains("Doctor : Dr. "+doctorName+""),"Doctor Name not matching in Pdf");
			Assert.assertTrue(pdfContent.contains(service),"Service not matching in Pdf");
			Assert.assertTrue(pdfContent.contains("Bill Number : "+billNumber +""), "Bill Number Not Matching in pdf");
			Assert.assertTrue(pdfContent.contains(paymentModeWallet),"Wallet payment not matching in Pdf");
			Assert.assertTrue(pdfContent.contains("SAC Code"), "SAC Code Not Present in pdf");
	        Assert.assertTrue(pdfContent.contains(getCurrentDate().trim()), "Current date is not matching in pdf");
	        double expectedGrandTotal=Double.parseDouble(pdfContent.substring(pdfContent.lastIndexOf("Rs.")+3,pdfContent.lastIndexOf('(')).trim());
	        double expectedAmountPayable=Double.parseDouble(pdfContent.substring(pdfContent.lastIndexOf("- Rs.")+5,pdfContent.lastIndexOf("Grand")).trim());
	        double expectedAdditionalDiscountAmount=Double.parseDouble(pdfContent.substring(pdfContent.lastIndexOf("amount")+11,pdfContent.lastIndexOf("amount")+15).trim());
	       // if(!(expectedGrandTotal==grandTotal))
	      //  {
	        //	Assert.assertTrue(false, "Grand total not matching in Pdf");
	     //   }
	         //if(!(expectedAmountPayable==payableAmount))
		       //{
		        	//Assert.assertTrue(false, "Paid at Clinic not matching in Pdf");
		      // }
	         if(!(expectedAdditionalDiscountAmount==additionalAmountDiscount))
		       {
		        	Assert.assertTrue(false, "Additional Discount Amount not matching in Pdf");
		       }
	        String[] s=patientName.trim().split("\\s+",2);	
	        patientName=s[1].trim();
	        
	}
		public synchronized void billingCashWalletNoServicePdfValidation() throws Exception {
			logger = Reports.extent.createTest("EMR_Billing Createbill Generate Bill Pdf Validation No Service");
			generateBillPrintButton();
			String pdfContent = pdfValidation().trim();
			pdf=pdfContent;
			Assert.assertTrue(pdfContent.contains("Name : "+patientName+""),"Patient Name not matching in Pdf");
			Assert.assertTrue(pdfContent.contains("Doctor : Dr. "+doctorName+""),"Doctor Name not matching in Pdf");
			Assert.assertTrue(pdfContent.contains(service),"Service not matching in Pdf");
			Assert.assertTrue(pdfContent.contains("Bill Number : "+billNumber +""), "Bill Number Not Matching in pdf");
			Assert.assertTrue(pdfContent.contains(paymentModeCash),"Cash payment not matching in Pdf");
			Assert.assertTrue(pdfContent.contains(paymentModeWallet), "Wallet Payment Not Matching in pdf");
			Assert.assertTrue(pdfContent.contains("SAC Code"), "SAC Code Not Present in pdf");
	        Assert.assertTrue(pdfContent.contains(getCurrentDate().trim()), "Current date is not matching in pdf");
	        double expectedGrandTotal=Double.parseDouble(pdfContent.substring(pdfContent.lastIndexOf("Rs.")+3,pdfContent.lastIndexOf('(')).trim());
	        double expectedAmountPayable=Double.parseDouble(pdfContent.substring(pdfContent.lastIndexOf("- Rs.")+5,pdfContent.lastIndexOf("Grand")).trim());
	       // if(!(expectedGrandTotal==grandTotal))
	      //  {
	        //	Assert.assertTrue(false, "Grand total not matching in Pdf");
	     //   }
	         //if(!(expectedAmountPayable==payableAmount))
		       //{
		        	//Assert.assertTrue(false, "Paid at Clinic not matching in Pdf");
	        String[] s=patientName.trim().split("\\s+",2);	
	        patientName=s[1].trim();
	        
	}
		public synchronized void billingCashCardNoServicePdfValidation() throws Exception {
			logger = Reports.extent.createTest("EMR_Billing Createbill Generate Bill Pdf Validation No Service");
			generateBillPrintButton();
			String pdfContent = pdfValidation().trim();
			pdf=pdfContent;
			Assert.assertTrue(pdfContent.contains("Name : "+patientName+""),"Patient Name not matching in Pdf");
			Assert.assertTrue(pdfContent.contains("Doctor : Dr. "+doctorName+""),"Doctor Name not matching in Pdf");
			Assert.assertTrue(pdfContent.contains(service),"Service not matching in Pdf");
			Assert.assertTrue(pdfContent.contains("Bill Number : "+billNumber +""), "Bill Number Not Matching in pdf");
			Assert.assertTrue(pdfContent.contains(paymentModeCash),"Cash payment not matching in Pdf");
			Assert.assertTrue(pdfContent.contains(paymentModeCard), "Card Payment Not Matching in pdf");
			Assert.assertTrue(pdfContent.contains("SAC Code"), "SAC Code Not Present in pdf");
	        Assert.assertTrue(pdfContent.contains(getCurrentDate().trim()), "Current date is not matching in pdf");
	        double expectedGrandTotal=Double.parseDouble(pdfContent.substring(pdfContent.lastIndexOf("Rs.")+3,pdfContent.lastIndexOf('(')).trim());
	        double expectedAmountPayable=Double.parseDouble(pdfContent.substring(pdfContent.lastIndexOf("- Rs.")+5,pdfContent.lastIndexOf("Grand")).trim());
	       // if(!(expectedGrandTotal==grandTotal))
	      //  {
	        //	Assert.assertTrue(false, "Grand total not matching in Pdf");
	     //   }
	         //if(!(expectedAmountPayable==payableAmount))
		       //{
		        	//Assert.assertTrue(false, "Paid at Clinic not matching in Pdf");
		      // }
	        String[] s=patientName.trim().split("\\s+",2);	
	        patientName=s[1].trim();
	        
	}
		public synchronized void billingCashNoServicePdfValidation() throws Exception {
			logger = Reports.extent.createTest("EMR_Billing Createbill Generate Bill Pdf Validation No Service");
			generateBillPrintButton();
			String pdfContent = pdfValidation().trim();
			pdf=pdfContent;
			Assert.assertTrue(pdfContent.contains("Name : "+patientName+""),"Patient Name not matching in Pdf");
			Assert.assertTrue(pdfContent.contains("Doctor : Dr. "+doctorName+""),"Doctor Name not matching in Pdf");
			Assert.assertTrue(pdfContent.contains(service),"Service not matching in Pdf");
			Assert.assertTrue(pdfContent.contains("Bill Number : "+billNumber +""), "Bill Number Not Matching in pdf");
			Assert.assertTrue(pdfContent.contains(paymentModeCash),"Cash payment not matching in Pdf");
			Assert.assertTrue(pdfContent.contains("SAC Code"), "SAC Code Not Present in pdf");
	        Assert.assertTrue(pdfContent.contains(getCurrentDate().trim()), "Current date is not matching in pdf");
	        double expectedGrandTotal=Double.parseDouble(pdfContent.substring(pdfContent.lastIndexOf("Rs.")+3,pdfContent.lastIndexOf('(')).trim());
	        double expectedAmountPayable=Double.parseDouble(pdfContent.substring(pdfContent.lastIndexOf("- Rs.")+5,pdfContent.lastIndexOf("Grand")).trim());
	       // if(!(expectedGrandTotal==grandTotal))
	      //  {
	        //	Assert.assertTrue(false, "Grand total not matching in Pdf");
	     //   }
	         //if(!(expectedAmountPayable==payableAmount))
		       //{
		        	//Assert.assertTrue(false, "Paid at Clinic not matching in Pdf");
		      // }
	        String[] s=patientName.trim().split("\\s+",2);	
	        patientName=s[1].trim();
	        
	}
		public synchronized void billingCardNoServicePdfValidation() throws Exception {
			logger = Reports.extent.createTest("EMR_Billing Createbill Generate Bill Pdf Validation No Service");
			generateBillPrintButton();
			String pdfContent = pdfValidation().trim();
			pdf=pdfContent;
			Assert.assertTrue(pdfContent.contains("Name : "+patientName+""),"Patient Name not matching in Pdf");
			Assert.assertTrue(pdfContent.contains("Doctor : Dr. "+doctorName+""),"Doctor Name not matching in Pdf");
			Assert.assertTrue(pdfContent.contains(service),"Service not matching in Pdf");
			Assert.assertTrue(pdfContent.contains("Bill Number : "+billNumber +""), "Bill Number Not Matching in pdf");
			Assert.assertTrue(pdfContent.contains(paymentModeCard),"Card payment not matching in Pdf");
			Assert.assertTrue(pdfContent.contains("SAC Code"), "SAC Code Not Present in pdf");
	        Assert.assertTrue(pdfContent.contains(getCurrentDate().trim()), "Current date is not matching in pdf");
	        double expectedGrandTotal=Double.parseDouble(pdfContent.substring(pdfContent.lastIndexOf("Rs.")+3,pdfContent.lastIndexOf('(')).trim());
	        double expectedAmountPayable=Double.parseDouble(pdfContent.substring(pdfContent.lastIndexOf("- Rs.")+5,pdfContent.lastIndexOf("Grand")).trim());
	       // if(!(expectedGrandTotal==grandTotal))
	      //  {
	        //	Assert.assertTrue(false, "Grand total not matching in Pdf");
	     //   }
	         //if(!(expectedAmountPayable==payableAmount))
		       //{
		        	//Assert.assertTrue(false, "Paid at Clinic not matching in Pdf");
		      // }
	        String[] s=patientName.trim().split("\\s+",2);	
	        patientName=s[1].trim();
	        
	}
		public synchronized void billingWalletNoServicePdfValidation() throws Exception {
			logger = Reports.extent.createTest("EMR_Billing Createbill Generate Bill Pdf Validation No Service");
			generateBillPrintButton();
			String pdfContent = pdfValidation().trim();
			pdf=pdfContent;
			Assert.assertTrue(pdfContent.contains("Name : "+patientName+""),"Patient Name not matching in Pdf");
			Assert.assertTrue(pdfContent.contains("Doctor : Dr. "+doctorName+""),"Doctor Name not matching in Pdf");
			Assert.assertTrue(pdfContent.contains(service),"Service not matching in Pdf");
			Assert.assertTrue(pdfContent.contains("Bill Number : "+billNumber +""), "Bill Number Not Matching in pdf");
			Assert.assertTrue(pdfContent.contains(paymentModeWallet),"Wallet payment not matching in Pdf");
			Assert.assertTrue(pdfContent.contains("SAC Code"), "SAC Code Not Present in pdf");
	        Assert.assertTrue(pdfContent.contains(getCurrentDate().trim()), "Current date is not matching in pdf");
	        double expectedGrandTotal=Double.parseDouble(pdfContent.substring(pdfContent.lastIndexOf("Rs.")+3,pdfContent.lastIndexOf('(')).trim());
	        double expectedAmountPayable=Double.parseDouble(pdfContent.substring(pdfContent.lastIndexOf("- Rs.")+5,pdfContent.lastIndexOf("Grand")).trim());
	       // if(!(expectedGrandTotal==grandTotal))
	      //  {
	        //	Assert.assertTrue(false, "Grand total not matching in Pdf");
	     //   }
	         //if(!(expectedAmountPayable==payableAmount))
		       //{
		        	//Assert.assertTrue(false, "Paid at Clinic not matching in Pdf");
		      // }
	        String[] s=patientName.trim().split("\\s+",2);	
	        patientName=s[1].trim();
	        
	}
		public synchronized String pdfValidation() throws Exception {

			logger = Reports.extent.createTest("EMR Bill Pdf Validation");
			GlobalPrintTest gpt = new GlobalPrintTest();
			Web_GeneralFunctions.wait(5);
			String pdfContent = gpt.getPDFPage();
			return pdfContent;

		}
	 public synchronized void billingCreateBillSelectDoctorSearchValidData() {
			logger = Reports.extent.createTest("EMR_Billing Createbill Doctor Search Validation Data");
			bill.getEmrCreateBillDoctorTextBox(Login_Doctor.driver, logger).clear();
			bill.getEmrCreateBillDoctorTextBox(Login_Doctor.driver, logger).sendKeys("Invalid data");
			String noResultFound = bill.getEmrCreateBillDoctorSearchNoResulFoundText(Login_Doctor.driver, logger).getText();
			Assert.assertTrue("No result found".equals(noResultFound.trim()),
					"Actual No Result Found not matched with expected");
			bill.getEmrCreateBillDoctorTextBox(Login_Doctor.driver, logger).clear();
			bill.getEmrCreateBillDoctorTextBox(Login_Doctor.driver, logger).sendKeys("Kasu");
			List<WebElement> eleDoctorSearchResult = bill.getEmrCreateBillDoctorSearchResult(Login_Doctor.driver, logger);
			eleDoctorSearchResult.get(0).click();
			doctorName=bill.getEmrCreateBillDoctorTextBox(Login_Doctor.driver, logger).getAttribute("value").trim();
		}

		public synchronized void billingCreateBillServiceSectionColumnDisplay() {
			logger = Reports.extent.createTest("EMR_Billing Createbill Service Column Section");
			bill.getEmrCreateBillServiceSectionText(Login_Doctor.driver, logger).isDisplayed();
			bill.getEmrCreateBillServiceColumnText(Login_Doctor.driver, logger, "Service").isDisplayed();
			bill.getEmrCreateBillServiceColumnText(Login_Doctor.driver, logger, "Tariff (Rs.)").isDisplayed();
			bill.getEmrCreateBillServiceColumnText(Login_Doctor.driver, logger, "Quantity").isDisplayed();
			bill.getEmrCreateBillServiceColumnText(Login_Doctor.driver, logger, "Discount (%)").isDisplayed();
			bill.getEmrCreateBillServiceColumnText(Login_Doctor.driver, logger, "Discount Amt. (Rs.)").isDisplayed();
			bill.getEmrCreateBillServiceColumnText(Login_Doctor.driver, logger, "App discount (Rs.)").isDisplayed();
			bill.getEmrCreateBillServiceColumnText(Login_Doctor.driver, logger, "Net Amt. (Rs.)").isDisplayed();
			bill.getEmrCreateBillServiceColumnText(Login_Doctor.driver, logger, "Actions").isDisplayed();
		}
	 
	 

	public synchronized void tariffValidation() throws Exception {
		Web_GeneralFunctions.wait(2);
		bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "tariff", rowCount).clear();
		bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "tariff", rowCount).sendKeys("asdfg$%^&((&^$!");
		String tariff = bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "tariff", rowCount).getAttribute("value");
		Assert.assertTrue(tariff.trim().equals(""), "Tariff taking other character than number");
		bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "tariff", rowCount).clear();
		double amount = (348.72*(rowCount + 1));
		bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "tariff", rowCount).sendKeys(String.valueOf(amount));
	}

	public synchronized void quantityValidation() {
		bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "quantity", rowCount).clear();
		bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "quantity", rowCount)
				.sendKeys("asdfg$%^&((&^$!1.a23");
		String quantity = bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "quantity", rowCount)
				.getAttribute("value");
		Assert.assertTrue(quantity.trim().equals("12"), "Traiff taking other character than number or more 2 digit");
		bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "quantity", rowCount).clear();
		int number = (2 * (rowCount + 1));
		bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "quantity", rowCount)
				.sendKeys(String.valueOf(number));
	}

	public synchronized void discountPercentageValidation() {
		double appDiscountAmount;
		bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "discountPercent", rowCount).clear();
		bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "discountPercent", rowCount)
				.sendKeys("101");
		String errorMessage = bill.getEmrBillErrorMessage(Login_Doctor.driver, logger).getText();
		Assert.assertTrue("Discount percentage cannot not exceed 100".equals(errorMessage.trim()),
				"Actual message not expected");
		Assert.assertTrue("100".equals(
				bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "discountPercent", rowCount)
						.getAttribute("value").trim()),
				"Actual percentage not expected");
		bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "discountPercent", rowCount).clear();
		double discountP = (20.5 * (rowCount + 1));
		bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "discountPercent", rowCount)
				.sendKeys(String.valueOf(discountP));
		double tariff = Double
				.parseDouble(bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "tariff", rowCount)
						.getAttribute("value"));
		int quantity = Integer
				.parseInt(bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "quantity", rowCount)
						.getAttribute("value"));
		double discountPercent = Double.parseDouble(
				bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "discountPercent", rowCount)
						.getAttribute("value"));
		double discountAmount = Double.parseDouble(
				bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "discountAmount", rowCount).getAttribute("value"));
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "appDiscountAmount", rowCount).getAttribute("readonly").trim().equals("true")), "AppDiscountAmount editable");
		String appDiscount = bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "appDiscountAmount", rowCount).getAttribute("value");
		if (appDiscount.trim().equals(""))
			appDiscountAmount = 0;
		else
			appDiscountAmount = Double.parseDouble(appDiscount);
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "netAmount", rowCount).getAttribute("readonly").trim().equals("true")), "netAmount editable");
		DecimalFormat df2 = new DecimalFormat("#.##");
		df2.setRoundingMode(RoundingMode.UP);
		double expectedDiscountAmount = Double
				.parseDouble((df2.format(((discountPercent / 100) * ((tariff * quantity) - appDiscountAmount)))));
		DecimalFormat d = new DecimalFormat("#.##");
		double x = Double.parseDouble(d.format(expectedDiscountAmount - discountAmount));
		if (!((expectedDiscountAmount - discountAmount == 0) || (x == 0.01)))
			Assert.assertTrue(false, "Actual discount amount not matching with Expected");
		double netAmount = Double.parseDouble(d.format(Double.parseDouble(bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "netAmount", rowCount)
				.getAttribute("value"))));
		double expectednetAmount = Double.parseDouble(d.format((tariff * quantity) - (appDiscountAmount + discountAmount)));
		if (!( expectednetAmount==netAmount))
			Assert.assertTrue(false, "Actual net amount not matching with Expected");

	}

	public synchronized void discountAmountValidation() throws Exception {
		Web_GeneralFunctions.wait(4);
		double appDiscountAmount;
		double tariff = Double
				.parseDouble(bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "tariff", rowCount)
						.getAttribute("value"));
		int quantity = Integer
				.parseInt(bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "quantity", rowCount)
						.getAttribute("value"));
		double maxAmount = (tariff * quantity) + 1;
		bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "discountAmount", rowCount).clear();
		bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "discountAmount", rowCount)
				.sendKeys(String.valueOf(maxAmount));
		String errorMessage = bill.getEmrBillErrorMessage(Login_Doctor.driver, logger).getText();
		double newAdditionalDiscountAmount = Double.parseDouble(
				bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "discountAmount", rowCount)
						.getAttribute("value"));
		Assert.assertTrue(
				("Discount amount cannot not exceed " + newAdditionalDiscountAmount + "").equals(errorMessage.trim()),
				"Actual message not expected");
		bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "discountAmount", rowCount).clear();
		double discountA = (20 * (rowCount + 1));
		bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "discountAmount", rowCount)
				.sendKeys(String.valueOf(discountA));
		double discountPercentage = Double.parseDouble(
				bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "discountPercent", rowCount)
						.getAttribute("value"));
		double discountAmount = Double.parseDouble(
				bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "discountAmount", rowCount)
						.getAttribute("value"));
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "appDiscountAmount", rowCount).getAttribute("readonly").trim().equals("true")), "AppDiscountAmount editable");
		String appDiscount = bill
				.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "appDiscountAmount", rowCount)
				.getAttribute("value");
		if (appDiscount.equals(""))
			appDiscountAmount = 0;
		else
			appDiscountAmount = Double.parseDouble(appDiscount);
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "netAmount", rowCount).getAttribute("readonly").trim().equals("true")), "nettAmount editable");
		double netAmount = Double.parseDouble(
				bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "netAmount", rowCount)
						.getAttribute("value"));
		DecimalFormat df2 = new DecimalFormat("#.#");
		double expectedDiscountPercentage = Double
				.parseDouble(df2.format(((discountAmount * 100) / ((tariff * quantity) - appDiscountAmount))));
		if (!(Double.parseDouble(df2.format(expectedDiscountPercentage-discountPercentage))==0.0))
			Assert.assertTrue(false, "Actual Discount Percentage not matching with Expected");
		double expectednetAmount = ((tariff * quantity) - (appDiscountAmount + discountAmount));
		if (!(Double.parseDouble(df2.format(expectednetAmount-netAmount))==0.0))
			Assert.assertTrue(false, "Actual net amount not matching with Expected");

	}

	public synchronized void addService() {
		bill.getEmrCreateBillActionAddButton(Login_Doctor.driver, logger).click();
		rowCount++;
	}

	public synchronized void deleteService(){
		
	bill.getEmrCreateBillActionDeleteButton(Login_Doctor.driver, logger, rowCount).click();
	
	}

	public synchronized void addNetTotalAmount() {
		double netAmount = Double.parseDouble(
				bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "netAmount", rowCount)
						.getAttribute("value"));
		expectedNetTotalAmount = expectedNetTotalAmount + netAmount;
	}
	public synchronized void subNetTotalAmount() throws Exception {
		Web_GeneralFunctions.wait(4);
        double netAmount = Double.parseDouble(
				bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "netAmount", rowCount)
						.getAttribute("value"));
		expectedNetTotalAmount = expectedNetTotalAmount - netAmount;
	}

	public synchronized void serviceTotalValidation() {
		DecimalFormat df2 = new DecimalFormat("#.##");
		bill.getEmrCreateBillServiceTotalText(Login_Doctor.driver, logger).isDisplayed();
		bill.getEmrCreateBillServiceValue(Login_Doctor.driver, logger, "tariffTotal").isDisplayed();
		bill.getEmrCreateBillServiceValue(Login_Doctor.driver, logger, "averageDiscountPercentage").isDisplayed();
		bill.getEmrCreateBillServiceValue(Login_Doctor.driver, logger, "discountAmountTotal").isDisplayed();
		bill.getEmrCreateBillServiceValue(Login_Doctor.driver, logger, "appDiscountTotal").isDisplayed();
		bill.getEmrCreateBillServiceValue(Login_Doctor.driver, logger, "netAmountTotal").isDisplayed();
		double actualNetTotalAmount = Double.parseDouble(
				bill.getEmrCreateBillServiceValue(Login_Doctor.driver, logger, "netAmountTotal").getText());
		if (!(actualNetTotalAmount == Double.parseDouble(df2.format(expectedNetTotalAmount))))
			Assert.assertTrue(false, " ACtual net Total Amount is not matching with expected");

	}

	public synchronized void additionalDiscountAndGrandTotalValidation() throws Exception {

		bill.getEmrCreateBillAdditionalDiscountAmountText(Login_Doctor.driver, logger).isDisplayed();
		bill.getEmrCreateBillAdditionalDiscountPercentageText(Login_Doctor.driver, logger).isDisplayed();
		bill.getEmrCreateBillPaidOnlineText(Login_Doctor.driver, logger).isDisplayed();
		bill.getEmrCreateBillPaidOnlineText(Login_Doctor.driver, logger).isDisplayed();
		bill.getEmrCreateBillAmountPayableText(Login_Doctor.driver, logger).isDisplayed();
		bill.getEmrCreateBillGrandTotalText(Login_Doctor.driver, logger).isDisplayed();
		additionalDiscountPercentageValidtaion();
		additionalDiscountAmountValidtaion();
	}

	public synchronized void additionalDiscountPercentageValidtaion() throws Exception {
		Web_GeneralFunctions.wait(4);
		bill.getEmrCreateBillAdditionalDiscountPercentageTextBox(Login_Doctor.driver, logger).clear();
		bill.getEmrCreateBillAdditionalDiscountPercentageTextBox(Login_Doctor.driver, logger).sendKeys("101");
		String errorMessage = bill.getEmrBillErrorMessage(Login_Doctor.driver, logger).getText();
		Assert.assertTrue("Additional discount percentage cannot not exceed 100".equals(errorMessage.trim()),
				"Actual message not expected");
		bill.getEmrCreateBillAdditionalDiscountPercentageTextBox(Login_Doctor.driver, logger).clear();
		bill.getEmrCreateBillAdditionalDiscountPercentageTextBox(Login_Doctor.driver, logger).sendKeys("8");
		Web_GeneralFunctions.wait(4);
		Assert.assertTrue((bill.getEmrCreateBillPaidOnlineTextBox(Login_Doctor.driver, logger).getAttribute("readonly").trim().equals("true")), "PaidOnline editable");
		double paidOnline = Double
				.parseDouble(bill.getEmrCreateBillPaidOnlineTextBox(Login_Doctor.driver, logger).getAttribute("value"));
		double actualAdditionalDiscountPercentage = Double
				.parseDouble(bill.getEmrCreateBillAdditionalDiscountPercentageTextBox(Login_Doctor.driver, logger)
						.getAttribute("value"));
		double actualAdditionalDiscountAmount = Double.parseDouble(bill
				.getEmrCreateBillAdditionalDiscountAmountTextBox(Login_Doctor.driver, logger).getAttribute("value"));
		double totalNetAmount = Double.parseDouble(
				bill.getEmrCreateBillServiceValue(Login_Doctor.driver, logger, "netAmountTotal").getText());
		DecimalFormat df2 = new DecimalFormat("#.##");
		DecimalFormat df1 = new DecimalFormat("#.#");
		df2.setRoundingMode(RoundingMode.UP);
		double expectedAdditionalDiscountAmount = Double
				.parseDouble(df2.format(((actualAdditionalDiscountPercentage / 100) * (totalNetAmount))));
		if (!(Double.parseDouble(df1.format(expectedAdditionalDiscountAmount-actualAdditionalDiscountAmount))==0.0))
			Assert.assertTrue(false, "Actual additional discount amount not equal to expected");
	}

	public synchronized void additionalDiscountAmountValidtaion() throws Exception {
		Assert.assertTrue((bill.getEmrCreateBillPaidOnlineTextBox(Login_Doctor.driver, logger).getAttribute("readonly").trim().equals("true")), "PaidOnline editable");
		double paidOnline = Double
				.parseDouble(bill.getEmrCreateBillPaidOnlineTextBox(Login_Doctor.driver, logger).getAttribute("value"));
		double totalNetAmount = Double.parseDouble(
				bill.getEmrCreateBillServiceValue(Login_Doctor.driver, logger, "netAmountTotal").getText());
		double maxAdditionAmount = totalNetAmount - paidOnline;
		bill.getEmrCreateBillAdditionalDiscountAmountTextBox(Login_Doctor.driver, logger).clear();
		bill.getEmrCreateBillAdditionalDiscountAmountTextBox(Login_Doctor.driver, logger)
				.sendKeys(String.valueOf(maxAdditionAmount + 1));
		String errorMessage = bill.getEmrBillErrorMessage(Login_Doctor.driver, logger).getText();
		Assert.assertTrue(
				("Additional discount amount cannot not exceed " + maxAdditionAmount + "").equals(errorMessage.trim()),
				"Actual message not expected");
		Assert.assertTrue(("" + maxAdditionAmount + "")
				.equals(bill.getEmrCreateBillAdditionalDiscountAmountTextBox(Login_Doctor.driver, logger)
						.getAttribute("value").trim()),
				"Actual amount not expected");
		bill.getEmrCreateBillAdditionalDiscountPercentageTextBox(Login_Doctor.driver, logger).clear();
		bill.getEmrCreateBillAdditionalDiscountPercentageTextBox(Login_Doctor.driver, logger).sendKeys("100");
		paymentValidation();
		bill.getEmrCreateBillAdditionalDiscountAmountTextBox(Login_Doctor.driver, logger).clear();
		bill.getEmrCreateBillAdditionalDiscountAmountTextBox(Login_Doctor.driver, logger).sendKeys("20");
		Web_GeneralFunctions.wait(4);
		double actualAdditionalDiscountPercentage = Double
				.parseDouble(bill.getEmrCreateBillAdditionalDiscountPercentageTextBox(Login_Doctor.driver, logger)
						.getAttribute("value"));
		double actualAdditionalDiscountAmount = Double.parseDouble(bill
				.getEmrCreateBillAdditionalDiscountAmountTextBox(Login_Doctor.driver, logger).getAttribute("value"));
		DecimalFormat df2 = new DecimalFormat("#.#");
		double expectedAdditionalDiscountPercentage = Double
				.parseDouble(df2.format(((actualAdditionalDiscountAmount * 100) / (totalNetAmount))));
		if (!(Double.parseDouble(df2.format(expectedAdditionalDiscountPercentage-actualAdditionalDiscountPercentage))==0.0))
			Assert.assertTrue(false, "Actual additional discount percentage not equal to expected");
	}

	public synchronized double getGrandTotal() {
		DecimalFormat df2 = new DecimalFormat("#.##");
		Assert.assertTrue((bill.getEmrCreateBillPaidOnlineTextBox(Login_Doctor.driver, logger).getAttribute("readonly").trim().equals("true")), "PaidOnline editable");
		double paidOnline = Double
				.parseDouble(bill.getEmrCreateBillPaidOnlineTextBox(Login_Doctor.driver, logger).getAttribute("value"));
		Assert.assertTrue((bill.getEmrCreateBillAmountPayableTextBox(Login_Doctor.driver, logger).getAttribute("readonly").trim().equals("true")), "Amount Payable editable");
		double amountPayable = Double.parseDouble(
				bill.getEmrCreateBillAmountPayableTextBox(Login_Doctor.driver, logger).getAttribute("value"));
		Assert.assertTrue((bill.getEmrCreateBillGrandTotalTextBox(Login_Doctor.driver, logger).getAttribute("readonly").trim().equals("true")), "Grand Total editable");
		double additionalDiscountAmount = Double.parseDouble(bill
				.getEmrCreateBillAdditionalDiscountAmountTextBox(Login_Doctor.driver, logger).getAttribute("value"));
		additionalAmountDiscount=additionalDiscountAmount;
		double totalNetAmount = Double.parseDouble(
				bill.getEmrCreateBillServiceValue(Login_Doctor.driver, logger, "netAmountTotal").getText());
		if (!(totalNetAmount == Double.parseDouble(df2.format(expectedNetTotalAmount))))
			Assert.assertTrue(false, "Actual net Total Amount not equal to expected");
		double expectedAmountPayable = totalNetAmount - paidOnline - additionalDiscountAmount;
		if (!(expectedAmountPayable == amountPayable))
			Assert.assertTrue(false, "Actual Amount Payable not equal to expected");
		payableAmount=amountPayable;
		double actualGrandTotal = Double
				.parseDouble(bill.getEmrCreateBillGrandTotalTextBox(Login_Doctor.driver, logger).getAttribute("value"));
		double expectedGrandTotal = paidOnline + amountPayable;
		if (!(actualGrandTotal == expectedGrandTotal))
			Assert.assertTrue(false, "Actual Grand Totalt not equal to expected");
		return actualGrandTotal;
	}

	public synchronized void paymentValidation() throws Exception {
		double amountPayable = Double.parseDouble(
				bill.getEmrCreateBillAmountPayableTextBox(Login_Doctor.driver, logger).getAttribute("value"));
		if (amountPayable == 0) {
				Assert.assertTrue(bill.getEmrCreateBillPayementOption(Login_Doctor.driver, logger, "Cash").getAttribute("disabled").equals("true"), "Cash option is not disabled");
				Assert.assertTrue(bill.getEmrCreateBillPayementOption(Login_Doctor.driver, logger, "Card").getAttribute("disabled").equals("true"), "Card option is not disabled");
				Assert.assertTrue(bill.getEmrCreateBillPayementOption(Login_Doctor.driver, logger, "Wallets").getAttribute("disabled").equals("true"), "Wallets option is not disabled");
		} else {
			generateBillButtonClick();
			String errorMessage = bill.getEmrBillErrorMessage(Login_Doctor.driver, logger).getText();
			Assert.assertTrue("Please select a mode of payment".equals(errorMessage.trim()), "Actual message not expected");
			Web_GeneralFunctions.wait(4);
			String expectedErrorMessage = "Payment Amount cannot exceed amount payable";
			bill.getEmrCreateBillPayementOption(Login_Doctor.driver, logger, "Cash").click();
			bill.getEmrCreateBillPayementOption(Login_Doctor.driver, logger, "Card").click();
			bill.getEmrCreateBillCardOption(Login_Doctor.driver, logger, "Credit Card").isDisplayed();
			bill.getEmrCreateBillCardOption(Login_Doctor.driver, logger, "Debit Card").isDisplayed();
			bill.getEmrCreateBillCardOption(Login_Doctor.driver, logger, "Credit Card").click();
			bill.getEmrCreateBillPayementOption(Login_Doctor.driver, logger, "Wallets").click();
			Web_GeneralFunctions.wait(2);
			bill.getEmrCreateBillPayementModeTextBox(Login_Doctor.driver, logger, "cashMode").clear();
			bill.getEmrCreateBillPayementModeTextBox(Login_Doctor.driver, logger, "cashMode")
					.sendKeys(String.valueOf(amountPayable + 1));
			String errorMessage1 = bill.getEmrBillErrorMessage(Login_Doctor.driver, logger).getText();
			Assert.assertTrue(expectedErrorMessage.equals(errorMessage1.trim()), "Actual message not expected");
			if(!(amountPayable==Double.parseDouble(bill.getEmrCreateBillPayementModeTextBox(Login_Doctor.driver, logger, "cashMode")
							.getAttribute("value").trim())))
			{
				Assert.assertTrue(false,"Actual payable amount not expected");
			}
			Web_GeneralFunctions.wait(5);
			bill.getEmrCreateBillPayementModeTextBox(Login_Doctor.driver, logger, "walletMode").clear();
			bill.getEmrCreateBillPayementModeTextBox(Login_Doctor.driver, logger, "walletMode")
					.sendKeys(String.valueOf(amountPayable + 1));
			Web_GeneralFunctions.wait(2);
			String errorMessage2 = bill.getEmrBillErrorMessage(Login_Doctor.driver, logger).getText();
			Assert.assertTrue(expectedErrorMessage.equals(errorMessage2.trim()), "Actual message not expected");
			if(!(amountPayable==Double.parseDouble(bill.getEmrCreateBillPayementModeTextBox(Login_Doctor.driver, logger, "walletMode")
					.getAttribute("value").trim())))
	            {
		              Assert.assertTrue(false,"Actual payable amount not expected");
	            }
			Web_GeneralFunctions.wait(3);
			bill.getEmrCreateBillPayementOption(Login_Doctor.driver, logger, "Card").click();
			bill.getEmrCreateBillCardOption(Login_Doctor.driver, logger, "Debit Card").click();
			bill.getEmrCreateBillPayementModeTextBox(Login_Doctor.driver, logger, "cardMode").clear();
			bill.getEmrCreateBillPayementModeTextBox(Login_Doctor.driver, logger, "cardMode")
					.sendKeys(String.valueOf(amountPayable + 1));
			String errorMessage3 = bill.getEmrBillErrorMessage(Login_Doctor.driver, logger).getText();
			Assert.assertTrue(expectedErrorMessage.equals(errorMessage3.trim()), "Actual message not expected");
			Web_GeneralFunctions.wait(5);
			generateBillButtonClick();
			String errorMessage4 = bill.getEmrBillErrorMessage(Login_Doctor.driver, logger).getText();
			Assert.assertTrue("Please enter a non-zero value".equals(errorMessage4.trim()),
					"Actual message not expected");
			bill.getEmrCreateBillPayementOption(Login_Doctor.driver, logger, "Cash").click();
			bill.getEmrCreateBillPayementOption(Login_Doctor.driver, logger, "Card").click();
}

	}
	public synchronized void paymentCashWallet() throws Exception {
		double amountPayable = Double.parseDouble(
		bill.getEmrCreateBillAmountPayableTextBox(Login_Doctor.driver, logger).getAttribute("value"));
		int entervalue = (int) (amountPayable - (amountPayable / 3));
		DecimalFormat df2 = new DecimalFormat("#.##");
		double remainingAmount = Double.parseDouble(df2.format(amountPayable - entervalue));
		double cashValue=entervalue;
		bill.getEmrCreateBillPayementOption(Login_Doctor.driver, logger, "Cash").click();
		bill.getEmrCreateBillPayementOption(Login_Doctor.driver, logger, "Wallets").click();
		bill.getEmrCreateBillPayementModeTextBox(Login_Doctor.driver, logger, "cashMode").clear();
		bill.getEmrCreateBillPayementModeTextBox(Login_Doctor.driver, logger, "cashMode")
				.sendKeys(String.valueOf(entervalue));
		double actualReaminingWalletAmount = Double
				.parseDouble(bill.getEmrCreateBillPayementModeTextBox(Login_Doctor.driver, logger, "walletMode")
						.getAttribute("value"));
		if (!(actualReaminingWalletAmount == remainingAmount))
			Assert.assertTrue(false, "Not expected Amount");
		paymentModeCash=String.format("Cash - Rs. %.2f",cashValue);
		paymentModeWallet=String.format("Wallet - Rs. %.2f", remainingAmount);
		
	}
	public synchronized void paymentCashDebitCard() throws Exception {
		double amountPayable = Double.parseDouble(
				bill.getEmrCreateBillAmountPayableTextBox(Login_Doctor.driver, logger).getAttribute("value"));
		int entervalue = (int) (amountPayable - (amountPayable / 3));
		DecimalFormat df2 = new DecimalFormat("#.##");
		double remainingAmount = Double.parseDouble(df2.format(amountPayable - entervalue));
		double cashValue=entervalue;
		bill.getEmrCreateBillPayementOption(Login_Doctor.driver, logger, "Cash").click();
		bill.getEmrCreateBillPayementOption(Login_Doctor.driver, logger, "Card").click();
		bill.getEmrCreateBillCardOption(Login_Doctor.driver, logger, "Debit Card").click();
		bill.getEmrCreateBillPayementModeTextBox(Login_Doctor.driver, logger, "cashMode").clear();
		bill.getEmrCreateBillPayementModeTextBox(Login_Doctor.driver, logger, "cashMode")
				.sendKeys(String.valueOf(entervalue));
		double actualReaminingCardAmount = Double
				.parseDouble(bill.getEmrCreateBillPayementModeTextBox(Login_Doctor.driver, logger, "cardMode")
						.getAttribute("value"));
		if (!(actualReaminingCardAmount == remainingAmount))
			Assert.assertTrue(false, "Not expected Amount");
		paymentModeCash=String.format("Cash - Rs. %.2f",cashValue);
		paymentModeCard=String.format("Debit card - Rs. %.2f", remainingAmount);
		
	}
	public synchronized void paymentCashCreditCard() throws Exception {
		double amountPayable = Double.parseDouble(
				bill.getEmrCreateBillAmountPayableTextBox(Login_Doctor.driver, logger).getAttribute("value"));
		int entervalue = (int) (amountPayable - (amountPayable / 3));
		DecimalFormat df2 = new DecimalFormat("#.##");
		double remainingAmount = Double.parseDouble(df2.format(amountPayable - entervalue));
		double cashValue=entervalue;
		bill.getEmrCreateBillPayementOption(Login_Doctor.driver, logger, "Cash").click();
		bill.getEmrCreateBillPayementOption(Login_Doctor.driver, logger, "Card").click();
		bill.getEmrCreateBillCardOption(Login_Doctor.driver, logger, "Credit Card").click();
		bill.getEmrCreateBillPayementModeTextBox(Login_Doctor.driver, logger, "cashMode").clear();
		bill.getEmrCreateBillPayementModeTextBox(Login_Doctor.driver, logger, "cashMode")
				.sendKeys(String.valueOf(entervalue));
		double actualReaminingCardAmount = Double
				.parseDouble(bill.getEmrCreateBillPayementModeTextBox(Login_Doctor.driver, logger, "cardMode")
						.getAttribute("value"));
		if (!(actualReaminingCardAmount == remainingAmount))
			Assert.assertTrue(false, "Not expected Amount");
		paymentModeCash=String.format("Cash - Rs. %.2f",cashValue);
		paymentModeCard=String.format("Credit card - Rs. %.2f", remainingAmount);
		
	}
	public synchronized void paymentCash() throws Exception {
		double amountPayable = Double.parseDouble(
				bill.getEmrCreateBillAmountPayableTextBox(Login_Doctor.driver, logger).getAttribute("value"));
		bill.getEmrCreateBillPayementOption(Login_Doctor.driver, logger, "Cash").click();
		paymentModeCash=String.format("Cash - Rs. %.2f",amountPayable);
		}
	public synchronized void paymentCreditCard() throws Exception {
		double amountPayable = Double.parseDouble(
				bill.getEmrCreateBillAmountPayableTextBox(Login_Doctor.driver, logger).getAttribute("value"));
		bill.getEmrCreateBillPayementOption(Login_Doctor.driver, logger, "Card").click();
		bill.getEmrCreateBillCardOption(Login_Doctor.driver, logger, "Credit Card").click();
		paymentModeCard=String.format("Credit card - Rs. %.2f", amountPayable);
		}
	public synchronized void paymentWallet() throws Exception {
		double amountPayable = Double.parseDouble(
				bill.getEmrCreateBillAmountPayableTextBox(Login_Doctor.driver, logger).getAttribute("value"));
		bill.getEmrCreateBillPayementOption(Login_Doctor.driver, logger, "Wallets").click();
		paymentModeWallet=String.format("Wallet - Rs. %.2f", amountPayable);
		}
	

	public synchronized void billPopUpValiadion() throws Exception {
		Web_GeneralFunctions.wait(4);
		bill.getEmrGenerateBillSuccessMessage(Login_Doctor.driver, logger).isDisplayed();
		String actualGenerateSuccessMessage = bill.getEmrGenerateBillSuccessMessage(Login_Doctor.driver, logger)
				.getText();
		Assert.assertTrue(actualGenerateSuccessMessage.equals(EXPECTED_GENERATE_BILL_SUCCESS_MESSAGE),
				"Actual Generate bill success message not equal to Expected");
		String message = bill.getEmrGeneratePatientNameBillNumber(Login_Doctor.driver, logger).getText();
		String billPatientName=message.substring(message.indexOf("for")+3,message.indexOf("has")).trim();
		String[] s=billPatientName.trim().split("\\s+",2);
		billPatientName=s[1].trim();
		Assert.assertTrue(billPatientName.equals(patientName),
				"Actual Patient Name is not equal to Expected");
		patientName=message.substring(message.indexOf("for")+3,message.indexOf("has")).trim();
		billNumber = Integer.parseInt(message.substring(message.indexOf(':')+1,message.length()).trim());
		bill.getEmrGenerateBillCloseButton(Login_Doctor.driver, logger).isDisplayed();
		bill.getEmrGenerateBillPrintButton(Login_Doctor.driver, logger).isDisplayed();
	}

	public synchronized void generateBillButtonClick() {
		bill.getEmrCreateBillGenerateButton(Login_Doctor.driver, logger).isEnabled();
		bill.getEmrCreateBillGenerateButton(Login_Doctor.driver, logger).click();
	}

	public synchronized void generateBillClosePopUp() {
		bill.getEmrGenerateBillCloseButton(Login_Doctor.driver, logger).isDisplayed();
		bill.getEmrGenerateBillCloseButton(Login_Doctor.driver, logger).click();
	}

	public synchronized void generateBillPrintButton() {
		bill.getEmrGenerateBillPrintButton(Login_Doctor.driver, logger).isEnabled();
		bill.getEmrGenerateBillPrintButton(Login_Doctor.driver, logger).click();
	}
	public synchronized String getCurrentDate() {
		
		Date d=new Date();
		DateFormat df=new SimpleDateFormat("dd-MM-yyyy");
		 return df.format(d);
	}
	public synchronized void viewBillPatientNameValidation() throws Exception {
		String expectedErrorMessage="No Results";
		bill.getEmrViewBillPatientNameSearchTextBox(Login_Doctor.driver, logger).clear();
		bill.getEmrViewBillPatientNameSearchTextBox(Login_Doctor.driver, logger).sendKeys("Invaliddata");
		searchBillButtonClick();
		String actualErrorNoResultMessage=bill.getEmrViewBillNoResultsMessage(Login_Doctor.driver, logger).getText().trim();
		Assert.assertTrue(actualErrorNoResultMessage.equals(expectedErrorMessage),"actual error message not matching with Expected");
		bill.getEmrViewBillPatientNameSearchTextBox(Login_Doctor.driver, logger).clear();
		bill.getEmrViewBillPatientNameSearchTextBox(Login_Doctor.driver, logger).sendKeys("A12$%^&*#bcd");
		Assert.assertTrue("Abcd".equals(bill.getEmrViewBillPatientNameSearchTextBox(Login_Doctor.driver, logger).getAttribute("value").trim()),"Taking special and numeric character");
		bill.getEmrViewBillPatientNameSearchTextBox(Login_Doctor.driver, logger).clear();
		bill.getEmrViewBillPatientNameSearchTextBox(Login_Doctor.driver, logger).sendKeys("Qwertyuiopasdfghjklzxcvbnmqwertyuiopasdfextra");
		Assert.assertTrue("Qwertyuiopasdfghjklzxcvbnmqwertyuiopasdf".equals(bill.getEmrViewBillPatientNameSearchTextBox(Login_Doctor.driver, logger).getAttribute("value").trim()),"limit  cross 40 character");
		bill.getEmrViewBillPatientNameSearchTextBox(Login_Doctor.driver, logger).clear();
		bill.getEmrViewBillPatientNameSearchTextBox(Login_Doctor.driver, logger).sendKeys("Ballu");
	    searchBillButtonClick();
		viewBillSearchResultColumnDisplayValidation();
		viewBillSearchResultColumnNetAmountValidation("Ballu",3);
		}
	public synchronized void viewBillAgeValidation() throws Exception {
		bill.getEmrViewBillAgeSearchTextBox(Login_Doctor.driver, logger).clear();
		bill.getEmrViewBillAgeSearchTextBox(Login_Doctor.driver, logger).sendKeys("1adjdjdj23%^^&56");
		Assert.assertTrue("123".equals(bill.getEmrViewBillAgeSearchTextBox(Login_Doctor.driver, logger).getAttribute("value").trim()),"Taking special and character, also more than 3 digit number");
		bill.getEmrViewBillAgeSearchTextBox(Login_Doctor.driver, logger).clear();
		bill.getEmrViewBillAgeSearchTextBox(Login_Doctor.driver, logger).sendKeys("22");
	    searchBillButtonClick();
		viewBillSearchResultColumnDisplayValidation();
		viewBillSearchResultColumnNetAmountValidation("22 Years",5);
		}
	public synchronized void viewBillGenderValidation() throws Exception {
		bill.getEmrViewBillGenderDropdown(Login_Doctor.driver, logger,"Male");
		searchBillButtonClick();
		viewBillSearchResultColumnDisplayValidation();
		viewBillSearchResultColumnNetAmountValidation("Male",3);
		}
	public synchronized void viewBillClinicIdValidation() throws Exception {
		String expectedErrorMessage="No Results";
		bill.getEmrViewBillClinicIdSearchTextBox(Login_Doctor.driver, logger).clear();
		bill.getEmrViewBillClinicIdSearchTextBox(Login_Doctor.driver, logger).sendKeys("invaliddata");
		searchBillButtonClick();
		String actualErrorNoResultMessage=bill.getEmrViewBillNoResultsMessage(Login_Doctor.driver, logger).getText().trim();
		Assert.assertTrue(actualErrorNoResultMessage.equals(expectedErrorMessage),"actual error message not matching with Expected");
		bill.getEmrViewBillClinicIdSearchTextBox(Login_Doctor.driver, logger).clear();
		bill.getEmrViewBillClinicIdSearchTextBox(Login_Doctor.driver, logger).sendKeys("12345&&^%%%%%DDFFGJJJJJjjjkwjdqw122");
		Assert.assertTrue("12345&&^%%%%%DDFFGJJJJJjjjkwjd".equals(bill.getEmrViewBillClinicIdSearchTextBox(Login_Doctor.driver, logger).getAttribute("value").trim()),"More than 30 character taking");
		bill.getEmrViewBillClinicIdSearchTextBox(Login_Doctor.driver, logger).clear();
		bill.getEmrViewBillClinicIdSearchTextBox(Login_Doctor.driver, logger).sendKeys("7777");
	    searchBillButtonClick();
		viewBillSearchResultColumnDisplayValidation();
		}
	public synchronized void viewBillUHIDValidation() throws Exception {
		String expectedErrorMessage="No Results";
		bill.getEmrViewBillUhIdSearchTextBox(Login_Doctor.driver, logger).clear();
		bill.getEmrViewBillUhIdSearchTextBox(Login_Doctor.driver, logger).sendKeys("123456789");
		searchBillButtonClick();
		String actualErrorNoResultMessage=bill.getEmrViewBillNoResultsMessage(Login_Doctor.driver, logger).getText().trim();
		Assert.assertTrue(actualErrorNoResultMessage.equals(expectedErrorMessage),"actual error message not matching with Expected");
		bill.getEmrViewBillUhIdSearchTextBox(Login_Doctor.driver, logger).clear();
		bill.getEmrViewBillUhIdSearchTextBox(Login_Doctor.driver, logger).sendKeys("1qwer^&&*%$765");
		Assert.assertTrue("1765".equals(bill.getEmrViewBillUhIdSearchTextBox(Login_Doctor.driver, logger).getAttribute("value").trim()),"Taking special and alpha character");
		bill.getEmrViewBillUhIdSearchTextBox(Login_Doctor.driver, logger).clear();
		bill.getEmrViewBillUhIdSearchTextBox(Login_Doctor.driver, logger).sendKeys("8117");
	    searchBillButtonClick();
		viewBillSearchResultColumnDisplayValidation();
		viewBillSearchResultColumnNetAmountValidation("8117",3);
		}
	public synchronized void viewBillFromDateValidation() throws Exception {
		bill.getEmrViewBillFromDateSearcCalendarIcon(Login_Doctor.driver, logger).click();
		bill.getEmrViewBillFromDateSearchTextBox(Login_Doctor.driver, logger).clear();
		bill.getEmrViewBillFromDateSearchTextBox(Login_Doctor.driver, logger).sendKeys("01-03-2020");
		searchBillButtonClick();
		viewBillSearchResultColumnDisplayValidation();
		}
	public synchronized void viewBillToDateValidation() throws Exception {
		bill.getEmrViewBillToDateSearcCalendarIcon(Login_Doctor.driver, logger).click();
		bill.getEmrViewBillToDateSearchTextBox(Login_Doctor.driver, logger).clear();
		bill.getEmrViewBillToDateSearchTextBox(Login_Doctor.driver, logger).sendKeys(getCurrentDate());
		searchBillButtonClick();
		viewBillSearchResultColumnDisplayValidation();
		}
	public synchronized void viewBillMobileNumberValidation() throws Exception {
		String expectedErrorMessage="No Results";
		bill.getEmrViewBillMobileNoSearchTextBox(Login_Doctor.driver, logger).clear();
		bill.getEmrViewBillMobileNoSearchTextBox(Login_Doctor.driver, logger).sendKeys("0827863264");
		searchBillButtonClick();
		String actualErrorNoResultMessage=bill.getEmrViewBillNoResultsMessage(Login_Doctor.driver, logger).getText().trim();
		Assert.assertTrue(actualErrorNoResultMessage.equals(expectedErrorMessage),"actual error message not matching with Expected");
		bill.getEmrViewBillMobileNoSearchTextBox(Login_Doctor.driver, logger).clear();
		bill.getEmrViewBillMobileNoSearchTextBox(Login_Doctor.driver, logger).sendKeys("123456789gsdajja$$$%%^10");
		Assert.assertTrue("1234567891".equals(bill.getEmrViewBillMobileNoSearchTextBox(Login_Doctor.driver, logger).getAttribute("value").trim()),"Taking special and alpha character,more than 10 digit");
		bill.getEmrViewBillMobileNoSearchTextBox(Login_Doctor.driver, logger).clear();
		bill.getEmrViewBillMobileNoSearchTextBox(Login_Doctor.driver, logger).sendKeys("1234567897");
	    searchBillButtonClick();
		viewBillSearchResultColumnDisplayValidation();
		viewBillSearchResultColumnNetAmountValidation("1234567897",4);
		bill.getEmrViewBillMobileNoSearchTextBox(Login_Doctor.driver, logger).clear();
		}
	public synchronized void viewBillGovtIdValidation() throws Exception {
		String expectedErrorMessage="No Results";
		bill.getEmrViewBillGovtIdSearchTextBox(Login_Doctor.driver, logger).clear();
		bill.getEmrViewBillGovtIdSearchTextBox(Login_Doctor.driver, logger).sendKeys("invaliddata");
		searchBillButtonClick();
		String actualErrorNoResultMessage=bill.getEmrViewBillNoResultsMessage(Login_Doctor.driver, logger).getText().trim();
		Assert.assertTrue(actualErrorNoResultMessage.equals(expectedErrorMessage),"actual error message not matching with Expected");
		bill.getEmrViewBillGovtIdSearchTextBox(Login_Doctor.driver, logger).clear();
		bill.getEmrViewBillGovtIdSearchTextBox(Login_Doctor.driver, logger).sendKeys("123456789gsdajja$$$%%^10");
		Assert.assertTrue("123456789gsdajja10".equals(bill.getEmrViewBillGovtIdSearchTextBox(Login_Doctor.driver, logger).getAttribute("value").trim()),"Taking special character");
		bill.getEmrViewBillGovtIdSearchTextBox(Login_Doctor.driver, logger).clear();
		bill.getEmrViewBillGovtIdSearchTextBox(Login_Doctor.driver, logger).sendKeys("123456789");
	    searchBillButtonClick();
		viewBillSearchResultColumnDisplayValidation();
		}
	public synchronized void viewBillTypeValidation() throws Exception {
		bill.getEmrViewBillTypeDropdown(Login_Doctor.driver, logger).click();
		bill.getEmrViewBillTypeDropdownValue(Login_Doctor.driver, logger,"Not Cancelled").click(); 
		bill.getEmrViewBillTypeDropdownValue(Login_Doctor.driver, logger,"Offline").click();
		searchBillButtonClick();
		viewBillSearchResultColumnDisplayValidation();
		bill.getEmrViewBillTypeDropdown(Login_Doctor.driver, logger).click();
		bill.getEmrViewBillTypeDropdownValue(Login_Doctor.driver, logger,"Not Cancelled").click(); 
		bill.getEmrViewBillTypeDropdownValue(Login_Doctor.driver, logger,"Offline").click();
		}
	public synchronized void viewBillSelectDoctorValidation() throws Exception {
		JavascriptExecutor je = (JavascriptExecutor) Login_Doctor.driver;
		bill. getEmrViewSelectDoctorDropdown(Login_Doctor.driver, logger).click();
		bill.getEmrViewBillSelectDoctorDropdownValue(Login_Doctor.driver, logger,"Baba").click(); 
		bill.getEmrViewBillSelectDoctorDropdownValue(Login_Doctor.driver, logger,"Kasu").click();
		je.executeScript("arguments[0].click();",bill.getEmrViewSelectDoctorDropdown(Login_Doctor.driver, logger));
		searchBillButtonClick();
		viewBillSearchResultColumnDisplayValidation();
		bill. getEmrViewSelectDoctorDropdown(Login_Doctor.driver, logger).click();
		bill.getEmrViewBillSelectDoctorDropdownValue(Login_Doctor.driver, logger,"Baba").click(); 
		je.executeScript("arguments[0].click();",bill. getEmrViewSelectDoctorDropdown(Login_Doctor.driver, logger));
		searchBillButtonClick();
		viewBillSearchResultColumnDisplayValidation();
		viewBillSearchResultColumnNetAmountValidation("Kasu",6);	
		}
	public synchronized void viewBillSearchResultColumnDisplayValidation() throws Exception {
		Web_GeneralFunctions.wait(4);
		bill.getEmrViewBillSerachResultColumnText(Login_Doctor.driver, logger,"Bill").isDisplayed();
		bill.getEmrViewBillSerachResultColumnText(Login_Doctor.driver, logger,"Date").isDisplayed();
		bill.getEmrViewBillSerachResultColumnText(Login_Doctor.driver, logger,"Patient").isDisplayed();
		bill.getEmrViewBillSerachResultColumnText(Login_Doctor.driver, logger,"Mobile No").isDisplayed();
		bill.getEmrViewBillSerachResultColumnText(Login_Doctor.driver, logger,"Age").isDisplayed();
		bill.getEmrViewBillSerachResultColumnText(Login_Doctor.driver, logger,"Doctor").isDisplayed();
		bill.getEmrViewBillSerachResultColumnText(Login_Doctor.driver, logger,"Net Amount").isDisplayed();
		bill.getEmrViewBillSerachResultColumnText(Login_Doctor.driver, logger,"Action").isDisplayed();
	}
	public synchronized void searchBillButtonClick() {
		bill.getEmrViewBillSearchBillButton(Login_Doctor.driver, logger).isEnabled();
		bill.getEmrViewBillSearchBillButton(Login_Doctor.driver, logger).click();
	}
	public synchronized void viewBillResultPrintValidation() throws Exception {
		bill.getEmrViewBillPatientNameSearchTextBox(Login_Doctor.driver, logger).sendKeys("Ballu");
	    searchBillButtonClick();
	    viewBillSearchResultColumnDisplayValidation();
		boolean check=true;
		List<WebElement> eleSearchResultPrint;
		 while(check)
		    {
			 eleSearchResultPrint=bill.getEmrViewBillSearchResultPrintList(Login_Doctor.driver, logger);
			 for(WebElement op:eleSearchResultPrint)
			 {
				 op.isDisplayed();
			 }
			Web_GeneralFunctions.scrollToElement(bill.getEmrViewBillSearchResultNextButton(Login_Doctor.driver,logger),Login_Doctor.driver);
			if(bill.getEmrViewBillSearchResultNextButton(Login_Doctor.driver,logger).getAttribute("class").trim().contains("disabled"))
			{
				check=false;
				eleSearchResultPrint.get(0).click();	
				Web_GeneralFunctions.wait(4);
				pdfValidation();
				
			}
			else
			{
				bill.getEmrViewBillSearchResultNextButton(Login_Doctor.driver, logger).click();
			}
			}
		}
	public synchronized void viewBillResultCancelValidation() throws Exception {
		bill.getEmrViewBillPatientNameSearchTextBox(Login_Doctor.driver, logger).sendKeys("Ballu");
	    searchBillButtonClick();
	    viewBillSearchResultColumnDisplayValidation();
	    String expectedCancelMessage="Are you sure, you want to cancel this bill?";
		boolean check=true;
		boolean done=true;
		List<WebElement> eleSearchResultCancelled;
		List<WebElement> eleSearchResultPrint;
		 while(check)
		    {
			 eleSearchResultCancelled=bill.getEmrViewBillSearchResultCancelList(Login_Doctor.driver, logger);
			 eleSearchResultPrint=bill.getEmrViewBillSearchResultPrintList(Login_Doctor.driver, logger);
			 for(int i=0;i<eleSearchResultCancelled.size();i++)
				{
					if(eleSearchResultCancelled.get(i).getText().trim().equals("Cancel"))
					{
						eleSearchResultCancelled.get(i).click();
						Web_GeneralFunctions.wait(4);
						String actualCancelMessage=bill.getEmrViewBillCancelMessage(Login_Doctor.driver, logger).getText();
						Assert.assertTrue(actualCancelMessage.equals(expectedCancelMessage),"Actual Message not equal to Expected");
						bill.getEmrViewBillCancelYesButton(Login_Doctor.driver, logger).click();
						String cancelledSuccessMessage = bill.getEmrBillErrorMessage(Login_Doctor.driver, logger).getText();
						Assert.assertTrue("Bill Cancelled Successfully".equals(cancelledSuccessMessage),"Actual Message not equal to Expected");
						Web_GeneralFunctions.wait(4);
						List<WebElement> eleSearchResultCancelled1=bill.getEmrViewBillSearchResultCancelList(Login_Doctor.driver, logger);
						Assert.assertTrue("Cancelled".equals(eleSearchResultCancelled1.get(i).getText().trim()),"Cancelled text not matching after cancel");
						eleSearchResultPrint.get(i).click();
						Web_GeneralFunctions.wait(4);
					String pdfContent=pdfValidation();
					Assert.assertTrue(pdfContent.contains("Cancelled"),"Cancelled text not matching after cancel In pdf");
					done=false;
						break;
					}
				}
			 if(done==false)
			 {
				 break;
			 }

			Web_GeneralFunctions.scrollToElement(bill.getEmrViewBillSearchResultNextButton(Login_Doctor.driver,logger),Login_Doctor.driver);
			if(bill.getEmrViewBillSearchResultNextButton(Login_Doctor.driver,logger).getAttribute("class").trim().contains("disabled"))
			{
				check=false;	
			}
			else
			{
				bill.getEmrViewBillSearchResultNextButton(Login_Doctor.driver, logger).click();
			}
			}
		 }
	public synchronized void viewBillResultCancelledValidation() throws Exception {
		bill.getEmrViewBillPatientNameSearchTextBox(Login_Doctor.driver, logger).sendKeys("Ballu");
	    searchBillButtonClick();
	    viewBillSearchResultColumnDisplayValidation();
		boolean check=true;
		List<WebElement> eleSearchResultCancelled;
		 while(check)
		    {
			 eleSearchResultCancelled=bill.getEmrViewBillSearchResultCancelList(Login_Doctor.driver, logger);
			 for(WebElement cancelled:eleSearchResultCancelled)
				{
					if(cancelled.getText().trim().equals("Cancelled"))
					{
					    Assert.assertTrue(cancelled.getAttribute("disabled").trim().equals("true"),"Cancelled Button not disabled");
					}
				}

			Web_GeneralFunctions.scrollToElement(bill.getEmrViewBillSearchResultNextButton(Login_Doctor.driver,logger),Login_Doctor.driver);
			if(bill.getEmrViewBillSearchResultNextButton(Login_Doctor.driver,logger).getAttribute("class").trim().contains("disabled"))
			{
				check=false;	
			}
			else
			{
				bill.getEmrViewBillSearchResultNextButton(Login_Doctor.driver, logger).click();
			}
			}
				
		}
	public synchronized void billPageDoctorAndPatientFieldValidation() throws Exception {
		Assert.assertTrue((bill.getEmrCreateBillPatientTextBox(Login_Doctor.driver, logger).getAttribute("disabled")
				.trim().equals("true")), "Patient Text Box editable");
		Assert.assertTrue((bill.getEmrCreateBillPatientTextBox(Login_Doctor.driver, logger).getAttribute("value").trim().equals(patientName)), "Patient name not matching");
		Assert.assertTrue((bill.getEmrCreateBillDoctorTextBox(Login_Doctor.driver, logger).getAttribute("readonly")
				.trim().equals("true")), "Doctor Text Box editable");
		Assert.assertTrue((bill.getEmrCreateBillDoctorTextBox(Login_Doctor.driver, logger).getAttribute("value")
				.trim().equals(doctorName)), "Doctor name not matching");
		}
	public synchronized void billDefaultServiceValidation() throws Exception {
		String actualDefaultService="MeetDoctor";
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "service", rowCount).getAttribute("value")
				.trim().equals(actualDefaultService)), "Default Service not matching");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "tariff", rowCount).getAttribute("value")
				.trim().equals("500.00")), "Deafult Tariff not matching");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "quantity", rowCount).getAttribute("value")
				.trim().equals("1")), "Default Quantity not matching");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "discountPercent", rowCount).getAttribute("value")
				.trim().equals("0.00")), "Default Discount Percntage not matching");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "discountAmount", rowCount).getAttribute("value")
				.trim().equals("0.00")), "Default Discount Amount not matching");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "appDiscountAmount", rowCount).getAttribute("value")
				.trim().equals("0")), "Default appDiscount Amount not matching");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "netAmount", rowCount).getAttribute("value")
				.trim().equals("500.00")), "Default Discount netAmount not matching");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "appDiscountAmount", rowCount).getAttribute("readonly")
				.trim().equals("true")), "appDiscount Amount editable");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "netAmount", rowCount).getAttribute("readonly")
				.trim().equals("true")), " netAmount editable");
		
		}
	public synchronized void billDefaultServiceLevelChangeValidation() throws Exception {
		
	  bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "tariff", rowCount).clear();
	  bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "tariff", rowCount).sendKeys("400");
	  bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "quantity", rowCount).clear();
	  bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "quantity", rowCount).sendKeys("2");
	  bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "discountPercent", rowCount).clear();
	  bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "discountPercent", rowCount).sendKeys("20");
	  bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "discountAmount", rowCount).clear();
	  bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "discountAmount", rowCount).sendKeys("250");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "appDiscountAmount", rowCount).getAttribute("readonly")
				.trim().equals("true")), "appDiscount Amount editable");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "netAmount", rowCount).getAttribute("readonly")
				.trim().equals("true")), " netAmount editable");
		
		}
	public synchronized void emrAppointmentGenerateBillValidation() throws Exception
	 {
		Web_GeneralFunctions.wait(4);
		expectedNetTotalAmount=0;
	  generateBillButtonClick();
		String errorMessage = bill.getEmrBillErrorMessage(Login_Doctor.driver, logger).getText();
		Assert.assertTrue("Please select a service".equals(errorMessage.trim()), "Actual message not expected");
		 bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "service",rowCount).clear();
		bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "service", rowCount)
				.sendKeys("invalid data");
		String noServiceFound = bill.getEmrCreateBillServiceSearchMatchingServiceFoundText(Login_Doctor.driver, logger)
				.getText();
		Assert.assertTrue("No matching service found".equals(noServiceFound.trim()),
				"Actual No Service Found not matched with expected");
		bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "service", rowCount).clear();
		bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "service", rowCount).sendKeys("service");
		List<WebElement> eleServiceSearchResult = bill.getEmrCreateBillServiceList(Login_Doctor.driver, logger);
		eleServiceSearchResult.get(0).click();
		 service = bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "service",rowCount)
				.getAttribute("value");
		tariffValidation();
		quantityValidation();
		discountPercentageValidation();
		discountAmountValidation();
		addNetTotalAmount();
		addService();
		rowCount++;
		Web_GeneralFunctions.wait(4);
		 bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "service",rowCount)
				.sendKeys("Test Service");
		bill.getEmrCreateBillService(Login_Doctor.driver, logger, "Test Service",rowCount-1).click();
		String errorMessage1 = bill.getEmrBillErrorMessage(Login_Doctor.driver, logger).getText();
		Assert.assertTrue(("Duplicate service").equals(errorMessage1.trim()), "Actual message not expected");
		bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "service",rowCount).clear();
		bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "service",rowCount)
				.sendKeys("Lab service");
		bill.getEmrCreateBillService(Login_Doctor.driver, logger, "Lab service",rowCount-1).click();
		 tariffValidation();
		 quantityValidation();
		 discountPercentageValidation();
		 discountAmountValidation();
		 addNetTotalAmount();
		 subNetTotalAmount();
		 rowCount--;
		 deleteService();
		 serviceTotalValidation();
		 additionalDiscountAndGrandTotalValidation();
		 grandTotal= getGrandTotal();
		 billingCreateBillPaymentValidation();
	   }
	
	public synchronized void viewBillSearchResultColumnNetAmountValidation(String parameter,int column) throws Exception {
	    int page=1;
	    double previousAmount=0;
	    boolean check=true;
	    bill.getEmrViewBillSerachResultColumnText(Login_Doctor.driver, logger,"Net Amount").click();
	    while(check)
	    {
		int x=	bill.getEmrViewBillSearchResultRowList(Login_Doctor.driver, logger).size();
		for(int i=1;i<=x;i++)
		{
			Assert.assertTrue(bill.getEmrViewBillSearchResultColumnVaildation(Login_Doctor.driver, logger,i,column).getText().trim().contains(parameter)," Search Result not Matching");
			String netAmountText=bill.getEmrViewBillSearchResultColumnVaildation(Login_Doctor.driver, logger,i,7).getText().trim();
			String[] a=netAmountText.split("\\s+",3);
			Assert.assertTrue(a[0].trim().equals("Rs."),"Contents Not Matching Rs.");
			double amount=Double.parseDouble(a[1].trim());
			if(amount>0)
			{
				if(amount>=previousAmount)
				{
					previousAmount=amount;
				}
				else
				{
					Assert.assertTrue(false,"sorting not in order according to Amount "+page+" at "+i+"row");
				}
			String paymentMode=a[2].trim();
			if(!(paymentMode.contains("Cash")||paymentMode.contains("Credit card")||paymentMode.contains("Wallet")||paymentMode.contains("Rewards")||paymentMode.contains("Online")||paymentMode.contains("Package")||paymentMode.contains("Coupon")||paymentMode.contains("Debit card")))
			{
				Assert.assertTrue(false,"PaymentMode not present in "+page+" page at "+i+"row");
			}
		}
		}
		Web_GeneralFunctions.scrollToElement(bill.getEmrViewBillSearchResultNextButton(Login_Doctor.driver,logger),Login_Doctor.driver);
		if(bill.getEmrViewBillSearchResultNextButton(Login_Doctor.driver,logger).getAttribute("class").trim().contains("disabled"))
		{
			check=false;
		}
		else
		{
			bill.getEmrViewBillSearchResultNextButton(Login_Doctor.driver, logger).click();
			page++;
		}
		}
		
		
	}
	public synchronized void viewBillSearchAllfieldDisplayValidation() throws Exception {
		  bill.getEmrViewBillSearchText(Login_Doctor.driver,logger,"Patient Name").isDisplayed();
		  bill.getEmrViewBillSearchText(Login_Doctor.driver,logger,"Age").isDisplayed();
		  bill.getEmrViewBillSearchText(Login_Doctor.driver,logger,"Gender").isDisplayed();
		  bill.getEmrViewBillSearchText(Login_Doctor.driver,logger,"UHID").isDisplayed();
		  bill.getEmrViewBillSearchText(Login_Doctor.driver,logger,"Clinic ID").isDisplayed();
		  bill.getEmrViewBillSearchText(Login_Doctor.driver,logger,"From Date").isDisplayed();
		  bill.getEmrViewBillSearchText(Login_Doctor.driver,logger,"To Date").isDisplayed();
		  bill.getEmrViewBillSearchText(Login_Doctor.driver,logger,"Mobile Number").isDisplayed();
		  bill.getEmrViewBillSearchText(Login_Doctor.driver,logger,"Govt ID").isDisplayed();
		  bill.getEmrViewBillSearchText(Login_Doctor.driver,logger,"Bill Type").isDisplayed();
		  bill.getEmrViewBillSearchText(Login_Doctor.driver,logger,"Select Doctor").isDisplayed();
	}
	public synchronized void billSaveServiceValidation() throws Exception {
		rowCount=0;
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "service", rowCount).getAttribute("value")
				.trim().equals(service)), "Service not matching");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "tariff", rowCount).getAttribute("value")
				.trim().equals("348.72")), "Tariff not matching");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "quantity", rowCount).getAttribute("value")
				.trim().equals("2")), "Quantity not matching");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "discountPercent", rowCount).getAttribute("value")
				.trim().equals("2.87")), "Discount Percntage not matching");
		//Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "discountAmount", rowCount).getAttribute("value")
				//.trim().equals("20.02")), "Discount Amount not matching");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "appDiscountAmount", rowCount).getAttribute("value")
				.trim().equals("0")), "appDiscount Amount not matching");
		//Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "netAmount", rowCount).getAttribute("value")
				//.trim().equals("677.42")), "Discount netAmount not matching");
		
		}
	public synchronized void emrAppointmentGenerateBillSaveDraftValidation() throws Exception
	 {
		Web_GeneralFunctions.wait(4);
	  generateBillButtonClick();
		String errorMessage = bill.getEmrBillErrorMessage(Login_Doctor.driver, logger).getText();
		Assert.assertTrue("Please select a service".equals(errorMessage.trim()), "Actual message not expected");
		 bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "service",rowCount).clear();
		bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "service", rowCount)
				.sendKeys("invalid data");
		String noServiceFound = bill.getEmrCreateBillServiceSearchMatchingServiceFoundText(Login_Doctor.driver, logger)
				.getText();
		Assert.assertTrue("No matching service found".equals(noServiceFound.trim()),
				"Actual No Service Found not matched with expected");
		bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "service", rowCount).clear();
		bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "service", rowCount).sendKeys("service");
		List<WebElement> eleServiceSearchResult = bill.getEmrCreateBillServiceList(Login_Doctor.driver, logger);
		eleServiceSearchResult.get(0).click();
		Web_GeneralFunctions.wait(4);
		 service = bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "service",rowCount)
				.getAttribute("value").trim();
		tariffValidation();
		quantityValidation();
		discountPercentageValidation();
		discountAmountValidation();
		addNetTotalAmount();
		addService();
		rowCount++;
		Web_GeneralFunctions.wait(5);
		 bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "service",rowCount)
				.sendKeys("Test Service");
		bill.getEmrCreateBillService(Login_Doctor.driver, logger, "Test Service",rowCount-1).click();
		String errorMessage1 = bill.getEmrBillErrorMessage(Login_Doctor.driver, logger).getText();
		Assert.assertTrue(("Duplicate service").equals(errorMessage1.trim()), "Actual message not expected");
		bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "service",rowCount).clear();
		bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "service",rowCount)
				.sendKeys("Lab service");
		bill.getEmrCreateBillService(Login_Doctor.driver, logger, "Lab service",rowCount-1).click();
		 tariffValidation();
		 quantityValidation();
		 discountPercentageValidation();
		 discountAmountValidation();
		 addNetTotalAmount();
		 subNetTotalAmount();
		 rowCount--;
		 deleteService();
	   }
	
	public synchronized void paymentRemoveSelectedCashWalletOption() throws Exception {
			bill.getEmrCreateBillPayementOption(Login_Doctor.driver, logger, "Cash").click();
			bill.getEmrCreateBillPayementOption(Login_Doctor.driver, logger, "Wallets").click();
		}
	public synchronized void paymentRemoveSelectedCashCardOption() throws Exception {
		bill.getEmrCreateBillPayementOption(Login_Doctor.driver, logger, "Cash").click();
		bill.getEmrCreateBillPayementOption(Login_Doctor.driver, logger, "Card").click();
	}
	public synchronized void paymentRemoveSelectedCardOption() throws Exception {
		bill.getEmrCreateBillPayementOption(Login_Doctor.driver, logger, "Card").click();
	}
	public synchronized void paymentRemoveSelectedWalletOption() throws Exception {
		bill.getEmrCreateBillPayementOption(Login_Doctor.driver, logger, "Wallets").click();
	}
	public synchronized void paymentRemoveSelectedCashOption() throws Exception {
		bill.getEmrCreateBillPayementOption(Login_Doctor.driver, logger, "Cash").click();
	}
	public synchronized void viewBillSearchResultBillSearchCancel() throws Exception {
		String expectedCancelMessage="Are you sure, you want to cancel this bill?";
	    boolean check=true;
	    bill.getEmrViewBillSerachResultColumnText(Login_Doctor.driver, logger,"Net Amount").click();
	    while(check)
	    {
		int x=	bill.getEmrViewBillSearchResultRowList(Login_Doctor.driver, logger).size();
		List<WebElement> eleSearchResultCancel=bill.getEmrViewBillSearchResultCancelList(Login_Doctor.driver, logger);
		for(int i=1;i<=x;i++)
		{
			if(bill.getEmrViewBillSearchResultColumnVaildation(Login_Doctor.driver, logger,i,1).getText().trim().equals(String.valueOf(billNumber)))
			{
				String netAmountText=bill.getEmrViewBillSearchResultColumnVaildation(Login_Doctor.driver, logger,i,7).getText().trim();
				String[] a=netAmountText.split("\\s+",3);
				Assert.assertTrue(a[0].trim().equals("Rs."),"Contents Not Matching Rs.");
			//	Assert.assertTrue(a[1].trim().equals(grandTotal),"Grand Total Not Matching");
				Assert.assertTrue(a[2].trim().equals("Cash, Wallet"),"Payment Mode Not Matching");
				Assert.assertTrue(eleSearchResultCancel.get(i-1).getText().trim().equals("Cancel"),"Cancel not matching");
				eleSearchResultCancel.get(i-1).click();
				Web_GeneralFunctions.wait(4);
				String actualCancelMessage=bill.getEmrViewBillCancelMessage(Login_Doctor.driver, logger).getText();
				Assert.assertTrue(actualCancelMessage.equals(expectedCancelMessage),"Actual Message not equal to Expected");
				bill.getEmrViewBillCancelYesButton(Login_Doctor.driver, logger).click();
				String cancelledSuccessMessage = bill.getEmrBillErrorMessage(Login_Doctor.driver, logger).getText();
				Assert.assertTrue("Bill Cancelled Successfully".equals(cancelledSuccessMessage),"Actual Message not equal to Expected");
				Web_GeneralFunctions.wait(4);
				List<WebElement> eleSearchResultCancelled1=bill.getEmrViewBillSearchResultCancelList(Login_Doctor.driver, logger);
			    Assert.assertTrue("Cancelled".equals(eleSearchResultCancelled1.get(i-1).getText().trim()),"Cancelled text not matching after cancel");
			    check=false;
			    break;
			}
			
		}
		if(check==false)
		{
			break;
		}
		Web_GeneralFunctions.scrollToElement(bill.getEmrViewBillSearchResultNextButton(Login_Doctor.driver,logger),Login_Doctor.driver);
		if(bill.getEmrViewBillSearchResultNextButton(Login_Doctor.driver,logger).getAttribute("class").trim().contains("disabled"))
		{
			check=false;
		}
		else
		{
			bill.getEmrViewBillSearchResultNextButton(Login_Doctor.driver, logger).click();
		}
		}
	}
	public synchronized void clickGenerateButtonAndClosePopUp() throws Exception {
		bill.getEmrCreateBillPayementOption(Login_Doctor.driver, logger, "Cash").click();
		generateBillButtonClick();
		Web_GeneralFunctions.wait(4);
		bill.getEmrGenerateBillCloseButton(Login_Doctor.driver,logger).click();
		Web_GeneralFunctions.wait(4);
		
	}
	public synchronized void billConsumerServiceReadOnlyValiadtion() throws Exception {
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "service", rowCount).getAttribute("readonly")
				.trim().equals("true")), "Service Editable");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "tariff", rowCount).getAttribute("readonly")
				.trim().equals("true")), "Tariff Editable");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "quantity", rowCount).getAttribute("readonly")
				.trim().equals("true")), "Quantity Editable");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "discountPercent", rowCount).getAttribute("readonly")
				.trim().equals("true")), "Discount Percntage Editable");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "discountAmount", rowCount).getAttribute("readonly")
				.trim().equals("true")), "Discount Amount Editable");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "appDiscountAmount", rowCount).getAttribute("readonly")
				.trim().equals("true")), "appDiscount Amount Editable");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "netAmount", rowCount).getAttribute("readonly")
				.trim().equals("true")), "Discount netAmount Editable");
		
		}
	
	public synchronized void consumerAppointmentGenerateBillValidation() throws Exception
	 {
		expectedNetTotalAmount=0;
		Web_GeneralFunctions.wait(4);
		addNetTotalAmount();
	     rowCount++;
	     addService();
		 bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "service",rowCount).clear();
		bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "service", rowCount)
				.sendKeys("invalid data");
		String noServiceFound = bill.getEmrCreateBillServiceSearchMatchingServiceFoundText(Login_Doctor.driver, logger)
				.getText();
		Assert.assertTrue("No matching service found".equals(noServiceFound.trim()),
				"Actual No Service Found not matched with expected");
		bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "service", rowCount).clear();
		bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "service", rowCount).sendKeys("service");
		List<WebElement> eleServiceSearchResult = bill.getEmrCreateBillServiceList(Login_Doctor.driver, logger);
		eleServiceSearchResult.get(0).click();
		 service = bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "service",rowCount)
				.getAttribute("value");
		tariffValidation();
		quantityValidation();
		discountPercentageValidation();
		discountAmountValidation();
		addNetTotalAmount();
		addService();
		Web_GeneralFunctions.wait(4);
		 bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "service",rowCount)
				.sendKeys("Test Service");
		bill.getEmrCreateBillService(Login_Doctor.driver, logger, "Test Service",rowCount-2).click();
		String errorMessage1 = bill.getEmrBillErrorMessage(Login_Doctor.driver, logger).getText();
		Assert.assertTrue(("Duplicate service").equals(errorMessage1.trim()), "Actual message not expected");
		bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "service",rowCount).clear();
		bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "service",rowCount)
				.sendKeys("Lab service");
		bill.getEmrCreateBillService(Login_Doctor.driver, logger, "Lab service",rowCount-2).click();
		 tariffValidation();
		 quantityValidation();
		 discountPercentageValidation();
		 discountAmountValidation();
		 addNetTotalAmount();
		 subNetTotalAmount();
		 rowCount--;
		 rowCount--;
		 deleteService();
		 serviceTotalValidation();
		 additionalDiscountAndGrandTotalValidation();
		 grandTotal= getGrandTotal();
		 billingCreateBillPaymentValidation();
	   }
	
	public synchronized void billConsumerOServiceValidation() throws Exception {
		double expectedServiceAmount=ConsumerAppointmentTest.serviceAmount;
		double actualServiceAmount=Double.parseDouble(bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "tariff", rowCount).getAttribute("value").trim());
		String expectedService="MeetDoctor";
		service=expectedService;
		String paid=ConsumerAppointmentTest.Paid;
		 String online=paid.substring(0,paid.lastIndexOf('/')).trim();
		 double expectedPaidOnline=Double.parseDouble(online);
		 double expectedAppDiscount=0;
		 double actualNetAmount=Double.parseDouble(bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "netAmount", rowCount).getAttribute("value").trim());
		 double actualpaidOnline = Double
					.parseDouble(bill.getEmrCreateBillPaidOnlineTextBox(Login_Doctor.driver, logger).getAttribute("value").trim());
			double actualamountPayable = Double.parseDouble(
					bill.getEmrCreateBillAmountPayableTextBox(Login_Doctor.driver, logger).getAttribute("value").trim());
			double actualGrandTotal = Double.parseDouble(
					bill.getEmrCreateBillGrandTotalTextBox(Login_Doctor.driver, logger).getAttribute("value").trim());
			double actualAppDiscount = Double.parseDouble(
					bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "appDiscountAmount", rowCount).getAttribute("value").trim());
			double expectedNetAmount=expectedServiceAmount-expectedAppDiscount;
			double expectedamountPayable=expectedNetAmount-expectedPaidOnline;
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "service", rowCount).getAttribute("value")
				.trim().equals(expectedService)), "Booked Service not matching");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "quantity", rowCount).getAttribute("value")
				.trim().equals("1")), "Booked Quantity not matching");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "discountPercent", rowCount).getAttribute("value")
				.trim().equals("0.00")), "Booked Discount Percntage not matching");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "discountAmount", rowCount).getAttribute("value")
				.trim().equals("0.00")), "Booked Discount Amount not matching");
		Assert.assertTrue((bill.getEmrCreateBillAmountPayableTextBox(Login_Doctor.driver, logger).getAttribute("readonly").trim().equals("true")), "Amount Payable editable");
		Assert.assertTrue((bill.getEmrCreateBillPaidOnlineTextBox(Login_Doctor.driver, logger).getAttribute("readonly").trim().equals("true")), "Grand Total editable");
		Assert.assertTrue((bill.getEmrCreateBillGrandTotalTextBox(Login_Doctor.driver, logger).getAttribute("readonly").trim().equals("true")), "Grand Total editable");
		
		if(actualpaidOnline!=expectedPaidOnline)
		{
			Assert.assertTrue(false,"Paid online not matching");
		}
		if(actualNetAmount!=expectedNetAmount)
		{
			Assert.assertTrue(false," Net Amount not matching");
		}
		if(actualServiceAmount!=expectedServiceAmount)
		{
			Assert.assertTrue(false,"Service or Tariff Amount not matching");
		}
		if(actualNetAmount!=actualGrandTotal)
		{
			Assert.assertTrue(false,"Grand Total not Correct");
		}
		if(actualAppDiscount!=expectedAppDiscount)
		{
			Assert.assertTrue(false,"App Discount not correct");
		}
		if(actualamountPayable!=expectedamountPayable)
		{
			Assert.assertTrue(false,"Amount Payable not Correct");
		}
		}
	
	public synchronized void viewBillSearchResultValiadtion(String paymentMode) throws Exception {
	    boolean check=true;
	    bill.getEmrViewBillSerachResultColumnText(Login_Doctor.driver, logger,"Net Amount").click();
	    
	    List<WebElement> eleSearchResultPrint;
	    while(check)
	    {
		int x=	bill.getEmrViewBillSearchResultRowList(Login_Doctor.driver, logger).size();
		eleSearchResultPrint=bill.getEmrViewBillSearchResultPrintList(Login_Doctor.driver, logger);
		for(int i=1;i<=x;i++)
		{
			if(bill.getEmrViewBillSearchResultColumnVaildation(Login_Doctor.driver, logger,i,1).getText().trim().equals(String.valueOf(billNumber)))
			{
				String netAmountText=bill.getEmrViewBillSearchResultColumnVaildation(Login_Doctor.driver, logger,i,7).getText().trim();
				String[] a=netAmountText.split("\\s+",3);
				Assert.assertTrue(a[0].trim().equals("Rs."),"Contents Not Matching Rs.");
			//	Assert.assertTrue(a[1].trim().equals(grandTotal),"Grand Total Not Matching");
				Assert.assertTrue(a[2].trim().equals(paymentMode),"Payment Mode Not Matching in view Bill");
				eleSearchResultPrint.get(i-1).click();	
				Web_GeneralFunctions.wait(4);
				String viewPdf=pdfValidation().trim();
				Assert.assertTrue(pdf.equals(viewPdf),"View Pdf not matching with Actual Pdf");
			    check=false;
			    break;
			}
			
		}
		if(check==false)
		{
			break;
		}
		Web_GeneralFunctions.scrollToElement(bill.getEmrViewBillSearchResultNextButton(Login_Doctor.driver,logger),Login_Doctor.driver);
		if(bill.getEmrViewBillSearchResultNextButton(Login_Doctor.driver,logger).getAttribute("class").trim().contains("disabled"))
		{
			check=false;
		}
		else
		{
			bill.getEmrViewBillSearchResultNextButton(Login_Doctor.driver, logger).click();
		}
		}
	}
	
	public synchronized void billConsumerPcServiceValidation() throws Exception {
		double expectedServiceAmount=ConsumerAppointmentTest.serviceAmount;
		double actualServiceAmount=Double.parseDouble(bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "tariff", rowCount).getAttribute("value").trim());
		String expectedService="MeetDoctor";
		service=expectedService;
		String paid=ConsumerAppointmentTest.Paid;
		 String payClinic=paid.substring(0,paid.lastIndexOf('/')).trim();
		 double expectedPaidOnline=0;
		 double expectedAppDiscount=0;
		 double actualNetAmount=Double.parseDouble(bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "netAmount", rowCount).getAttribute("value").trim());
		 double actualpaidOnline = Double
					.parseDouble(bill.getEmrCreateBillPaidOnlineTextBox(Login_Doctor.driver, logger).getAttribute("value").trim());
			double actualamountPayable = Double.parseDouble(
					bill.getEmrCreateBillAmountPayableTextBox(Login_Doctor.driver, logger).getAttribute("value").trim());
			double actualGrandTotal = Double.parseDouble(
					bill.getEmrCreateBillGrandTotalTextBox(Login_Doctor.driver, logger).getAttribute("value").trim());
			double actualAppDiscount = Double.parseDouble(
					bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "appDiscountAmount", rowCount).getAttribute("value").trim());
			double expectedNetAmount=expectedServiceAmount-expectedAppDiscount;
			double expectedamountPayable=Double.parseDouble(payClinic);
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "service", rowCount).getAttribute("value")
				.trim().equals(expectedService)), "Booked Service not matching");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "quantity", rowCount).getAttribute("value")
				.trim().equals("1")), "Booked Quantity not matching");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "discountPercent", rowCount).getAttribute("value")
				.trim().equals("0.00")), "Booked Discount Percntage not matching");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "discountAmount", rowCount).getAttribute("value")
				.trim().equals("0.00")), "Booked Discount Amount not matching");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "appDiscountAmount", rowCount).getAttribute("readonly")
				.trim().equals("true")), "appDiscount Amount editable");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "netAmount", rowCount).getAttribute("readonly")
				.trim().equals("true")), " netAmount editable");
		Assert.assertTrue((bill.getEmrCreateBillAmountPayableTextBox(Login_Doctor.driver, logger).getAttribute("readonly").trim().equals("true")), "Amount Payable editable");
		Assert.assertTrue((bill.getEmrCreateBillPaidOnlineTextBox(Login_Doctor.driver, logger).getAttribute("readonly").trim().equals("true")), "Paid Online editable");
		Assert.assertTrue((bill.getEmrCreateBillGrandTotalTextBox(Login_Doctor.driver, logger).getAttribute("readonly").trim().equals("true")), "Grand Total editable");
		
		if(actualpaidOnline!=expectedPaidOnline)
		{
			Assert.assertTrue(false,"Paid online not matching");
		}
		if(actualNetAmount!=expectedNetAmount)
		{
			Assert.assertTrue(false," Net Amount not matching");
		}
		if(actualServiceAmount!=expectedServiceAmount)
		{
			Assert.assertTrue(false,"Service or Tariff Amount not matching");
		}
		if(expectedamountPayable!=actualGrandTotal)
		{
			Assert.assertTrue(false,"Grand Total not Correct");
		}
		if(actualAppDiscount!=expectedAppDiscount)
		{
			Assert.assertTrue(false,"App Discount not correct");
		}
		if(actualamountPayable!=expectedamountPayable)
		{
			Assert.assertTrue(false,"Amount Payable not Correct");
		}
		}
	public synchronized void billConsumerPServiceValidation() throws Exception {
		double expectedServiceAmount=ConsumerAppointmentTest.serviceAmount;
		double actualServiceAmount=Double.parseDouble(bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "tariff", rowCount).getAttribute("value").trim());
		String expectedService="MeetDoctor";
		service=expectedService;
		String actualBenefitText=bill.getEmrCreateBillAppliedBenefitText(Login_Doctor.driver, logger).getText().trim();
		String expectedBenefitText="Package Applied -";
		String paid=ConsumerAppointmentTest.Paid;
		String pack=paid.substring(0,paid.lastIndexOf('/')).trim();
		 double expectedPaidOnline=0;
		 double expectedAppDiscount=Double.parseDouble(pack);
		 double actualNetAmount=Double.parseDouble(bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "netAmount", rowCount).getAttribute("value").trim());
		 double actualpaidOnline = Double
					.parseDouble(bill.getEmrCreateBillPaidOnlineTextBox(Login_Doctor.driver, logger).getAttribute("value").trim());
			double actualamountPayable = Double.parseDouble(
					bill.getEmrCreateBillAmountPayableTextBox(Login_Doctor.driver, logger).getAttribute("value").trim());
			double actualGrandTotal = Double.parseDouble(
					bill.getEmrCreateBillGrandTotalTextBox(Login_Doctor.driver, logger).getAttribute("value").trim());
			double actualAppDiscount = Double.parseDouble(
					bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "appDiscountAmount", rowCount).getAttribute("value").trim());
			double expectedNetAmount=expectedServiceAmount-expectedAppDiscount;
			double expectedamountPayable=expectedNetAmount-expectedPaidOnline;
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "service", rowCount).getAttribute("value")
				.trim().equals(expectedService)), "Booked Service not matching");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "quantity", rowCount).getAttribute("value")
				.trim().equals("1")), "Booked Quantity not matching");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "discountPercent", rowCount).getAttribute("value")
				.trim().equals("0.00")), "Booked Discount Percntage not matching");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "discountAmount", rowCount).getAttribute("value")
				.trim().equals("0.00")), "Booked Discount Amount not matching");
		Assert.assertTrue(actualBenefitText.contains(expectedBenefitText),"Benefit Text not matching");
		Assert.assertTrue((bill.getEmrCreateBillAmountPayableTextBox(Login_Doctor.driver, logger).getAttribute("readonly").trim().equals("true")), "Amount Payable editable");
		Assert.assertTrue((bill.getEmrCreateBillPaidOnlineTextBox(Login_Doctor.driver, logger).getAttribute("readonly").trim().equals("true")), "Grand Total editable");
		Assert.assertTrue((bill.getEmrCreateBillGrandTotalTextBox(Login_Doctor.driver, logger).getAttribute("readonly").trim().equals("true")), "Grand Total editable");
		
		if(actualpaidOnline!=expectedPaidOnline)
		{
			Assert.assertTrue(false,"Paid online not matching");
		}
		if(actualNetAmount!=expectedNetAmount)
		{
			Assert.assertTrue(false," Net Amount not matching");
		}
		if(actualServiceAmount!=expectedServiceAmount)
		{
			Assert.assertTrue(false,"Service or Tariff Amount not matching");
		}
		if(actualNetAmount!=actualGrandTotal)
		{
			Assert.assertTrue(false,"Grand Total not Correct");
		}
		if(actualAppDiscount!=expectedAppDiscount)
		{
			Assert.assertTrue(false,"App Dicount not correct");
		}
		if(actualamountPayable!=expectedamountPayable)
		{
			Assert.assertTrue(false,"Amount Payable not Correct");
		}
		}
	public synchronized void billConsumerCServiceValidation() throws Exception {
		double expectedServiceAmount=ConsumerAppointmentTest.serviceAmount;
		double actualServiceAmount=Double.parseDouble(bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "tariff", rowCount).getAttribute("value").trim());
		String expectedService="MeetDoctor";
		service=expectedService;
		String actualBenefitText=bill.getEmrCreateBillAppliedBenefitText(Login_Doctor.driver, logger).getText().trim();
		String expectedBenefitText="Coupon Applied -";
		String paid=ConsumerAppointmentTest.Paid;
		String coupon=paid.substring(0,paid.lastIndexOf('/')).trim();
		 double expectedPaidOnline=0;
		 double expectedAppDiscount=Double.parseDouble(coupon);
		 double actualNetAmount=Double.parseDouble(bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "netAmount", rowCount).getAttribute("value").trim());
		 double actualpaidOnline = Double
					.parseDouble(bill.getEmrCreateBillPaidOnlineTextBox(Login_Doctor.driver, logger).getAttribute("value").trim());
			double actualamountPayable = Double.parseDouble(
					bill.getEmrCreateBillAmountPayableTextBox(Login_Doctor.driver, logger).getAttribute("value").trim());
			double actualGrandTotal = Double.parseDouble(
					bill.getEmrCreateBillGrandTotalTextBox(Login_Doctor.driver, logger).getAttribute("value").trim());
			double actualAppDiscount = Double.parseDouble(
					bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "appDiscountAmount", rowCount).getAttribute("value").trim());
			double expectedNetAmount=expectedServiceAmount-expectedAppDiscount;
			double expectedamountPayable=expectedNetAmount-expectedPaidOnline;
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "service", rowCount).getAttribute("value")
				.trim().equals(expectedService)), "Booked Service not matching");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "quantity", rowCount).getAttribute("value")
				.trim().equals("1")), "Booked Quantity not matching");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "discountPercent", rowCount).getAttribute("value")
				.trim().equals("0.00")), "Booked Discount Percntage not matching");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "discountAmount", rowCount).getAttribute("value")
				.trim().equals("0.00")), "Booked Discount Amount not matching");
		Assert.assertTrue(actualBenefitText.contains(expectedBenefitText),"Benefit Text not matching");
		Assert.assertTrue((bill.getEmrCreateBillAmountPayableTextBox(Login_Doctor.driver, logger).getAttribute("readonly").trim().equals("true")), "Amount Payable editable");
		Assert.assertTrue((bill.getEmrCreateBillPaidOnlineTextBox(Login_Doctor.driver, logger).getAttribute("readonly").trim().equals("true")), "Grand Total editable");
		Assert.assertTrue((bill.getEmrCreateBillGrandTotalTextBox(Login_Doctor.driver, logger).getAttribute("readonly").trim().equals("true")), "Grand Total editable");
		
		if(actualpaidOnline!=expectedPaidOnline)
		{
			Assert.assertTrue(false,"Paid online not matching");
		}
		if(actualNetAmount!=expectedNetAmount)
		{
			Assert.assertTrue(false," Net Amount not matching");
		}
		if(actualServiceAmount!=expectedServiceAmount)
		{
			Assert.assertTrue(false,"Service or Tariff Amount not matching");
		}
		if(actualNetAmount!=actualGrandTotal)
		{
			Assert.assertTrue(false,"Grand Total not Correct");
		}
		if(actualAppDiscount!=expectedAppDiscount)
		{
			Assert.assertTrue(false,"App Discount not correct");
		}
		if(actualamountPayable!=expectedamountPayable)
		{
			Assert.assertTrue(false,"Amount Payable not Correct");
		}
		}
	
	public synchronized void billConsumerFServiceValidation() throws Exception {
		double expectedServiceAmount=ConsumerAppointmentTest.serviceAmount;
		double actualServiceAmount=Double.parseDouble(bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "tariff", rowCount).getAttribute("value").trim());
		String expectedService="MeetDoctor";
		service=expectedService;
		String paid=ConsumerAppointmentTest.Paid;
		String fitcoin=paid.substring(0,paid.lastIndexOf('/')).trim();
		 double expectedPaidOnline=Double.parseDouble(fitcoin);
		 double expectedAppDiscount=0;
		 double actualNetAmount=Double.parseDouble(bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "netAmount", rowCount).getAttribute("value").trim());
		 double actualpaidOnline = Double
					.parseDouble(bill.getEmrCreateBillPaidOnlineTextBox(Login_Doctor.driver, logger).getAttribute("value").trim());
			double actualamountPayable = Double.parseDouble(
					bill.getEmrCreateBillAmountPayableTextBox(Login_Doctor.driver, logger).getAttribute("value").trim());
			double actualGrandTotal = Double.parseDouble(
					bill.getEmrCreateBillGrandTotalTextBox(Login_Doctor.driver, logger).getAttribute("value").trim());
			double actualAppDiscount = Double.parseDouble(
					bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "appDiscountAmount", rowCount).getAttribute("value").trim());
			double expectedNetAmount=expectedServiceAmount-expectedAppDiscount;
			double expectedamountPayable=expectedNetAmount-expectedPaidOnline;
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "service", rowCount).getAttribute("value")
				.trim().equals(expectedService)), "Booked Service not matching");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "quantity", rowCount).getAttribute("value")
				.trim().equals("1")), "Booked Quantity not matching");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "discountPercent", rowCount).getAttribute("value")
				.trim().equals("0.00")), "Booked Discount Percntage not matching");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "discountAmount", rowCount).getAttribute("value")
				.trim().equals("0.00")), "Booked Discount Amount not matching");
		Assert.assertTrue((bill.getEmrCreateBillAmountPayableTextBox(Login_Doctor.driver, logger).getAttribute("readonly").trim().equals("true")), "Amount Payable editable");
		Assert.assertTrue((bill.getEmrCreateBillPaidOnlineTextBox(Login_Doctor.driver, logger).getAttribute("readonly").trim().equals("true")), "Grand Total editable");
		Assert.assertTrue((bill.getEmrCreateBillGrandTotalTextBox(Login_Doctor.driver, logger).getAttribute("readonly").trim().equals("true")), "Grand Total editable");
		
		if(actualpaidOnline!=expectedPaidOnline)
		{
			Assert.assertTrue(false,"Paid online not matching");
		}
		if(actualNetAmount!=expectedNetAmount)
		{
			Assert.assertTrue(false," Net Amount not matching");
		}
		if(actualServiceAmount!=expectedServiceAmount)
		{
			Assert.assertTrue(false,"Service or Tariff Amount not matching");
		}
		if(actualNetAmount!=actualGrandTotal)
		{
			Assert.assertTrue(false,"Grand Total not Correct");
		}
		if(actualAppDiscount!=expectedAppDiscount)
		{
			Assert.assertTrue(false,"App Discount not correct");
		}
		if(actualamountPayable!=expectedamountPayable)
		{
			Assert.assertTrue(false,"Amount Payable not Correct");
		}
		}
	public synchronized void billConsumerCOServiceValidation() throws Exception {
		double expectedServiceAmount=ConsumerAppointmentTest.serviceAmount;
		double actualServiceAmount=Double.parseDouble(bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "tariff", rowCount).getAttribute("value").trim());
		String expectedService="MeetDoctor";
		service=expectedService;
		String actualBenefitText=bill.getEmrCreateBillAppliedBenefitText(Login_Doctor.driver, logger).getText().trim();
		String expectedBenefitText="Coupon Applied -";
		String paid=ConsumerAppointmentTest.Paid;
		 String coupon=paid.substring(0,paid.indexOf('/')).trim();
		 String online=paid.substring(paid.indexOf('+')+1,paid.lastIndexOf('/')).trim();
		 double expectedPaidOnline=Double.parseDouble(online);
		 double expectedAppDiscount=Double.parseDouble(coupon);
		 double actualNetAmount=Double.parseDouble(bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "netAmount", rowCount).getAttribute("value").trim());
		 double actualpaidOnline = Double
					.parseDouble(bill.getEmrCreateBillPaidOnlineTextBox(Login_Doctor.driver, logger).getAttribute("value").trim());
			double actualamountPayable = Double.parseDouble(
					bill.getEmrCreateBillAmountPayableTextBox(Login_Doctor.driver, logger).getAttribute("value").trim());
			double actualGrandTotal = Double.parseDouble(
					bill.getEmrCreateBillGrandTotalTextBox(Login_Doctor.driver, logger).getAttribute("value").trim());
			double actualAppDiscount = Double.parseDouble(
					bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "appDiscountAmount", rowCount).getAttribute("value").trim());
			double expectedNetAmount=expectedServiceAmount-expectedAppDiscount;
			double expectedamountPayable=expectedNetAmount-expectedPaidOnline;
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "service", rowCount).getAttribute("value")
				.trim().equals(expectedService)), "Booked Service not matching");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "quantity", rowCount).getAttribute("value")
				.trim().equals("1")), "Booked Quantity not matching");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "discountPercent", rowCount).getAttribute("value")
				.trim().equals("0.00")), "Booked Discount Percntage not matching");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "discountAmount", rowCount).getAttribute("value")
				.trim().equals("0.00")), "Booked Discount Amount not matching");
		Assert.assertTrue(actualBenefitText.contains(expectedBenefitText),"Benefit Text not matching");
		Assert.assertTrue((bill.getEmrCreateBillAmountPayableTextBox(Login_Doctor.driver, logger).getAttribute("readonly").trim().equals("true")), "Amount Payable editable");
		Assert.assertTrue((bill.getEmrCreateBillPaidOnlineTextBox(Login_Doctor.driver, logger).getAttribute("readonly").trim().equals("true")), "Grand Total editable");
		Assert.assertTrue((bill.getEmrCreateBillGrandTotalTextBox(Login_Doctor.driver, logger).getAttribute("readonly").trim().equals("true")), "Grand Total editable");
		
		if(actualpaidOnline!=expectedPaidOnline)
		{
			Assert.assertTrue(false,"Paid online not matching");
		}
		if(actualNetAmount!=expectedNetAmount)
		{
			Assert.assertTrue(false," Net Amount not matching");
		}
		if(actualServiceAmount!=expectedServiceAmount)
		{
			Assert.assertTrue(false,"Service or Tariff Amount not matching");
		}
		if(actualNetAmount!=actualGrandTotal)
		{
			Assert.assertTrue(false,"Grand Total not Correct");
		}
		if(actualAppDiscount!=expectedAppDiscount)
		{
			Assert.assertTrue(false,"App Discount not correct");
		}
		if(actualamountPayable!=expectedamountPayable)
		{
			Assert.assertTrue(false,"Amount Payable not Correct");
		}
		}
	public synchronized void billConsumerCFServiceValidation() throws Exception {
		double expectedServiceAmount=ConsumerAppointmentTest.serviceAmount;
		double actualServiceAmount=Double.parseDouble(bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "tariff", rowCount).getAttribute("value").trim());
		String expectedService="MeetDoctor";
		service=expectedService;
		String actualBenefitText=bill.getEmrCreateBillAppliedBenefitText(Login_Doctor.driver, logger).getText().trim();
		String expectedBenefitText="Coupon Applied -";
		String paid=ConsumerAppointmentTest.Paid;
		 String coupon=paid.substring(0,paid.indexOf('/')).trim();
		 String fitcoin=paid.substring(paid.indexOf('+')+1,paid.lastIndexOf('/')).trim();
		 double expectedPaidOnline=Double.parseDouble(fitcoin);
		 double expectedAppDiscount=Double.parseDouble(coupon);
		 double actualNetAmount=Double.parseDouble(bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "netAmount", rowCount).getAttribute("value").trim());
		 double actualpaidOnline = Double
					.parseDouble(bill.getEmrCreateBillPaidOnlineTextBox(Login_Doctor.driver, logger).getAttribute("value").trim());
			double actualamountPayable = Double.parseDouble(
					bill.getEmrCreateBillAmountPayableTextBox(Login_Doctor.driver, logger).getAttribute("value").trim());
			double actualGrandTotal = Double.parseDouble(
					bill.getEmrCreateBillGrandTotalTextBox(Login_Doctor.driver, logger).getAttribute("value").trim());
			double actualAppDiscount = Double.parseDouble(
					bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "appDiscountAmount", rowCount).getAttribute("value").trim());
			double expectedNetAmount=expectedServiceAmount-expectedAppDiscount;
			double expectedamountPayable=expectedNetAmount-expectedPaidOnline;
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "service", rowCount).getAttribute("value")
				.trim().equals(expectedService)), "Booked Service not matching");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "quantity", rowCount).getAttribute("value")
				.trim().equals("1")), "Booked Quantity not matching");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "discountPercent", rowCount).getAttribute("value")
				.trim().equals("0.00")), "Booked Discount Percntage not matching");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "discountAmount", rowCount).getAttribute("value")
				.trim().equals("0.00")), "Booked Discount Amount not matching");
		Assert.assertTrue(actualBenefitText.contains(expectedBenefitText),"Benefit Text not matching");
		Assert.assertTrue((bill.getEmrCreateBillAmountPayableTextBox(Login_Doctor.driver, logger).getAttribute("readonly").trim().equals("true")), "Amount Payable editable");
		Assert.assertTrue((bill.getEmrCreateBillPaidOnlineTextBox(Login_Doctor.driver, logger).getAttribute("readonly").trim().equals("true")), "Grand Total editable");
		Assert.assertTrue((bill.getEmrCreateBillGrandTotalTextBox(Login_Doctor.driver, logger).getAttribute("readonly").trim().equals("true")), "Grand Total editable");
		
		if(actualpaidOnline!=expectedPaidOnline)
		{
			Assert.assertTrue(false,"Paid online not matching");
		}
		if(actualNetAmount!=expectedNetAmount)
		{
			Assert.assertTrue(false," Net Amount not matching");
		}
		if(actualServiceAmount!=expectedServiceAmount)
		{
			Assert.assertTrue(false,"Service or Tariff Amount not matching");
		}
		if(actualNetAmount!=actualGrandTotal)
		{
			Assert.assertTrue(false,"Grand Total not Correct");
		}
		if(actualAppDiscount!=expectedAppDiscount)
		{
			Assert.assertTrue(false,"App Discount not correct");
		}
		if(actualamountPayable!=expectedamountPayable)
		{
			Assert.assertTrue(false,"Amount Payable not Correct");
		}
		}
	public synchronized void billConsumerCPcServiceValidation() throws Exception {
		double expectedServiceAmount=ConsumerAppointmentTest.serviceAmount;
		double actualServiceAmount=Double.parseDouble(bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "tariff", rowCount).getAttribute("value").trim());
		String expectedService="MeetDoctor";
		service=expectedService;
		String actualBenefitText=bill.getEmrCreateBillAppliedBenefitText(Login_Doctor.driver, logger).getText().trim();
		String expectedBenefitText="Coupon Applied -";
		String paid=ConsumerAppointmentTest.Paid;
		 String coupon=paid.substring(0,paid.indexOf('/')).trim();
		 String payClinic=paid.substring(paid.indexOf('+')+1,paid.lastIndexOf('/')).trim();
		 double expectedPaidOnline=0;
		 double expectedAppDiscount=Double.parseDouble(coupon);
		 double actualNetAmount=Double.parseDouble(bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "netAmount", rowCount).getAttribute("value").trim());
		 double actualpaidOnline = Double
					.parseDouble(bill.getEmrCreateBillPaidOnlineTextBox(Login_Doctor.driver, logger).getAttribute("value").trim());
			double actualamountPayable = Double.parseDouble(
					bill.getEmrCreateBillAmountPayableTextBox(Login_Doctor.driver, logger).getAttribute("value").trim());
			double actualGrandTotal = Double.parseDouble(
					bill.getEmrCreateBillGrandTotalTextBox(Login_Doctor.driver, logger).getAttribute("value").trim());
			double actualAppDiscount = Double.parseDouble(
					bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "appDiscountAmount", rowCount).getAttribute("value").trim());
			double expectedNetAmount=expectedServiceAmount-expectedAppDiscount;
			double expectedamountPayable=Double.parseDouble(payClinic);
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "service", rowCount).getAttribute("value")
				.trim().equals(expectedService)), "Booked Service not matching");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "quantity", rowCount).getAttribute("value")
				.trim().equals("1")), "Booked Quantity not matching");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "discountPercent", rowCount).getAttribute("value")
				.trim().equals("0.00")), "Booked Discount Percntage not matching");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "discountAmount", rowCount).getAttribute("value")
				.trim().equals("0.00")), "Booked Discount Amount not matching");
		Assert.assertTrue(actualBenefitText.contains(expectedBenefitText),"Benefit Text not matching");
		Assert.assertTrue((bill.getEmrCreateBillAmountPayableTextBox(Login_Doctor.driver, logger).getAttribute("readonly").trim().equals("true")), "Amount Payable editable");
		Assert.assertTrue((bill.getEmrCreateBillPaidOnlineTextBox(Login_Doctor.driver, logger).getAttribute("readonly").trim().equals("true")), "Grand Total editable");
		Assert.assertTrue((bill.getEmrCreateBillGrandTotalTextBox(Login_Doctor.driver, logger).getAttribute("readonly").trim().equals("true")), "Grand Total editable");
		
		if(actualpaidOnline!=expectedPaidOnline)
		{
			Assert.assertTrue(false,"Paid online not matching");
		}
		if(actualNetAmount!=expectedNetAmount)
		{
			Assert.assertTrue(false," Net Amount not matching");
		}
		if(actualServiceAmount!=expectedServiceAmount)
		{
			Assert.assertTrue(false,"Service or Tariff Amount not matching");
		}
		if(actualNetAmount!=actualGrandTotal)
		{
			Assert.assertTrue(false,"Grand Total not Correct");
		}
		if(actualAppDiscount!=expectedAppDiscount)
		{
			Assert.assertTrue(false,"App Discount not correct");
		}
		if(actualamountPayable!=expectedamountPayable)
		{
			Assert.assertTrue(false,"Amount Payable not Correct");
		}
		}
	public synchronized void billConsumerPPcServiceValidation() throws Exception {
		double expectedServiceAmount=ConsumerAppointmentTest.serviceAmount;
		double actualServiceAmount=Double.parseDouble(bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "tariff", rowCount).getAttribute("value").trim());
		String expectedService="MeetDoctor";
		service=expectedService;
		String actualBenefitText=bill.getEmrCreateBillAppliedBenefitText(Login_Doctor.driver, logger).getText().trim();
		String expectedBenefitText="Package Applied -";
		String paid=ConsumerAppointmentTest.Paid;
		 String pack=paid.substring(0,paid.indexOf('/')).trim();
		 String payClinic=paid.substring(paid.indexOf('+')+1,paid.lastIndexOf('/')).trim();
		 double expectedPaidOnline=0;
		 double expectedAppDiscount=Double.parseDouble(pack);
		 double actualNetAmount=Double.parseDouble(bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "netAmount", rowCount).getAttribute("value").trim());
		 double actualpaidOnline = Double
					.parseDouble(bill.getEmrCreateBillPaidOnlineTextBox(Login_Doctor.driver, logger).getAttribute("value").trim());
			double actualamountPayable = Double.parseDouble(
					bill.getEmrCreateBillAmountPayableTextBox(Login_Doctor.driver, logger).getAttribute("value").trim());
			double actualGrandTotal = Double.parseDouble(
					bill.getEmrCreateBillGrandTotalTextBox(Login_Doctor.driver, logger).getAttribute("value").trim());
			double actualAppDiscount = Double.parseDouble(
					bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "appDiscountAmount", rowCount).getAttribute("value").trim());
			double expectedNetAmount=expectedServiceAmount-expectedAppDiscount;
			double expectedamountPayable=Double.parseDouble(payClinic);
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "service", rowCount).getAttribute("value")
				.trim().equals(expectedService)), "Booked Service not matching");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "quantity", rowCount).getAttribute("value")
				.trim().equals("1")), "Booked Quantity not matching");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "discountPercent", rowCount).getAttribute("value")
				.trim().equals("0.00")), "Booked Discount Percntage not matching");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "discountAmount", rowCount).getAttribute("value")
				.trim().equals("0.00")), "Booked Discount Amount not matching");
		Assert.assertTrue(actualBenefitText.contains(expectedBenefitText),"Benefit Text not matching");
		Assert.assertTrue((bill.getEmrCreateBillAmountPayableTextBox(Login_Doctor.driver, logger).getAttribute("readonly").trim().equals("true")), "Amount Payable editable");
		Assert.assertTrue((bill.getEmrCreateBillPaidOnlineTextBox(Login_Doctor.driver, logger).getAttribute("readonly").trim().equals("true")), "Grand Total editable");
		Assert.assertTrue((bill.getEmrCreateBillGrandTotalTextBox(Login_Doctor.driver, logger).getAttribute("readonly").trim().equals("true")), "Grand Total editable");
		
		if(actualpaidOnline!=expectedPaidOnline)
		{
			Assert.assertTrue(false,"Paid online not matching");
		}
		if(actualNetAmount!=expectedNetAmount)
		{
			Assert.assertTrue(false," Net Amount not matching");
		}
		if(actualServiceAmount!=expectedServiceAmount)
		{
			Assert.assertTrue(false,"Service or Tariff Amount not matching");
		}
		if(actualNetAmount!=actualGrandTotal)
		{
			Assert.assertTrue(false,"Grand Total not Correct");
		}
		if(actualAppDiscount!=expectedAppDiscount)
		{
			Assert.assertTrue(false,"App Discount not correct");
		}
		if(actualamountPayable!=expectedamountPayable)
		{
			Assert.assertTrue(false,"Amount Payable not Correct");
		}
		}
	public synchronized void billConsumerPOServiceValidation() throws Exception {
		double expectedServiceAmount=ConsumerAppointmentTest.serviceAmount;
		double actualServiceAmount=Double.parseDouble(bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "tariff", rowCount).getAttribute("value").trim());
		String expectedService="MeetDoctor";
		service=expectedService;
		String actualBenefitText=bill.getEmrCreateBillAppliedBenefitText(Login_Doctor.driver, logger).getText().trim();
		String expectedBenefitText="Package Applied -";
		String paid=ConsumerAppointmentTest.Paid;
		 String pack=paid.substring(0,paid.indexOf('/')).trim();
		 String online=paid.substring(paid.indexOf('+')+1,paid.lastIndexOf('/')).trim();
		 double expectedPaidOnline=Double.parseDouble(online);
		 double expectedAppDiscount=Double.parseDouble(pack);
		 double actualNetAmount=Double.parseDouble(bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "netAmount", rowCount).getAttribute("value").trim());
		 double actualpaidOnline = Double
					.parseDouble(bill.getEmrCreateBillPaidOnlineTextBox(Login_Doctor.driver, logger).getAttribute("value").trim());
			double actualamountPayable = Double.parseDouble(
					bill.getEmrCreateBillAmountPayableTextBox(Login_Doctor.driver, logger).getAttribute("value").trim());
			double actualGrandTotal = Double.parseDouble(
					bill.getEmrCreateBillGrandTotalTextBox(Login_Doctor.driver, logger).getAttribute("value").trim());
			double actualAppDiscount = Double.parseDouble(
					bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "appDiscountAmount", rowCount).getAttribute("value").trim());
			double expectedNetAmount=expectedServiceAmount-expectedAppDiscount;
			double expectedamountPayable=expectedNetAmount-expectedPaidOnline;
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "service", rowCount).getAttribute("value")
				.trim().equals(expectedService)), "Booked Service not matching");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "quantity", rowCount).getAttribute("value")
				.trim().equals("1")), "Booked Quantity not matching");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "discountPercent", rowCount).getAttribute("value")
				.trim().equals("0.00")), "Booked Discount Percntage not matching");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "discountAmount", rowCount).getAttribute("value")
				.trim().equals("0.00")), "Booked Discount Amount not matching");
		Assert.assertTrue(actualBenefitText.contains(expectedBenefitText),"Benefit Text not matching");
		Assert.assertTrue((bill.getEmrCreateBillAmountPayableTextBox(Login_Doctor.driver, logger).getAttribute("readonly").trim().equals("true")), "Amount Payable editable");
		Assert.assertTrue((bill.getEmrCreateBillPaidOnlineTextBox(Login_Doctor.driver, logger).getAttribute("readonly").trim().equals("true")), "Grand Total editable");
		Assert.assertTrue((bill.getEmrCreateBillGrandTotalTextBox(Login_Doctor.driver, logger).getAttribute("readonly").trim().equals("true")), "Grand Total editable");
		
		if(actualpaidOnline!=expectedPaidOnline)
		{
			Assert.assertTrue(false,"Paid online not matching");
		}
		if(actualNetAmount!=expectedNetAmount)
		{
			Assert.assertTrue(false," Net Amount not matching");
		}
		if(actualServiceAmount!=expectedServiceAmount)
		{
			Assert.assertTrue(false,"Service or Tariff Amount not matching");
		}
		if(actualNetAmount!=actualGrandTotal)
		{
			Assert.assertTrue(false,"Grand Total not Correct");
		}
		if(actualAppDiscount!=expectedAppDiscount)
		{
			Assert.assertTrue(false,"App Discount not correct");
		}
		if(actualamountPayable!=expectedamountPayable)
		{
			Assert.assertTrue(false,"Amount Payable not Correct");
		}
		}
	public synchronized void billConsumerFPcServiceValidation() throws Exception {
		double expectedServiceAmount=ConsumerAppointmentTest.serviceAmount;
		double actualServiceAmount=Double.parseDouble(bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "tariff", rowCount).getAttribute("value").trim());
		String expectedService="MeetDoctor";
		service=expectedService;
		String paid=ConsumerAppointmentTest.Paid;
		 String fitcoin=paid.substring(0,paid.indexOf('/')).trim();
		 String payClinic=paid.substring(paid.indexOf('+')+1,paid.lastIndexOf('/')).trim();
		 double expectedPaidOnline=Double.parseDouble(fitcoin);
		 double expectedAppDiscount=0;
		 double actualNetAmount=Double.parseDouble(bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "netAmount", rowCount).getAttribute("value").trim());
		 double actualpaidOnline = Double
					.parseDouble(bill.getEmrCreateBillPaidOnlineTextBox(Login_Doctor.driver, logger).getAttribute("value").trim());
			double actualamountPayable = Double.parseDouble(
					bill.getEmrCreateBillAmountPayableTextBox(Login_Doctor.driver, logger).getAttribute("value").trim());
			double actualGrandTotal = Double.parseDouble(
					bill.getEmrCreateBillGrandTotalTextBox(Login_Doctor.driver, logger).getAttribute("value").trim());
			double actualAppDiscount = Double.parseDouble(
					bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "appDiscountAmount", rowCount).getAttribute("value").trim());
			double expectedNetAmount=expectedServiceAmount-expectedAppDiscount;
			double expectedamountPayable=Double.parseDouble(payClinic);
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "service", rowCount).getAttribute("value")
				.trim().equals(expectedService)), "Booked Service not matching");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "quantity", rowCount).getAttribute("value")
				.trim().equals("1")), "Booked Quantity not matching");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "discountPercent", rowCount).getAttribute("value")
				.trim().equals("0.00")), "Booked Discount Percntage not matching");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "discountAmount", rowCount).getAttribute("value")
				.trim().equals("0.00")), "Booked Discount Amount not matching");
		Assert.assertTrue((bill.getEmrCreateBillAmountPayableTextBox(Login_Doctor.driver, logger).getAttribute("readonly").trim().equals("true")), "Amount Payable editable");
		Assert.assertTrue((bill.getEmrCreateBillPaidOnlineTextBox(Login_Doctor.driver, logger).getAttribute("readonly").trim().equals("true")), "Grand Total editable");
		Assert.assertTrue((bill.getEmrCreateBillGrandTotalTextBox(Login_Doctor.driver, logger).getAttribute("readonly").trim().equals("true")), "Grand Total editable");
		
		if(actualpaidOnline!=expectedPaidOnline)
		{
			Assert.assertTrue(false,"Paid online not matching");
		}
		if(actualNetAmount!=expectedNetAmount)
		{
			Assert.assertTrue(false," Net Amount not matching");
		}
		if(actualServiceAmount!=expectedServiceAmount)
		{
			Assert.assertTrue(false,"Service or Tariff Amount not matching");
		}
		if(actualNetAmount!=actualGrandTotal)
		{
			Assert.assertTrue(false,"Grand Total not Correct");
		}
		if(actualAppDiscount!=expectedAppDiscount)
		{
			Assert.assertTrue(false,"App Discount not correct");
		}
		if(actualamountPayable!=expectedamountPayable)
		{
			Assert.assertTrue(false,"Amount Payable not Correct");
		}
		}
	public synchronized void billConsumerFOServiceValidation() throws Exception {
		double expectedServiceAmount=ConsumerAppointmentTest.serviceAmount;
		double actualServiceAmount=Double.parseDouble(bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "tariff", rowCount).getAttribute("value").trim());
		String expectedService="MeetDoctor";
		service=expectedService;
		String paid=ConsumerAppointmentTest.Paid;
		 String fitcoin=paid.substring(0,paid.indexOf('/')).trim();
		 String online=paid.substring(paid.indexOf('+')+1,paid.lastIndexOf('/')).trim();
		 double expectedPaidOnline=Double.parseDouble(online)+Double.parseDouble(fitcoin);
		 double expectedAppDiscount=0;
		 double actualNetAmount=Double.parseDouble(bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "netAmount", rowCount).getAttribute("value").trim());
		 double actualpaidOnline = Double
					.parseDouble(bill.getEmrCreateBillPaidOnlineTextBox(Login_Doctor.driver, logger).getAttribute("value").trim());
			double actualamountPayable = Double.parseDouble(
					bill.getEmrCreateBillAmountPayableTextBox(Login_Doctor.driver, logger).getAttribute("value").trim());
			double actualGrandTotal = Double.parseDouble(
					bill.getEmrCreateBillGrandTotalTextBox(Login_Doctor.driver, logger).getAttribute("value").trim());
			double actualAppDiscount = Double.parseDouble(
					bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "appDiscountAmount", rowCount).getAttribute("value").trim());
			double expectedNetAmount=expectedServiceAmount-expectedAppDiscount;
			double expectedamountPayable=expectedNetAmount-expectedPaidOnline;
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "service", rowCount).getAttribute("value")
				.trim().equals(expectedService)), "Booked Service not matching");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "quantity", rowCount).getAttribute("value")
				.trim().equals("1")), "Booked Quantity not matching");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "discountPercent", rowCount).getAttribute("value")
				.trim().equals("0.00")), "Booked Discount Percntage not matching");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "discountAmount", rowCount).getAttribute("value")
				.trim().equals("0.00")), "Booked Discount Amount not matching");
		Assert.assertTrue((bill.getEmrCreateBillAmountPayableTextBox(Login_Doctor.driver, logger).getAttribute("readonly").trim().equals("true")), "Amount Payable editable");
		Assert.assertTrue((bill.getEmrCreateBillPaidOnlineTextBox(Login_Doctor.driver, logger).getAttribute("readonly").trim().equals("true")), "Grand Total editable");
		Assert.assertTrue((bill.getEmrCreateBillGrandTotalTextBox(Login_Doctor.driver, logger).getAttribute("readonly").trim().equals("true")), "Grand Total editable");
		
		if(actualpaidOnline!=expectedPaidOnline)
		{
			Assert.assertTrue(false,"Paid online not matching");
		}
		if(actualNetAmount!=expectedNetAmount)
		{
			Assert.assertTrue(false," Net Amount not matching");
		}
		if(actualServiceAmount!=expectedServiceAmount)
		{
			Assert.assertTrue(false,"Service or Tariff Amount not matching");
		}
		if(actualNetAmount!=actualGrandTotal)
		{
			Assert.assertTrue(false,"Grand Total not Correct");
		}
		if(actualAppDiscount!=expectedAppDiscount)
		{
			Assert.assertTrue(false,"App Discount not correct");
		}
		if(actualamountPayable!=expectedamountPayable)
		{
			Assert.assertTrue(false,"Amount Payable not Correct");
		}
		}
	public synchronized void billConsumerCFPcServiceValidation() throws Exception {
		double expectedServiceAmount=ConsumerAppointmentTest.serviceAmount;
		double actualServiceAmount=Double.parseDouble(bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "tariff", rowCount).getAttribute("value").trim());
		String expectedService="MeetDoctor";
		service=expectedService;
		service=expectedService;
		String actualBenefitText=bill.getEmrCreateBillAppliedBenefitText(Login_Doctor.driver, logger).getText().trim();
		String expectedBenefitText="Coupon Applied -";
		String paid=ConsumerAppointmentTest.Paid;
		 String coupon=paid.substring(0,paid.indexOf('/')).trim();
		 String fitcoin=paid.substring(paid.indexOf('+')+1,paid.lastIndexOf('+')-2).trim();
		 String payClinic=paid.substring(paid.lastIndexOf('+')+1,paid.lastIndexOf('/')).trim();
		 double expectedPaidOnline=Double.parseDouble(fitcoin);
		 double expectedAppDiscount=Double.parseDouble(coupon);
		 double actualNetAmount=Double.parseDouble(bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "netAmount", rowCount).getAttribute("value").trim());
		 double actualpaidOnline = Double
					.parseDouble(bill.getEmrCreateBillPaidOnlineTextBox(Login_Doctor.driver, logger).getAttribute("value").trim());
			double actualamountPayable = Double.parseDouble(
					bill.getEmrCreateBillAmountPayableTextBox(Login_Doctor.driver, logger).getAttribute("value").trim());
			double actualGrandTotal = Double.parseDouble(
					bill.getEmrCreateBillGrandTotalTextBox(Login_Doctor.driver, logger).getAttribute("value").trim());
			double actualAppDiscount = Double.parseDouble(
					bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "appDiscountAmount", rowCount).getAttribute("value").trim());
			double expectedNetAmount=expectedServiceAmount-expectedAppDiscount;
			double expectedamountPayable=Double.parseDouble(payClinic);
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "service", rowCount).getAttribute("value")
				.trim().equals(expectedService)), "Booked Service not matching");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "quantity", rowCount).getAttribute("value")
				.trim().equals("1")), "Booked Quantity not matching");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "discountPercent", rowCount).getAttribute("value")
				.trim().equals("0.00")), "Booked Discount Percntage not matching");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "discountAmount", rowCount).getAttribute("value")
				.trim().equals("0.00")), "Booked Discount Amount not matching");
		Assert.assertTrue(actualBenefitText.contains(expectedBenefitText),"Benefit Text not matching");
		Assert.assertTrue((bill.getEmrCreateBillAmountPayableTextBox(Login_Doctor.driver, logger).getAttribute("readonly").trim().equals("true")), "Amount Payable editable");
		Assert.assertTrue((bill.getEmrCreateBillPaidOnlineTextBox(Login_Doctor.driver, logger).getAttribute("readonly").trim().equals("true")), "Grand Total editable");
		Assert.assertTrue((bill.getEmrCreateBillGrandTotalTextBox(Login_Doctor.driver, logger).getAttribute("readonly").trim().equals("true")), "Grand Total editable");
		
		if(actualpaidOnline!=expectedPaidOnline)
		{
			Assert.assertTrue(false,"Paid online not matching");
		}
		if(actualNetAmount!=expectedNetAmount)
		{
			Assert.assertTrue(false," Net Amount not matching");
		}
		if(actualServiceAmount!=expectedServiceAmount)
		{
			Assert.assertTrue(false,"Service or Tariff Amount not matching");
		}
		if(actualNetAmount!=actualGrandTotal)
		{
			Assert.assertTrue(false,"Grand Total not Correct");
		}
		if(actualAppDiscount!=expectedAppDiscount)
		{
			Assert.assertTrue(false,"App Discount not correct");
		}
		if(actualamountPayable!=expectedamountPayable)
		{
			Assert.assertTrue(false,"Amount Payable not Correct");
		}
		}
	public synchronized void billConsumerCFOServiceValidation() throws Exception {
		double expectedServiceAmount=ConsumerAppointmentTest.serviceAmount;
		double actualServiceAmount=Double.parseDouble(bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "tariff", rowCount).getAttribute("value").trim());
		String expectedService="MeetDoctor";
		service=expectedService;
		String actualBenefitText=bill.getEmrCreateBillAppliedBenefitText(Login_Doctor.driver, logger).getText().trim();
		String expectedBenefitText="Coupon Applied -";
		String paid=ConsumerAppointmentTest.Paid;
		 String coupon=paid.substring(0,paid.indexOf('/')).trim();
		 String fitcoin=paid.substring(paid.indexOf('+')+1,paid.lastIndexOf('+')-2).trim();
		 String online=paid.substring(paid.lastIndexOf('+')+1,paid.lastIndexOf('/')).trim();
		 double expectedPaidOnline=Double.parseDouble(online)+Double.parseDouble(fitcoin);
		 double expectedAppDiscount=Double.parseDouble(coupon);
		 double actualNetAmount=Double.parseDouble(bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "netAmount", rowCount).getAttribute("value").trim());
		 double actualpaidOnline = Double
					.parseDouble(bill.getEmrCreateBillPaidOnlineTextBox(Login_Doctor.driver, logger).getAttribute("value").trim());
			double actualamountPayable = Double.parseDouble(
					bill.getEmrCreateBillAmountPayableTextBox(Login_Doctor.driver, logger).getAttribute("value").trim());
			double actualGrandTotal = Double.parseDouble(
					bill.getEmrCreateBillGrandTotalTextBox(Login_Doctor.driver, logger).getAttribute("value").trim());
			double actualAppDiscount = Double.parseDouble(
					bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "appDiscountAmount", rowCount).getAttribute("value").trim());
			double expectedNetAmount=expectedServiceAmount-expectedAppDiscount;
			double expectedamountPayable=expectedNetAmount-expectedPaidOnline;
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "service", rowCount).getAttribute("value")
				.trim().equals(expectedService)), "Booked Service not matching");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "quantity", rowCount).getAttribute("value")
				.trim().equals("1")), "Booked Quantity not matching");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "discountPercent", rowCount).getAttribute("value")
				.trim().equals("0.00")), "Booked Discount Percntage not matching");
		Assert.assertTrue((bill.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "discountAmount", rowCount).getAttribute("value")
				.trim().equals("0.00")), "Booked Discount Amount not matching");
		Assert.assertTrue(actualBenefitText.contains(expectedBenefitText),"Benefit Text not matching");
		Assert.assertTrue((bill.getEmrCreateBillAmountPayableTextBox(Login_Doctor.driver, logger).getAttribute("readonly").trim().equals("true")), "Amount Payable editable");
		Assert.assertTrue((bill.getEmrCreateBillPaidOnlineTextBox(Login_Doctor.driver, logger).getAttribute("readonly").trim().equals("true")), "Grand Total editable");
		Assert.assertTrue((bill.getEmrCreateBillGrandTotalTextBox(Login_Doctor.driver, logger).getAttribute("readonly").trim().equals("true")), "Grand Total editable");
		
		if(actualpaidOnline!=expectedPaidOnline)
		{
			Assert.assertTrue(false,"Paid online not matching");
		}
		if(actualNetAmount!=expectedNetAmount)
		{
			Assert.assertTrue(false," Net Amount not matching");
		}
		if(actualServiceAmount!=expectedServiceAmount)
		{
			Assert.assertTrue(false,"Service or Tariff Amount not matching");
		}
		if(actualNetAmount!=actualGrandTotal)
		{
			Assert.assertTrue(false,"Grand Total not Correct");
		}
		if(actualAppDiscount!=expectedAppDiscount)
		{
			Assert.assertTrue(false,"App Discount not correct");
		}
		if(actualamountPayable!=expectedamountPayable)
		{
			Assert.assertTrue(false,"Amount Payable not Correct");
		}
		}
	
	public synchronized void billingCreatePaymentOptionDisplay() throws Exception {
		bill.getEmrCreateBillPaymentModeText(Login_Doctor.driver, logger).isDisplayed();
		bill.getEmrCreateBillPayementOption(Login_Doctor.driver, logger, "Cash").isDisplayed();
		bill.getEmrCreateBillPayementOption(Login_Doctor.driver, logger, "Card").isDisplayed();
		bill.getEmrCreateBillPayementOption(Login_Doctor.driver, logger, "Wallets").isDisplayed();
	}
	public synchronized void consumerAppointmentNoServiceGenerateBillValidation() throws Exception
	 {
		expectedNetTotalAmount=0;
		Web_GeneralFunctions.wait(4);
		addNetTotalAmount();
		 grandTotal= getGrandTotal();
		 billingCreateBillPaymentValidation();
	   }
}
