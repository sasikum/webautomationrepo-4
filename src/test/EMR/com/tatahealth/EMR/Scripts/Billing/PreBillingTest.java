package com.tatahealth.EMR.Scripts.Billing;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.EMR.Scripts.Consultation.ConsultationTest;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.pages.AppointmentDashboard.AllSlotsPage;
import com.tatahealth.EMR.pages.Billing.Billing;
import com.tatahealth.EMR.pages.Consultation.ConsultationPage;
import com.tatahealth.EMR.pages.Login.LoginPage;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class PreBillingTest {
	
	BillingTest bs=new BillingTest();
	Billing bill = new Billing();
	LoginPage loginPage = new LoginPage();
	MasterAdministrationTest masterAdmin=new MasterAdministrationTest();
	ConsultationTest appt=new ConsultationTest();
	AllSlotsPage ASpage = new AllSlotsPage();
	ConsultationPage consultation = new ConsultationPage();
	public static ExtentTest logger;
	
	  @BeforeClass(alwaysRun = true) public static void beforeClass() throws Exception { 
		  Reports.reports();
		  Login_Doctor.executionName="PreBilling";
		  BillingTest.doctorName="Saket";
		  Login_Doctor.LoginTestwithDiffrentUser(BillingTest.doctorName);
			 ConsultationTest.patientName="Ballu";
		  }
	  
	  @AfterClass(alwaysRun = true) public static void afterClass() throws Exception {
		  Login_Doctor.LogoutTest();
		  Reports.extent.flush(); 
		  }
	@Test(priority=18) 
	  public synchronized void preBillingMasterValidation() throws Exception
	 {
	  logger = Reports.extent.createTest("EMR_Pre Billing Master Validation");
	  bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
		Web_GeneralFunctions.wait(2);
	  bill.getEmrTopMenu(Login_Doctor.driver, logger).click();
	  masterAdmin.masterAdministrationBillType("Pre-Billing");
	  Web_GeneralFunctions.wait(4);
	  Login_Doctor.Logout();
	  Login_Doctor.LoginTestwithoutNewInstance(BillingTest.doctorName);
	  }
	/*Test case:EMR_Prebill_12*/
	/*EMR_Prebill_14 user specific*/
	@Test(priority=19) 
	 public synchronized void preBillingRescheduleAppointmentValidation() throws Exception
	 {
		 logger = Reports.extent.createTest("EMR_Pre Billing Reschedule Appointment Validation");
		 bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
			 appt.startBookingAppointment();
			 appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
			 appt.bookedAppointmentOptionValidation();
			 appt.getAppointmentDropDownValue(4,"Click on Reschedule");
			 appt.appointmentReschedule();
			 Web_GeneralFunctions.wait(5);
			 appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
			 appt.bookedAppointmentOptionValidation();
			 appt.getAppointmentDropDownValue(1,"Click on check in");
			 Web_GeneralFunctions.wait(5);
			 appt.getAppointmentDropdown();
			 appt.appointmentCheckInStatus();
			 appt.bookedAppointmentOptionAfterCheckinOrCheckinGenerateBillCancelValidation();
			 appt.getAppointmentDropDownValue(1,"Click on  Preconsultaion");
			 Web_GeneralFunctions.wait(5);
			 bill.getEmrTopMenu(Login_Doctor.driver, logger).click();
			  bill.getEmrTopMenuElement(Login_Doctor.driver, logger, "Appointments").click();
			  Web_GeneralFunctions.wait(5);
			  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			  Web_GeneralFunctions.wait(5);
				 appt.getAppointmentDropdown();
				 appt.appointmentPreConsultingStatus();
				 appt.bookedAppointmentOptionCheckinAndNotGenerateBillAfterPreConsultationValidation();
				 appt.getAppointmentDropDownValue(1,"Click on  Preconsulting");
				 ASpage.getNotePreConsultPage(Login_Doctor.driver, logger).sendKeys("addNote");
				 ASpage.getSavePreConsultPage(Login_Doctor.driver, logger).click();
				 Web_GeneralFunctions.wait(8);
			 appt.getAppointmentDropdown();
			 appt.appointmentPreConsultedStatus();
			 appt.bookedAppointmentOptionCheckinAndNotGenerateBillAfterPreConsultingValidation();
			 appt.getAppointmentDropDownValue(1,"Click on consult");
			 appt.afterConsultClickValidation();
			 ASpage.getNotePreConsultPage(Login_Doctor.driver, logger).sendKeys("addNote");
			 appt.getShareUpFollowConsultPageClick();
			 Web_GeneralFunctions.wait(5);
			 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			 Web_GeneralFunctions.wait(5);
			 appt.getAppointmentDropdown();
			 appt.appointmentFollowUpStatus();
			 appt.bookedAppointmentOptionPreBillAfterConsultShareFollowUpValidation();
		    appt.getAppointmentDropDownValue(1,"Click on FollowUp");
		    ASpage.getNotePreConsultPage(Login_Doctor.driver, logger).sendKeys("addNote");
		    appt.checkOutAndPrintConsultPageClick();
		    Web_GeneralFunctions.wait(5);
		    consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		    Web_GeneralFunctions.wait(5); appt.getAppointmentDropdown();
		    appt.appointmentCheckedOutStatus();
		    appt.bookedAppointmentOptionAfterConsultCancelBillValidation();
		 	 
	 }
	
	/*Test case:EMR_Prebill_1-EMR_Prebill_3*/
	@Test(priority=20) 
	 public synchronized void preBillingAppointmentValidation() throws Exception
	 {
		 logger = Reports.extent.createTest("EMR_Pre Billing Appointment Validation");
		 bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
			 appt.startBookingAppointment();
			 appt.getAppointmentDropdown();
			 appt.appointmentBookedStatus();
			 appt.bookedAppointmentOptionValidation();
			 appt.getAppointmentDropDownValue(1,"Click on check in");
			 Web_GeneralFunctions.wait(5);
			 appt.getAppointmentDropdown();
			 appt.appointmentCheckInStatus();
			 appt.bookedAppointmentOptionAfterCheckinOrCheckinGenerateBillCancelValidation();
			 appt.getAppointmentDropDownValue(1,"Click on  Preconsultaion");
			 bill.getEmrTopMenu(Login_Doctor.driver, logger).click();
			  bill.getEmrTopMenuElement(Login_Doctor.driver, logger, "Appointments").click();
			  Web_GeneralFunctions.wait(5);
			  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			  Web_GeneralFunctions.wait(5);
				 appt.getAppointmentDropdown();
				 appt.appointmentPreConsultingStatus();
				 appt.bookedAppointmentOptionCheckinAndNotGenerateBillAfterPreConsultationValidation();
				 appt.getAppointmentDropDownValue(1,"Click on  Preconsulting");
				 ASpage.getNotePreConsultPage(Login_Doctor.driver, logger).sendKeys("addNote");
				 ASpage.getSavePreConsultPage(Login_Doctor.driver, logger).click();
				 Web_GeneralFunctions.wait(5);
			 appt.getAppointmentDropdown();
			 appt.appointmentPreConsultedStatus();
			 appt.bookedAppointmentOptionCheckinAndNotGenerateBillAfterPreConsultingValidation();
			 appt.getAppointmentDropDownValue(1,"Click on consult");
			 appt.afterConsultClickValidation();
			 ASpage.getNotePreConsultPage(Login_Doctor.driver, logger).sendKeys("addNote");
			 appt.getShareUpFollowConsultPageClick();
			 Web_GeneralFunctions.wait(5);
			 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
			 Web_GeneralFunctions.wait(5);
			 appt.getAppointmentDropdown();
			 appt.appointmentFollowUpStatus();
			 appt.bookedAppointmentOptionPreBillAfterConsultShareFollowUpValidation();
			 appt.getAppointmentDropDownValue(1,"Click on FollowUp");
			 ASpage.getNotePreConsultPage(Login_Doctor.driver, logger).sendKeys("addNote");
		    appt.checkOutAndPrintConsultPageClick();
		    Web_GeneralFunctions.wait(5);
		    consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.appointmentCheckedOutStatus();
		  appt.bookedAppointmentOptionAfterConsultCancelBillValidation();
		 
			 
	 }
		

	 /*Test case:EMR_Bill_083-EMR_Bill_084,Bill_cond_001-Bill_cond_002,EMR_Prebill_4*/
	@Test(priority=21) 
	  public synchronized void preBillingCheckInAndGenerateBillValidation() throws Exception
	 {
		 logger = Reports.extent.createTest("EMR_Pre Billing check in and Generate Bill Validation");
		 bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
		  bs.rowCount=0;
		  bs. expectedNetTotalAmount=0;
		  bs.patientName=ConsultationTest.patientName;
		  appt.startBookingAppointment();
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionValidation();
		  appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
		  bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		  bs.billPageDoctorAndPatientFieldValidation();
		  bs.billDefaultServiceValidation();
		  bs.billDefaultServiceLevelChangeValidation();
		  bs.deleteService();
		  bs.emrAppointmentGenerateBillValidation();
		  bs.paymentCashWallet();
		  bs.billingCreateBillPopUpValidation();
		  bs.billingCreateBillPdfValidation();
		  Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
	  }
	
	/*Test case:Bill_cond_004*,EMR_Prebill_5 */
	/*Common:EMR_Prebill_6 */
	 @Test(priority=22) 
	  public synchronized void preBillingCheckInAndGenerateBillCancelValidation() throws Exception
	 {
		 logger = Reports.extent.createTest("EMR_Pre Billing check in and Generate Bill Cancel Validation");
		 bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
		  appt.startBookingAppointment();
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionValidation();
		  appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
		  bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		  bill.getEmrCreateBillCancelButton(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinOrCheckinGenerateBillCancelValidation();
	  }
	 @Test(priority=23) 
	  public synchronized void preBillingCheckInAndGenerateBillSaveDraftValidation() throws Exception
	 {
		 logger = Reports.extent.createTest("EMR_Pre Billing check in and Generate Bill Save Draft Validation");
		 bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
		  bs.rowCount=0;
		  bs. expectedNetTotalAmount=0;
		  bs.patientName=ConsultationTest.patientName;
		  appt.startBookingAppointment();
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionValidation();
		  appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
		  bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		  bs.deleteService();
		  bs. emrAppointmentGenerateBillSaveDraftValidation();
		  bill.getEmrCreateBillSaveDraftButton(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinOrCheckinGenerateBillCancelValidation();
		  appt.getAppointmentDropDownValue(3,"Click on Generate Bill");
		  bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		  bs.billPageDoctorAndPatientFieldValidation();
		  bs.billSaveServiceValidation();
	  }
	
	 /*Test case:Bill_cond_004*,EMR_Prebill_7 */
	 @Test(priority=24) 
	  public synchronized void preBillingCheckInAndGenerateBillPreConsultationValidation() throws Exception
	 {

		 logger = Reports.extent.createTest("EMR_Pre Billing check in and Generate Bill PreConsultation Validation");
		 bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
		  bs.rowCount=0;
		  bs. expectedNetTotalAmount=0;
		  bs.patientName=ConsultationTest.patientName;
		  appt.startBookingAppointment();
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionValidation();
		  appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
		  bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		  bs.billPageDoctorAndPatientFieldValidation();
		  bs.billDefaultServiceValidation();
		  bs.clickGenerateButtonAndClosePopUp();
		  Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.getAppointmentDropDownValue(1,"Click on Pre Consulation");
		  bill.getEmrTopMenu(Login_Doctor.driver, logger).click();
		  bill.getEmrTopMenuElement(Login_Doctor.driver, logger, "Appointments").click();
		  Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionCheckinAndGenerateBillAfterPreConsultationValidation();
	  }
	 /*Test case:Bill_cond_004*,EMR_Prebill_8 */
	 @Test(priority=25) 
	  public synchronized void preBillingCheckInAndGenerateBillConsultValidation() throws Exception
	 {

		 logger = Reports.extent.createTest("EMR_Pre Billing check in and Generate Bill Consult Validation");
		 bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
		  bs.rowCount=0;
		  bs. expectedNetTotalAmount=0;
		  bs.patientName=ConsultationTest.patientName;
		  appt.startBookingAppointment();
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionValidation();
		  appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
		  bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		  bs.billPageDoctorAndPatientFieldValidation();
		  bs.billDefaultServiceValidation();
		  bs.clickGenerateButtonAndClosePopUp();
		  Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.getAppointmentDropDownValue(2,"Click on Consult");
		  appt.afterConsultClickValidation();
		  bill.getEmrTopMenu(Login_Doctor.driver, logger).click();
		  bill.getEmrTopMenuElement(Login_Doctor.driver, logger, "Appointments").click();
		  Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionCheckinAndGenerateBillAfterConsultValidation();
	  }
	 
	 /*Test case:Bill_cond_004*,EMR_Prebill_9,RGB_9*/
      @Test(priority=26) 
	  public synchronized void preBillingCheckInGenerateBillAndConsultShareFollowUpValidation() throws Exception
	 {

		 logger = Reports.extent.createTest("EMR_Pre Billing check In Generate Bill And Consult Share FollowUp Validation");
		 bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
		  bs.rowCount=0;
		  bs. expectedNetTotalAmount=0;
		  bs.patientName=ConsultationTest.patientName;
		  appt.startBookingAppointment();
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionValidation();
		  appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
		  bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		  bs.billPageDoctorAndPatientFieldValidation();
		  bs.billDefaultServiceValidation();
		  bs.clickGenerateButtonAndClosePopUp();
		  Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.getAppointmentDropDownValue(2,"Click on Consult");
		  appt.afterConsultClickValidation();
		  ASpage.getNotePreConsultPage(Login_Doctor.driver, logger).sendKeys("addNote");
		  appt.getShareUpFollowConsultPageClick();
		  Web_GeneralFunctions.wait(5);
		 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		 Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionPreBillGenerateAfterConsultShareFollowUpValidation();
		  
	  }
	 
      /*Test case:EMR_Bill_085,EMR_Prebill_10,RGB_8*/
	 @Test(priority=27) 
	  public synchronized void preBillingCheckInGenerateBillAndConsultCheckout() throws Exception
	 {

		 logger = Reports.extent.createTest("EMR_Pre Billing checkInGenerate Bill And Consultation Validation");
		 bill.getTataHealthLogoLink(Login_Doctor.driver, logger).click();
			Web_GeneralFunctions.wait(2);
		  bs.rowCount=0;
		  bs. expectedNetTotalAmount=0;
		  bs.patientName=ConsultationTest.patientName;
		  appt.startBookingAppointment();
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionValidation();
		  appt.getAppointmentDropDownValue(2,"Click on checkin and Generate Bill");
		  bill.getEmrBillTab(Login_Doctor.driver, logger, "Create Bill").isDisplayed();
		  bs.billPageDoctorAndPatientFieldValidation();
		  bs.billDefaultServiceValidation();
		  bs.clickGenerateButtonAndClosePopUp();
		  Web_GeneralFunctions.wait(5);
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterCheckinAndGenerateBillValidation();
		  appt.getAppointmentDropDownValue(2,"Click on Consult");
		  appt.afterConsultClickValidation();
		  appt.checkOutAndPrintConsultPageClick();
		  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
		  Web_GeneralFunctions.wait(5);
		  appt.getAppointmentDropdown();
		  appt.bookedAppointmentOptionAfterConsultGenerateBillValidation();	
	  }
		 public synchronized void preBillingAppointmentConsumerValidation() throws Exception
		 {
			 logger = Reports.extent.createTest("EMR_Pre Billing Appointment Consumer Validation");
				 appt.getAppointmentDropdown();
				 appt.appointmentBookedStatus();
				 appt.bookedAppointmentOptionValidation();
				 appt.getAppointmentDropDownValue(1,"Click on check in");
				 Web_GeneralFunctions.wait(5);
				 appt.getAppointmentDropdown();
				 appt.appointmentCheckInStatus();
				 appt.bookedAppointmentOptionAfterCheckinOrCheckinGenerateBillCancelValidation();
				 appt.getAppointmentDropDownValue(1,"Click on  Preconsultaion");
				 bill.getEmrTopMenu(Login_Doctor.driver, logger).click();
				  bill.getEmrTopMenuElement(Login_Doctor.driver, logger, "Appointments").click();
				  Web_GeneralFunctions.wait(5);;
				  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
				  Web_GeneralFunctions.wait(5);
					 appt.getAppointmentDropdown();
					 appt.appointmentPreConsultingStatus();
					 appt.bookedAppointmentOptionCheckinAndNotGenerateBillAfterPreConsultationValidation();
					 appt.getAppointmentDropDownValue(1,"Click on  Preconsulting");
					 ASpage.getNotePreConsultPage(Login_Doctor.driver, logger).sendKeys("addNote");
					 ASpage.getSavePreConsultPage(Login_Doctor.driver, logger).click();
					 Web_GeneralFunctions.wait(5);
				 appt.getAppointmentDropdown();
				 appt.appointmentPreConsultedStatus();
				 appt.bookedAppointmentOptionCheckinAndNotGenerateBillAfterPreConsultingValidation();
				 appt.getAppointmentDropDownValue(1,"Click on consult");
				 appt.afterConsultClickValidation();
				 ASpage.getNotePreConsultPage(Login_Doctor.driver, logger).sendKeys("addNote");
				 appt.getShareUpFollowConsultPageClick();
				 Web_GeneralFunctions.wait(5);
				 consultation.clickAllSlots(Login_Doctor.driver, logger).click();
				 Web_GeneralFunctions.wait(5);
				 appt.getAppointmentDropdown();
				 appt.appointmentFollowUpStatus();
				 appt.bookedAppointmentOptionPreBillAfterConsultShareFollowUpValidation();
				 appt.getAppointmentDropDownValue(1,"Click on FollowUp");
				  appt.checkOutAndPrintConsultPageClick();
				  Web_GeneralFunctions.wait(5);
				  consultation.clickAllSlots(Login_Doctor.driver, logger).click();
				  Web_GeneralFunctions.wait(5);
				  appt.getAppointmentDropdown();
				  appt.appointmentCheckedOutStatus();
				  appt.bookedAppointmentOptionAfterConsultCancelBillValidation();
				 
		 }
	
}
