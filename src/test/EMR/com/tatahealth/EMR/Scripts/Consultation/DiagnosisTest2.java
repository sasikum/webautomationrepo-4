
package com.tatahealth.EMR.Scripts.Consultation;

import static org.testng.Assert.assertTrue;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.util.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.pages.Consultation.DiagnosisPage;
import com.tatahealth.EMR.pages.Consultation.GlobalPrintPage;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;
import com.tatahealth.ReusableModules.ReusableData;

public class DiagnosisTest2 extends DiagnosisPage {

	@BeforeClass(alwaysRun=true,groups= {"Regression","Appointment_Doctor"})
	public static void beforeClass() throws Exception {
		ConsultationTest currentTest = new ConsultationTest();
		currentTest.loginAndStartConsultation("Diagnosis 2");
	}
	
	@AfterClass(groups= {"Regression","Appointment_Doctor"})
	public static void afterClass() throws Exception {
		Login_Doctor.LogoutTest();
		System.out.println("Closed Login_Doctor.driver");
		Reports.extent.flush();
	}
	
	public static Integer row = 0;
	public static Integer commonDiagnosisId = 0;
	public static String commonDiagnosisName = "";
	public static List<String> savedDiagnosis = new ArrayList<String>();
	public static String deletedDiagnosis = "";
	public static String statusChangeDiagnosis = "";
	GlobalPrintPage gp = new GlobalPrintPage();
	WebDriver driver;
	DiagnosisTest dp = new DiagnosisTest();

	ExtentTest logger;

	public synchronized void clickAddDiagnosis() throws Exception {

		logger = Reports.extent.createTest("EMR Add New Row In Diagnosis");
		DiagnosisPage diagnosis = new DiagnosisPage();
		Web_GeneralFunctions.click(diagnosis.getAddDiagnosisButton(Login_Doctor.driver, logger),
				"Click Add New Row button in diagnosis", Login_Doctor.driver, logger);
		Thread.sleep(1000);

	}

	public synchronized void clickDeleteDiagnosisButton() throws Exception {

		logger = Reports.extent.createTest("EMR Delete Row In Diagnosis");
		DiagnosisPage diagnosis = new DiagnosisPage();
		Web_GeneralFunctions.click(diagnosis.getDeleteDiagnosis(row, Login_Doctor.driver, logger),
				"Click Diagnosis Delete button", Login_Doctor.driver, logger);
		Thread.sleep(1000);

	}

	public synchronized void Clickcheckout() throws Exception {

		logger = Reports.extent.createTest("EMR Click check out  Button");
		Web_GeneralFunctions.click(gp.getCheckOutButton(Login_Doctor.driver, logger), "Click Check out  Button",
				Login_Doctor.driver, logger);
		Thread.sleep(3000);

	}

	public synchronized void moveToDiagnosisModule() throws Exception {

		logger = Reports.extent.createTest("EMR Move to Diagnosis Module");
		DiagnosisPage diagnosis = new DiagnosisPage();
		Web_GeneralFunctions.click(diagnosis.moveToDiagnosisModule(Login_Doctor.driver, logger),
				"Move to diagnosis module", Login_Doctor.driver, logger);
		Thread.sleep(1000);
	}

	public synchronized void clickOnShareToFollowUp() throws Exception {

		logger = Reports.extent.createTest("EMR Click Share To Follow up  Button");
		Web_GeneralFunctions.click(gp.getShareToFollowUpButton(Login_Doctor.driver, logger),"Click Share to Follow Up  Button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(5);

		/*
		 * String xpath = "//div[@class='sweet-alert showSweetAlert visible']"; boolean
		 * status = gp.getEmptyDiagnosisAlertPopUp(Login_Doctor.driver, logger);
		 * Thread.sleep(3000); if(status == true) {
		 * Web_GeneralFunctions.click(gp.getEmptyDiagnosisConfirm(Login_Doctor.driver,
		 * logger), "Click confirm in empty diagnosis pop up", Login_Doctor.driver,
		 * logger); Thread.sleep(8000); }
		 */
	}

	/*
	 * test case num in master reg OD_002;OD_003;OD_004;OD_005;OD_006;OD_007;OD_008
	 */
	public synchronized void worningPopUpIsPresent()

	{
		String WorningpopUp = "body.stop-scrolling:nth-child(6) > div.sweet-alert.showSweetAlert.visible:nth-child(47)";

		try {

			assertTrue(driver.findElement(By.cssSelector(WorningpopUp)).isDisplayed(), "WorningpopUp displayed");
			driver.findElement(By.cssSelector(WorningpopUp)).click();
			Thread.sleep(500);

		} catch (Exception e) {
			System.out.println(e);

		}
	}

	public synchronized void clickOnWorningPopUpNo() {
		String WorningpopUpNo = "body.stop-scrolling:nth-child(6) div.sweet-alert.showSweetAlert.visible:nth-child(47) div.sa-button-container:nth-child(10) > button.cancel]";
		String WorningpopUpYes = "//button[@class='confirm']";
		try {

			Robot robot = new Robot();
			Thread.sleep(1000);
			robot.keyPress(KeyEvent.VK_TAB);
			robot.keyRelease(KeyEvent.VK_TAB);
			robot.keyPress(KeyEvent.VK_TAB);
			robot.keyRelease(KeyEvent.VK_TAB);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);

			// driver.findElement(By.xpath(WorningpopUpNo)).click();
			Thread.sleep(200);

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public synchronized void clickOnWorningPopUpYes() {

		String WorningpopUpYes = "//button[@class='confirm']";
		try {

			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_TAB);
			robot.keyRelease(KeyEvent.VK_TAB);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			// driver.findElement(By.xpath(WorningpopUpYes)).click();
			Thread.sleep(200);

		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	// ....................................................................

	@Test(groups = { "Regression", "Diagnosis" }, priority = 651)
	public synchronized void checkOutWithOutDiagonosis() throws Exception {

		moveToDiagnosisModule();
		dp.saveDiagnosis();
		Clickcheckout();

		// div[@class='sweet-alert showSweetAlert visible']
	}

	@Test(groups = { "Regression", "Diagnosis" }, priority = 652)

	public synchronized void ValidateWorningPopUpNoForCheckOut() throws Exception {

		worningPopUpIsPresent();
		System.out.println("...");

		clickOnWorningPopUpNo();
		System.out.println("...");
		Thread.sleep(200);

	}

	@Test(groups = { "Regression", "Diagnosis" }, priority = 652)

	public synchronized void ValidateWorningPopUpYesForCheckOut() throws Exception {

		Clickcheckout();
		clickOnWorningPopUpYes();
		Thread.sleep(2000);
	}

	@Test(groups = { "Regression", "Diagnosis" }, priority = 653)

	public synchronized void ValidatesecondWorningPopUp() throws Exception {
		try {

			GlobalPrintTest gpt = new GlobalPrintTest();
			gpt.clickCheckOutClose();
			 

		} catch (Exception e) { // TODO: handle exception
		}
	}
	
	
	

     //@Test(groups= {"Regression","Diagnosis"},priority=654)

	public synchronized void SharefollowUpWithOutDiagonosis() throws Exception {
		try {
			moveToDiagnosisModule();
			clickOnShareToFollowUp();

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	// @Test(groups= {"Regression","Diagnosis"},priority=655)

	public synchronized void ValidateWorningPopUpNoForShareToFollowUp() throws Exception {

		worningPopUpIsPresent();
		clickOnWorningPopUpNo();
		Thread.sleep(200);

	}
	// @Test(groups= {"Diagnosis"},priority=656)

	public synchronized void ValidateWorningPopUpYesForShareToFollowUp() throws Exception {

		Clickcheckout();
		clickOnWorningPopUpYes();
		Thread.sleep(200);
		// WebDriverWait wait = new WebDriverWait(driver,60);

		// wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='printSummarybtn']")));
		Thread.sleep(2000);
	}

	// @Test(groups= {"Diagnosis"},priority=657)

	public synchronized void clickOnConfirmationBoxPrint() throws Exception {

		String WorningpopUp = "//div[@id='checkoutConsultSummary']//div[@class='modal-header']";
		String WorningpopUpClose = "//a[@id='checkoutClose']";
		// String WorningpopUpPrintSummarybtn= "//*[@id='printSummarybtn' and
		// text()='View Summary']";
		String WorningpopUpPrintSummarybtn = "//div[@id='checkoutConsultSummary']//div[@class='modal-footer']/a[1]";
		try {
			Thread.sleep(1000);

			driver.findElement(By.xpath("//div[@id='checkoutConsultSummary']//div[@class='modal-footer']")).click();
			Thread.sleep(500);

			driver.findElement(By.xpath("WorningpopUpPrintSummarybtn")).click();

			Thread.sleep(1000);

			/*
			 * ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
			 * driver.switchTo().window(tabs.get(1));
			 * 
			 * driver.close();
			 * 
			 * tabs = new ArrayList<String> (driver.getWindowHandles());
			 * driver.switchTo().window(tabs.get(0));
			 */
		} catch (Exception e) {
			// TODO: handle exception
		}

	}
}
