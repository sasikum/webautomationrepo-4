package com.tatahealth.EMR.pages.AppointmentDashboard;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;



public class ConsultationPage {
	
	
	public WebElement getCheckoutBtn(WebDriver driver,ExtentTest logger) {
		WebElement element = Web_GeneralFunctions.findElementbySelector("#checkoutButton", driver, logger);
		return element;
	}
	
	
	public WebElement setNotes(WebDriver driver,ExtentTest logger) {
		WebElement element = Web_GeneralFunctions.findElementbySelector("#txtparameterComment", driver, logger);
		return element;
	}
	
	public WebElement getPrintAllBtn(WebDriver driver,ExtentTest logger) {
		WebElement element = Web_GeneralFunctions.findElementbySelector("#TransferPrescription", driver, logger);
		return element;
	}
	
	public WebElement getYesinSweetAlert(WebDriver driver,ExtentTest logger) {
		WebElement element = Web_GeneralFunctions.findElementbySelector("body > div.sweet-alert.showSweetAlert.visible > div.sa-button-container > div > button", driver, logger,10);
		return element;
	}
	
	public WebElement getCloseBtninPopup(WebDriver driver,ExtentTest logger) {
		WebElement element = Web_GeneralFunctions.findElementbySelector("#checkoutClose", driver, logger);
		return element;
	}
	
	
	
}