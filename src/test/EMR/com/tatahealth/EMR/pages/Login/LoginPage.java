package com.tatahealth.EMR.pages.Login;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;



public class LoginPage {
	
	
	public WebElement getUsernameTextBox(WebDriver driver,ExtentTest logger) 
	{
		WebElement username = Web_GeneralFunctions.findElementbySelector("#txtUsername",driver,logger);
		return username;	
	}
	
	public WebElement getPasswordTextBox(WebDriver driver,ExtentTest logger) 
	{
		WebElement password = Web_GeneralFunctions.findElementbySelector("#txtPassword",driver,logger);
		return password;	
	}
	
	public WebElement getLoginBtn(WebDriver driver,ExtentTest logger) 
	{
		WebElement Signinbtn = Web_GeneralFunctions.findElementbySelector("#login",driver,logger);
		return Signinbtn;	
	}
	
	public WebElement getSignUpLink(WebDriver driver,ExtentTest logger) 
	{
		WebElement signUpLink = Web_GeneralFunctions.findElementbySelector("#signUpLink",driver,logger);
		return signUpLink;	
	}
	
	// elements in the pop up
	
	
	public WebElement getIAgreeCbx(WebDriver driver,ExtentTest logger) 
	{
		WebElement iAgreeCbx = Web_GeneralFunctions.findElementbyXPath("//label[contains(text(),'I Agree')]//input", driver, logger);
		return iAgreeCbx;	
	}
	
	
	public WebElement getProceedBtn(WebDriver driver,ExtentTest logger) 
	{
		WebElement proceedBtn = Web_GeneralFunctions.findElementbyXPath("//button[@id='proceed']", driver, logger);
		return proceedBtn;	
	}
	
		
	// elements in signup the pop up

	public WebElement getSignupClinicUserIdTbx(WebDriver driver,ExtentTest logger) 
	{
		WebElement signupClinicUserId = Web_GeneralFunctions.findElementbySelector("#signupClinicUserId",driver,logger);
		return signupClinicUserId;	
	}
	
	public WebElement getSignupOrgIdTbx(WebDriver driver,ExtentTest logger) 
	{
		WebElement signupOrgId = Web_GeneralFunctions.findElementbySelector("#signupOrgId",driver,logger);
		return signupOrgId;	
	}
	
	public WebElement getvalidateSignUpBtn(WebDriver driver,ExtentTest logger) 
	{
		WebElement validateSignUpBtn = Web_GeneralFunctions.findElementbySelector("#validateSignUpButton",driver,logger);
		return validateSignUpBtn;	
	}
	public WebElement getuserLoginNameTbx(WebDriver driver,ExtentTest logger) 
	{
		WebElement userLoginName = Web_GeneralFunctions.findElementbySelector("#userLoginName",driver,logger);
		return userLoginName;	
	}
	
	public WebElement getverifyLoginNameBtn(WebDriver driver,ExtentTest logger) 
	{
		WebElement verifyLoginName = Web_GeneralFunctions.findElementbySelector("#verifyLoginName",driver,logger);
		return verifyLoginName;	
	}

	public WebElement getSignupOtpTbx(WebDriver driver,ExtentTest logger) 
	{
		WebElement signupOtpTbx = Web_GeneralFunctions.findElementbySelector("#signupOTP",driver,logger);
		return signupOtpTbx;	
	}
	/*public WebElement getSignupResendOTPBtn(WebDriver driver,ExtentTest logger) 
	{
		WebElement verifyLoginName = Web_GeneralFunctions.findElementbySelector("#signupResendOTP",driver,logger);
		return verifyLoginName;	
	}
	public WebElement getokButtonBtn(WebDriver driver,ExtentTest logger) 
	{
		WebElement okButton = Web_GeneralFunctions.findElementbySelector("#okButton",driver,logger);
		return okButton;	
	}*/
	
	public WebElement getUserPasswordTbx(WebDriver driver,ExtentTest logger) 
	{
		WebElement userPasswordTbx = Web_GeneralFunctions.findElementbyXPath("//input[@id='newPass']", driver, logger);
		return userPasswordTbx;	
	}
	
	public WebElement getUserconfirmPasswordTbx(WebDriver driver,ExtentTest logger) 
	{
		WebElement userconfirmPasswordTbx = Web_GeneralFunctions.findElementbyXPath("//input[@id='confirmPass']", driver, logger);
		return userconfirmPasswordTbx;	
	}
	
	public WebElement getUserSignUpSaveBtn(WebDriver driver,ExtentTest logger) 
	{
		WebElement userconfirmPasswordBtn = Web_GeneralFunctions.findElementbyXPath("//button[@onclick='userSignUpSubmit()']", driver, logger);
		return userconfirmPasswordBtn;	
	}

	public WebElement getSignupResendOTPBtn(WebDriver driver,ExtentTest logger) 
	{
		WebElement verifyLoginName = Web_GeneralFunctions.findElementbySelector("#signupResendOTP",driver,logger);
		return verifyLoginName;	
	}
	public WebElement getokButtonBtn(WebDriver driver,ExtentTest logger) 
	{
		WebElement okButton = Web_GeneralFunctions.findElementbySelector("#okButton",driver,logger);
		return okButton;	
	}
	public WebElement getLogoutBtn(WebDriver driver,ExtentTest logger) 
	{
		WebElement logoutBtn = Web_GeneralFunctions.findElementbyXPath("//a[@title='Logout']/img", driver, logger);
		return logoutBtn;		
	}
}