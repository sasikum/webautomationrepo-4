package com.tatahealth.EMR.pages.Login;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;



public class SweetAlertPage {
	
	
	public boolean getSweetAlertCheck(WebDriver driver,ExtentTest logger) 
	{
		try {
			boolean val = Web_GeneralFunctions.findElementbySelector("body > div.sweet-alert.showSweetAlert.visible > h2",driver,logger).isDisplayed();
			return val;
		}catch(Exception e) {
			return false;
		}	
	}
	
	public WebElement getYesBtn(WebDriver driver,ExtentTest logger,int time) 
	{
		WebElement element = Web_GeneralFunctions.findElementbySelector("body > div.sweet-alert.showSweetAlert.visible > div.sa-button-container > div > button",driver,logger,time);
		return element;	
	}
	
	public WebElement getNoBtn(WebDriver driver,ExtentTest logger) 
	{
		WebElement element = Web_GeneralFunctions.findElementbySelector("body > div.sweet-alert.showSweetAlert.visible > div.sa-button-container > button",driver,logger);
		return element;	
	}
	
}