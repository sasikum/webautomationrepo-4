package com.tatahealth.EMR.pages.PracticeManagement;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class ExternalTestTemplatePages {
	
	public WebElement moveToExternalTestTemplate(WebDriver driver,ExtentTest logger) {
		
		String xpath = "//a[contains(text(),'External Tests')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getCreateTemplateButton(WebDriver driver,ExtentTest logger) {
		
		String xpath = "//button[@id='create_tmp_tests']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	
	public WebElement getSaveTemplateButton(WebDriver driver,ExtentTest logger) {
		
		String xpath = "//button[@id='saveTemplateTestOrScan']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	
	public WebElement getUpdateTemplateButton(WebDriver driver,ExtentTest logger) {
		
		String xpath = "//button[@id='update_tmp_tests']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getEnterTemplateNameMessage(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[contains(@class,'animated fadeInDown')]//div[@class='toast-message'][contains(text(),'Please enter Template Name')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public WebElement getMessage(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[contains(@class,'animated fadeInDown')]//div[@class='toast-message']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public WebElement getLabTestIdAttribute(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//table[@id='tosTable']/tbody/tr[1]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	public WebElement getTemplateNameTextField(WebDriver driver,ExtentTest logger) {
		String xpath ="//input[@name='testOrscanTemplateName']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getTemplateNameSection(WebDriver driver,ExtentTest logger) {
		String xpath = "//div[@id='template-Text-Container']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	public WebElement getTestNameSection(WebDriver driver,ExtentTest logger) {
		String xpath = "//div[@id='testOrScanList']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getAddrowButton(WebDriver driver,ExtentTest logger) {
		String xpath = "//a[@class='btn takephotobtn allbtnfl']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getRemoveRowButton(WebDriver driver,ExtentTest logger) {
		String xpath ="//button[@class='text-center btn btn-danger deleteTestOrScan']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getFirstRemoveRowButtom(WebDriver driver,ExtentTest logger) {
		String xpath="(//button[@class='text-center btn btn-danger deleteTestOrScan'])[1]";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getLabTestTextField(WebDriver driver,ExtentTest logger) {
		String xpath = "//input[@name='testNameTestOrScan']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getFirstTemplateFromDropdown(WebDriver driver,ExtentTest logger) {
		String xpath ="(//ul[@class='ui-menu ui-widget ui-widget-content ui-autocomplete ui-front']//li)[1]//a";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getTemplateSearchField(WebDriver driver,ExtentTest logger) {
		String xpath ="//input[@id='labTestsTemplateSearchField']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getFirstLabTestFromDropdown(WebDriver driver,ExtentTest logger) {
		String xpath ="(//ul[@class='ui-menu ui-widget ui-widget-content ui-autocomplete ui-front']//li)[1]//a";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getLabTestIdSearchField(WebDriver driver,ExtentTest logger) {
		return (Web_GeneralFunctions.findElementbyXPath("//input[@id='labTestsTemplateSearchField']", driver, logger));
	}
	
	public List<WebElement> getAllElementsAsList(WebDriver driver,ExtentTest logger) {
		String xpath ="//ul[@class='ui-menu ui-widget ui-widget-content ui-autocomplete ui-front'][starts-with(@style,'display: block;')]/li/a";
		return Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger);
	}
	
	public List<String> getAllLabTestSuggestionsAsList(WebDriver driver,ExtentTest logger) throws Exception{
		List<String> labTestsList  = new ArrayList<>();
		List<WebElement> labTestElements = getAllElementsAsList(driver,logger);
		for(WebElement e : labTestElements) {
			labTestsList.add(Web_GeneralFunctions.getText(e, "extract labtest text", driver, logger));
		}
		return labTestsList;
	}
	
	
	public WebElement getLabNotesField(WebDriver driver,ExtentTest logger) {
		return (Web_GeneralFunctions.findElementbyXPath("//input[@name='notesTestOrScan']", driver, logger));
	}
	
	public WebElement getLabTestTextField(WebDriver driver,int row,ExtentTest logger) {
		return(Web_GeneralFunctions.findElementbyXPath("//input[@id='testOrScanSearchString"+row+"']", driver, logger));
	}
	//input[@name='notesTestOrScan']
	
	public WebElement getLabNotesTextField(WebDriver driver,int row,ExtentTest logger) {
		return (Web_GeneralFunctions.findElementbyXPath("//input[@id='notesTestOrScan"+row+"']", driver, logger));
	}
	
	
	public WebElement getLabTestFromDropDown(WebDriver driver,ExtentTest logger) {
		String xpath ="//ul[@class='ui-menu ui-widget ui-widget-content ui-autocomplete ui-front'][starts-with(@style,'display: block;')]/li[1]/a";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public List<String> getAllSavedLabTestNamesAsList(WebDriver driver,ExtentTest logger) throws Exception{
		String xpath ="//tbody//tr//td//div//input[@placeholder='Search Test Or Scan']";
		List<String> savedlabTestsList  = new ArrayList<>();
		List<WebElement> labTestsElements= Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger);
		for(WebElement e : labTestsElements) {
			savedlabTestsList.add(Web_GeneralFunctions.getAttribute(e,"value", "extract labtest text", driver, logger));
		}
		return savedlabTestsList;
	}
	
	public List<String> getAllSavedLabNotesAsList(WebDriver driver,ExtentTest logger) throws Exception{
		String xpath ="//tbody//tr//td//input[@name='notesTestOrScan']";
		List<String> savedlabNotesList  = new ArrayList<>();
		List<WebElement> labNotesElements= Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger);
		for(WebElement e : labNotesElements) {
			savedlabNotesList.add(Web_GeneralFunctions.getAttribute(e,"value", "extract labtest text", driver, logger));
		}
		return savedlabNotesList;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
