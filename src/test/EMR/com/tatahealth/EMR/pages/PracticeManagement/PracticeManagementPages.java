package com.tatahealth.EMR.pages.PracticeManagement;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class PracticeManagementPages {

	public WebElement getLeftMainMenu(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//a[@id='menu-toggle']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}

	public WebElement getPracticeManagementMenu(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//ul[@id='sidebarlinks']/li[@name='templateMaster']/a";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;	
	}
	
	public WebElement getOrgHeader(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//a[@id='orgLogoLinkHeader']//div[2]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;	
	}
	
	public boolean isappointmentDisplayed(WebDriver driver,ExtentTest logger)throws Exception{
		
		boolean status = false;
		String xpath = "//div[contains(@class,'innerpagepad')]/strong[contains(text(),'There are no booked appointments for this date with selected filters')]";
		if(Web_GeneralFunctions.isDisplayed(xpath, driver)) {
			status = true;
		}else {
			status = false;
		}
		/*
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		String text = Web_GeneralFunctions.getText(element, "Get appointment message", driver, logger);
		Thread.sleep(1000);
		System.out.println("text : "+text);
		if(text.equalsIgnoreCase("There are no booked appointments for this date with selected filters")) {
			status = true;
		}else {
			status = false;
		}*/
		return status;
	}
	
	public WebElement clickOption(WebDriver driver,ExtentTest logger) throws InterruptedException {
		
		WebElement element = Web_GeneralFunctions.findElementbyXPath("//ul[contains(@class,'appointmentwarp')]/li/div[5]/div[3]/div[2]/a", driver, logger);
		return element;
		
	}

	public WebElement selectConsulting(WebDriver driver,ExtentTest logger) throws InterruptedException {
		
		WebElement element = Web_GeneralFunctions.findElementbyXPath("//ul[contains(@class,'appointmentwarp')]/li/div[5]/div[3]/div[2]/ul/li/a", driver, logger);
		return element;
		
	}
	
	public WebElement getPrescription(WebDriver driver,ExtentTest logger) {
		return (Web_GeneralFunctions.findElementbyXPath("//a[contains(text(),'Prescription')]", driver, logger));
	}
	
	public WebElement getExternalTests(WebDriver driver,ExtentTest logger) {
		return (Web_GeneralFunctions.findElementbyXPath("//a[contains(text(),'External Tests')]", driver, logger));
	}
	
	public WebElement getVitalsMaster(WebDriver driver,ExtentTest logger) {
		return (Web_GeneralFunctions.findElementbyXPath("//a[contains(text(),'Vitals Master')]", driver, logger));
	}
	
	public WebElement getConsultationFlow(WebDriver driver,ExtentTest logger) {
		return (Web_GeneralFunctions.findElementbyXPath("//a[contains(text(),'Consultation Flow')]", driver, logger));
	}
	
	public WebElement getSummaryPdfPreference(WebDriver driver,ExtentTest logger) {
		return (Web_GeneralFunctions.findElementbyXPath("//a[contains(text(),'Summary PDF Preference')]",driver,logger));
	}
	
	public WebElement getGoogleCalender(WebDriver driver,ExtentTest logger) {
		return (Web_GeneralFunctions.findElementbyXPath("//li//a[contains(text(),'Google Calendar')]", driver, logger));
	}
	
	public WebElement getMedicalKits(WebDriver driver,ExtentTest logger) {
		return (Web_GeneralFunctions.findElementbyXPath("//a[contains(text(),'Medical Kits')]", driver, logger));
	}
	
	public WebElement getConsultationPdfDesigner(WebDriver driver,ExtentTest logger) {
		return (Web_GeneralFunctions.findElementbyXPath("//a[contains(text(),'Consultation PDF Designer')]", driver, logger));
	}
	
	public WebElement getCaseSheetDesigner(WebDriver driver,ExtentTest logger) {
		return (Web_GeneralFunctions.findElementbyXPath("//li//a[contains(text(),' Casesheet Designer ')]", driver, logger));
	}
	
	
	public WebElement getGeneralAdvice(WebDriver driver,ExtentTest logger) {
		return (Web_GeneralFunctions.findElementbyXPath("//a[contains(text(),'General Advice')]", driver, logger));
	}
}
