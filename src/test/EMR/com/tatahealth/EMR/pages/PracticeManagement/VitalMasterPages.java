package com.tatahealth.EMR.pages.PracticeManagement;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class VitalMasterPages {
	
	public WebElement getVitalMasterMenu(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//a[contains(text(),'Vitals Master')]";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getHeadCircumFerence(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//label[contains(text(),'Head Circumference')]";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getSaveButton(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//button[@id='vitalMasterSaveBtn']";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getMessage(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[contains(@class,'animated fadeInDown')]//div[@class='toast-message']";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getVitalElements(int vitalNo,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//input[@id='vitalParameterCheckbox_"+vitalNo+"']";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));	
		
		
	}
	
	public boolean getVitalCheckedStatus(int vitalNo,WebDriver driver)throws Exception{
		
		
		String xpath = "//input[@id='vitalParameterCheckbox_"+vitalNo+"']";
		return(Web_GeneralFunctions.isChecked(xpath, driver));
		
	}
	
	public WebElement getTemporalTemparatureCheckbox(WebDriver driver,ExtentTest logger) {
		String xpath = "//td//div//label[contains(text(),'Temperature (Temporal)')]//input";
		return(Web_GeneralFunctions.findElementbyXPath(xpath,driver,logger));
	}
	
	public WebElement getBpSittingCheckbox(WebDriver driver,ExtentTest logger) {
		String xpath = "//td//div//label[contains(text(),'BP - Sitting')]//input";
		return(Web_GeneralFunctions.findElementbyXPath(xpath,driver,logger));
	}
	
	public WebElement getHeadCircumferenceCheckbox(WebDriver driver,ExtentTest logger) {
		String xpath = "//td//div//label[contains(text(),'Head Circumference')]//input";
		return(Web_GeneralFunctions.findElementbyXPath(xpath,driver,logger));
	}
	
	public WebElement getBloodPressureCheckbox(WebDriver driver,ExtentTest logger) {
		String xpath = "//td//div//label[contains(text(),'Blood Pressure')]//input";
		return(Web_GeneralFunctions.findElementbyXPath(xpath,driver,logger));
	}
	
	public WebElement getPulseRateCheckbox(WebDriver driver,ExtentTest logger) {
		String xpath = "//td//div//label[contains(text(),'Pulse Rate')]//input";
		return(Web_GeneralFunctions.findElementbyXPath(xpath,driver,logger));
	}
	
	public WebElement getRespirationRateCheckbox(WebDriver driver,ExtentTest logger) {
		String xpath = "//td//div//label[contains(text(),'Respiration Rate')]//input";
		return(Web_GeneralFunctions.findElementbyXPath(xpath,driver,logger));
	}
	
	
	public WebElement getVitalNameFromConsultationPage(int vitalNo,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[@id='vitalRow']/div["+vitalNo+"]/div[2]/span[@class='graytext']";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));	
		
	}
	
	public WebElement getVitalValues(int row,int elementNo,WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//table[@class='table table-bordered table-striped']/tbody/tr["+row+"]/td["+elementNo+"]/div/label";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
		
	}
	
	public List<String> getActualVitalNamesAsList(){
			List<String> actualVitalNamesList = new ArrayList<>();
			actualVitalNamesList = Arrays.asList(new String[] {"Blood Pressure","SpO2%","Temperature","Pulse Rate","Mean Arterial Pressure"
					,"Blood Glucose","Weight","Height","BMI","Respiration Rate","Head Circumference","Temperature (Auxiliary)"
					,"Temperature (Oral)","Temperature (Tympanic)","Temperature (Temporal)","Temperature (Rectal)","BP - Standing"
					,"BP - Sitting","Tooth Eruption","Hip Girth","Abdominal Girth","Abdominal Girth/Hip Girth","OFC"});
			return actualVitalNamesList;		
			
	}
	
	public List<String> getDisplayedVitalNamesAsList(WebDriver driver,ExtentTest logger) throws Exception{
		List<String> displayedVitalNamesList = new ArrayList<>();
		String vitalsXpath = "//div[@id='vitalTemplate']//tbody//child::label";
		List<WebElement> allVitalElements = Web_GeneralFunctions.findElementsbyXpath(vitalsXpath, driver, logger);
		for(WebElement element : allVitalElements) {
			displayedVitalNamesList.add(Web_GeneralFunctions.getText(element, "Extracting vitalName", driver, logger));
		}
		return displayedVitalNamesList;
	}
	
	public List<WebElement> getActiveConsultationsOnAppointmentDashboard(WebDriver driver,ExtentTest logger){
		String xpath ="//ul[@class='appointmentwarp']//li//div[contains(text(),'Consulting')]";
		return(Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger));
	}
	
	public WebElement getConsultingDropDownToggle(WebDriver driver,ExtentTest logger) {
		String xpath ="//div[contains(text(),'Consulting')]//ancestor::li//child::a[@data-toggle='dropdown']";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getConsultingLinkFromDropDownToggle(WebDriver driver,ExtentTest logger) {
		String xpath ="//ul[@class='dropdown-menu droppad']//li//a[contains(text(),'Consulting')]";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}

}
