package com.tatahealth.API.Scripts;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xml.sax.SAXException;


public class GeneralAPIFunctions {
	
	
	private HttpClient httpClient;

	
		
	public JSONObject getRequest(String URL) throws ClientProtocolException, IOException, JSONException, SAXException, ParserConfigurationException {
		
		httpClient = HttpClients.createDefault();
		
		HttpGet get = new HttpGet(URL);
		get.setHeader("Content-Type","application/json");
		
		HttpResponse response = httpClient.execute(get);
		
		GeneralAPIFunctions func = new GeneralAPIFunctions();
		
		JSONObject j = func.getJSONObjectForResponse(response);
		 
		return j;
	}
	
	
	public JSONObject getRequestWithHeaders(String URL,List<NameValuePair> headers) throws ClientProtocolException, IOException, JSONException, SAXException, ParserConfigurationException {
		
		httpClient = HttpClients.createDefault();
		
		HttpGet get = new HttpGet(URL);
		
		System.out.println("URL=" + URL);
		for(int i=0;i<headers.size();i++) {
			get.setHeader(headers.get(i).getName(),headers.get(i).getValue());
			System.out.println(".........................");
			System.out.println(headers.get(i).getName() + "-" + headers.get(i).getValue());
			System.out.println(".........................");
		}
		
		HttpResponse response = httpClient.execute(get);
		
		GeneralAPIFunctions func = new GeneralAPIFunctions();
		
		JSONObject j = func.getJSONObjectForResponse(response);
		 
		return j;
	}
	
	
	
	
	public JSONObject PostRequest(String URL,List<NameValuePair> params) throws ClientProtocolException, IOException, JSONException, SAXException, ParserConfigurationException {
		
		httpClient = HttpClients.createDefault();
		
		HttpPost post = new HttpPost(URL);
		post.setHeader("User-Agent", "HTTP Client");
		post.setHeader("Content-Type","application/json");
		
		
		JSONObject JSONObjectData = new JSONObject();

		for (NameValuePair nameValuePair : params) {
			try {
				JSONObjectData.put(nameValuePair.getName(), nameValuePair.getValue());
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		
		StringEntity requestentity = new StringEntity(JSONObjectData.toString());
		
		post.setEntity(requestentity);
		
		HttpResponse response = httpClient.execute(post);
		
		GeneralAPIFunctions func = new GeneralAPIFunctions();
		
		JSONObject j = func.getJSONObjectForResponse(response);
		
		return j;
	}
	
	
	public JSONObject PostRequestwithFormData(String URL, List<NameValuePair> headers, List<NameValuePair> params,File file) throws ClientProtocolException, IOException, JSONException, SAXException, ParserConfigurationException {
		httpClient = HttpClients.createDefault();
		
		HttpPost post = new HttpPost(URL);
		post.setHeader("User-Agent", "HTTP Client");
		
		for(int i=0;i<headers.size();i++) {
			post.setHeader(headers.get(i).getName(),headers.get(i).getValue());
		}
		
		System.out.println(post);
		
		MultipartEntityBuilder mpEntity = MultipartEntityBuilder.create();
		

		for (NameValuePair nameValuePair : params) {
			try {
				mpEntity.addTextBody(nameValuePair.getName(), nameValuePair.getValue());
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		
		//FileEntity ent = new FileEntity(file,ContentType.create("application/pdf"));
		
		mpEntity.addBinaryBody("file", file);	
		
		HttpEntity entity = mpEntity.build(); 
		
		post.setEntity(entity);
		
		HttpResponse response = httpClient.execute(post);
		
		GeneralAPIFunctions func = new GeneralAPIFunctions();
		
		JSONObject j = func.getJSONObjectForResponse(response);
		
		return j;

	}	
	
	public JSONObject PostHeadersWithRequest(String URL,List<NameValuePair> params, List<NameValuePair> headers) throws ClientProtocolException, IOException, JSONException, SAXException, ParserConfigurationException {
		
		httpClient = HttpClients.createDefault();
		
		HttpPost post = new HttpPost(URL);
		post.setHeader("User-Agent", "HTTP Client");
		post.setHeader("Content-Type","application/json");
		
		for(int i=0;i<headers.size();i++) {
			post.setHeader(headers.get(i).getName(),headers.get(i).getValue());
		}
		
		JSONArray JSONArrayData = new JSONArray();
		JSONObject JSONObjectData = new JSONObject();

		for (NameValuePair nameValuePair : params) {
			try {
				JSONObjectData.put(nameValuePair.getName(), nameValuePair.getValue());
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		JSONArrayData.put(JSONObjectData);
		
		StringEntity requestentity = new StringEntity(JSONArrayData.toString());
		System.out.println(JSONArrayData);
		
		post.setEntity(requestentity);
		
		HttpResponse response = httpClient.execute(post);
		
		GeneralAPIFunctions func = new GeneralAPIFunctions();
		
		JSONObject j = func.getJSONObjectForResponse(response);
		
		return j;
	}
	
	
	public JSONObject PostHeadersWithJSONRequest(String URL,JSONObject Param, List<NameValuePair> headers) throws ClientProtocolException, IOException, JSONException, SAXException, ParserConfigurationException {
		
		httpClient = HttpClients.createDefault();
		
		HttpPost post = new HttpPost(URL);
		post.setHeader("User-Agent", "HTTP Client");
		post.setHeader("Content-Type","application/json");
		
		for(int i=0;i<headers.size();i++) {
			post.setHeader(headers.get(i).getName(),headers.get(i).getValue());
		}
		
		StringEntity requestentity = new StringEntity(Param.toString());
		System.out.println(Param);
		
		post.setEntity(requestentity);
		
		HttpResponse response = httpClient.execute(post);
		
		GeneralAPIFunctions func = new GeneralAPIFunctions();
		
		JSONObject j = func.getJSONObjectForResponse(response);
		
		return j;
	}
	
	
	
	public JSONObject DeletePostRequest(String URL,List<NameValuePair> params) throws ClientProtocolException, IOException, JSONException, SAXException, ParserConfigurationException {
		
		httpClient = HttpClients.createDefault();
		
		HttpDelete post = new HttpDelete(URL);
		post.setHeader("User-Agent", "HTTP Client");
		post.setHeader("Content-Type","application/json");
		
		
		JSONObject JSONObjectData = new JSONObject();

		for (NameValuePair nameValuePair : params) {
			try {
				JSONObjectData.put(nameValuePair.getName(), nameValuePair.getValue());
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		
		StringEntity requestentity = new StringEntity(JSONObjectData.toString());
		
		((HttpResponse) post).setEntity(requestentity);
		
		HttpResponse response = httpClient.execute(post);
		
		GeneralAPIFunctions func = new GeneralAPIFunctions();
		
		JSONObject j = func.getJSONObjectForResponse(response);
		
		return j;
	}
	
	
	
	public JSONObject deleteRequest(String URL) throws ClientProtocolException, IOException, JSONException, SAXException, ParserConfigurationException {
		
		httpClient = HttpClients.createDefault();
		
		HttpDelete delete = new HttpDelete(URL);
		delete.setHeader("Content-Type","application/json");
		
		HttpResponse response = httpClient.execute(delete);
		
		GeneralAPIFunctions func = new GeneralAPIFunctions();
		
		JSONObject j = func.getJSONObjectForResponse(response);
		 
		return j;
	}
	
		
	
	
	public JSONObject getJSONObjectForResponse(HttpResponse httpResponse) throws ClientProtocolException, IOException, SAXException, ParserConfigurationException, JSONException {
		HttpEntity respEntity = httpResponse.getEntity();
		
		// Convert the response to a String format
		String responseString = EntityUtils.toString(respEntity, "UTF-8");
		
		//System.out.println("Response:" + responseString);
		
		// Convert the result as a String to a JSON object
		JSONObject jo = new JSONObject(responseString);

		return jo;

	}
	
	
	
	
	
	
	
}
