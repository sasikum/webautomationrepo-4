package com.tatahealth.API.libraries;

import java.io.File;
//import java.io.File;
import java.util.List;

import javax.mail.internet.MimeMessage;

//import javax.mail.internet.MimeMessage;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import com.tatahealth.API.libraries.SendEmail;
import org.testng.ITestContext;
import org.testng.ITestResult;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class Reports {
	
	public static ExtentHtmlReporter htmlReporter;
	public static ExtentReports extent;
	public static String reportName = System.getProperty("env")+" Automation";
	public static String Val;
	
	@BeforeSuite(alwaysRun=true)
	public static void reports() throws Exception{	
		CommonAPI.deletescreens();
		htmlReporter = new ExtentHtmlReporter("./Extent.html");
		extent = new ExtentReports ();
		extent.attachReporter(htmlReporter);
		htmlReporter.config().setDocumentTitle(reportName);
		htmlReporter.config().setReportName(reportName);
		htmlReporter.config().setTestViewChartLocation(ChartLocation.TOP);
		htmlReporter.config().setTheme(Theme.DARK);
	}
	
	
	@AfterSuite(alwaysRun=true)
	public void AfterReports(ITestContext context) throws Exception{
		extent.flush();
		String suiteFileName = context.getCurrentXmlTest().getSuite().getFileName();
		System.out.println(suiteFileName);
		String basedir = System.getProperty("user.dir");
		suiteFileName.replace(basedir, "");
		List<String> to = SheetsAPI.getMailId();
		String from="no-reply-qa@tatahealth.com";
		String Subject = "EMR UI Autmn-"+System.getProperty("env")+suiteFileName;
		String body = "Hi all, \n\nThe report is Attached in this mail."
				+ "\nPlease Download the file to view it.";
		File file = new File("./Extent.html");    
		MimeMessage message=GmailAPI.createEmailWithAttachment(to,from,Subject, body,file);
		GmailAPI.sendMessage(from, message);
		
	}
}
