package com.tatahealth.API.libraries;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.ValueRange;

public class SheetsAPI {

	public static String spreadsheetId = "1fTcX6mwMc4LsMXcrc-p1HV5d-uZDYtf4RieQ_kLNGUE";

	/*
	 * public static String getDataFromSheet(String range) throws Exception{
	 * 
	 * Sheets sheetsService = Quickstart.getSheetsService(); //String spreadsheetId
	 * = "1_z2B1B4j8eMJhmp-1iDl33GBM-f9cjJwiC9koOMxZ9A"; ValueRange result =
	 * sheetsService.spreadsheets().values().get(spreadsheetId, range).execute();
	 * String s = result.getValues().get(0).get(0).toString().trim();
	 * //System.out.println(s); return s;
	 * 
	 * 
	 * }
	 */

	public static String getDataConfig(String key) throws Exception {
		FileReader fr = new FileReader("config.properties");
		Properties prop = new Properties();
		prop.load(fr);
		String value = prop.getProperty(key);
		System.out.println(value);
		fr.close();
		return value;
	}
	
	public static String getDataProperties(String key) throws Exception {
		FileReader fr = new FileReader("testData.properties");
		Properties prop = new Properties();
		prop.load(fr);
		String value = prop.getProperty(key);
		System.out.println(value);
		fr.close();
		return value;
	}

	public static String getDataFromSheet(String range) {

		Sheets sheetsService;
		spreadsheetId = "1_c_dHIqnOzacl5jSOQSizPNgdGoM8pMc1kPJROT7CIc";
		String s = null;
		try {

			sheetsService = Quickstart.getSheetsService();
			ValueRange result = sheetsService.spreadsheets().values().get(spreadsheetId, range).execute();
			s = result.getValues().get(0).get(0).toString().trim();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}

	/*
	 * public static void setDataToSheet(String value,String range) throws
	 * Exception{
	 * 
	 * Sheets sheetsService = Quickstart.getSheetsService(); //String spreadsheetId
	 * = "1_z2B1B4j8eMJhmp-1iDl33GBM-f9cjJwiC9koOMxZ9A"; Object obj = value;
	 * List<List<Object>> values = Arrays.asList(Arrays.asList(obj)); ValueRange
	 * requestBody = new ValueRange().setValues(values);
	 * sheetsService.spreadsheets().values().update(spreadsheetId, range,
	 * requestBody).setValueInputOption("RAW").execute();
	 * requestBody.setValues(Arrays.asList(Arrays.asList(value)));
	 * Sheets.Spreadsheets.Values.Update request =
	 * sheetsService.spreadsheets().values().update(spreadsheetId, range,
	 * requestBody); request.execute();
	 * 
	 * }
	 */

	/*
	 * public static List<String> getMailId() throws Exception{
	 * 
	 * Sheets sheetsService = Quickstart.getSheetsService(); //String spreadsheetId
	 * = "1_z2B1B4j8eMJhmp-1iDl33GBM-f9cjJwiC9koOMxZ9A"; List<String> mailID = new
	 * ArrayList<String>(); int n = 2;
	 * 
	 * while(true){ String range = "Mail!A"+n; ValueRange result =
	 * sheetsService.spreadsheets().values().get(spreadsheetId, range).execute();
	 * try{ String s = result.getValues().get(0).get(0).toString().trim();
	 * System.out.println(s); mailID.add(s); n++; }catch(Exception e){ break; }
	 * 
	 * }
	 * 
	 * return mailID;
	 * 
	 * }
	 */

	public static List<String> getMailId() throws Exception {
	    List<String> to=new ArrayList<String>();
	    to.add("qa.automation@tatahealth.com");
		/*FileReader fr = new FileReader("config.properties");
		Properties prop = new Properties();
		prop.load(fr);
		int n = 1;
		while (true) {
			String s = prop.getProperty("MAIL.id" + n);

		if (s==null) {
				break;
			}
			System.out.println(s);
			to.add(s);
			n++;

		}
		fr.close();*/

		return to;

	}

	/*
	 * public static List<String> getTestsToRun() throws Exception{
	 * 
	 * Sheets sheetsService = Quickstart.getSheetsService(); //String spreadsheetId
	 * = "1_z2B1B4j8eMJhmp-1iDl33GBM-f9cjJwiC9koOMxZ9A"; List<String> check = new
	 * ArrayList<String>(); int n = 2; String s = null; String range = null;
	 * while(true){
	 * 
	 * try{ range = "Check!B"+n; ValueRange result =
	 * sheetsService.spreadsheets().values().get(spreadsheetId, range).execute(); s
	 * = result.getValues().get(0).get(0).toString().trim();
	 * 
	 * if(s.equalsIgnoreCase("True")) { range = "Check!A"+n; result =
	 * sheetsService.spreadsheets().values().get(spreadsheetId, range).execute(); s
	 * = result.getValues().get(0).get(0).toString().trim(); check.add(s); }
	 * 
	 * n++; }catch(Exception e){ break; }
	 * 
	 * } System.out.println(check); return check;
	 * 
	 * }
	 * 
	 * 
	 * public static List<String> getTestsToRunOnDay1() throws Exception{
	 * 
	 * Sheets sheetsService = Quickstart.getSheetsService(); // String spreadsheetId
	 * = "1_z2B1B4j8eMJhmp-1iDl33GBM-f9cjJwiC9koOMxZ9A"; List<String> check = new
	 * ArrayList<String>(); int n = 2; String s = null; String range = null;
	 * while(true){
	 * 
	 * try{ range = "Check!B"+n; ValueRange result =
	 * sheetsService.spreadsheets().values().get(spreadsheetId, range).execute(); s
	 * = result.getValues().get(0).get(0).toString().trim();
	 * 
	 * if(s.equalsIgnoreCase("True")) { range = "Check!A"+n; result =
	 * sheetsService.spreadsheets().values().get(spreadsheetId, range).execute(); s
	 * = result.getValues().get(0).get(0).toString().trim(); check.add(s); }
	 * 
	 * n++; }catch(Exception e){ break; }
	 * 
	 * } System.out.println(check); return check;
	 * 
	 * }
	 * 
	 * 
	 * public static List<String> getTestsToRunOnDay2() throws Exception{
	 * 
	 * Sheets sheetsService = Quickstart.getSheetsService(); // String spreadsheetId
	 * = "1_z2B1B4j8eMJhmp-1iDl33GBM-f9cjJwiC9koOMxZ9A"; List<String> check = new
	 * ArrayList<String>(); int n = 2; String s = null; String range = null;
	 * while(true){
	 * 
	 * try{ range = "Check!C"+n; ValueRange result =
	 * sheetsService.spreadsheets().values().get(spreadsheetId, range).execute(); s
	 * = result.getValues().get(0).get(0).toString().trim();
	 * 
	 * if(s.equalsIgnoreCase("True")) { range = "Check!A"+n; result =
	 * sheetsService.spreadsheets().values().get(spreadsheetId, range).execute(); s
	 * = result.getValues().get(0).get(0).toString().trim(); check.add(s); }
	 * 
	 * n++; }catch(Exception e){ break; }
	 * 
	 * } System.out.println(check); return check;
	 * 
	 * }
	 */

}
